-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 20 Apr 2018 pada 10.22
-- Versi Server: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms_opd`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `username` varchar(30) NOT NULL,
  `nama` varchar(40) DEFAULT NULL,
  `telpon` varchar(15) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `alamat` varchar(45) DEFAULT NULL,
  `jenis_kelamin` tinyint(4) DEFAULT NULL,
  `password` varchar(65) DEFAULT NULL,
  `gambar` varchar(150) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isi_biodata` tinyint(4) DEFAULT NULL,
  `id_level_admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`username`, `nama`, `telpon`, `email`, `alamat`, `jenis_kelamin`, `password`, `gambar`, `status`, `isi_biodata`, `id_level_admin`) VALUES
('admin', 'Admin', NULL, NULL, NULL, NULL, '21232f297a57a5a743894a0e4a801fc3', NULL, 1, 1, 2),
('author', 'Author', NULL, NULL, NULL, NULL, '21232f297a57a5a743894a0e4a801fc3', NULL, 1, 1, 3),
('superadmin', 'Super Admin', NULL, NULL, NULL, NULL, '21232f297a57a5a743894a0e4a801fc3', NULL, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `album`
--

CREATE TABLE `album` (
  `id_album` int(11) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `tanggal_create` datetime DEFAULT NULL,
  `admin_username` varchar(30) NOT NULL,
  `softdelete_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `judul_berita` varchar(45) DEFAULT NULL,
  `kode_berita` varchar(45) DEFAULT NULL,
  `isi_berita` text,
  `created_time` date DEFAULT NULL,
  `updated_time` date DEFAULT NULL,
  `id_kategori_berita` int(11) NOT NULL,
  `author` varchar(30) NOT NULL,
  `keyword` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `softdelete_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `download`
--

CREATE TABLE `download` (
  `id_download` int(11) NOT NULL,
  `judul` varchar(45) DEFAULT NULL,
  `url_file` varchar(300) DEFAULT NULL,
  `admin_username` varchar(30) NOT NULL,
  `softdelete_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `foto`
--

CREATE TABLE `foto` (
  `id_foto` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `tanggal_posting` varchar(45) DEFAULT NULL,
  `nama_album` varchar(50) NOT NULL COMMENT 'Termasuk dalam album yang mana foto tersebut',
  `admin_username` varchar(30) NOT NULL,
  `token` varchar(100) NOT NULL,
  `softdelete_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_berita`
--

CREATE TABLE `kategori_berita` (
  `id_kategori_berita` int(11) NOT NULL,
  `nama_kategori` varchar(45) DEFAULT NULL,
  `deskripsi_kategori` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori_berita`
--

INSERT INTO `kategori_berita` (`id_kategori_berita`, `nama_kategori`, `deskripsi_kategori`) VALUES
(6, 'Pengumuman', 'Terbaru');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level_admin`
--

CREATE TABLE `level_admin` (
  `id_level_admin` int(11) NOT NULL,
  `nama_level` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level_admin`
--

INSERT INTO `level_admin` (`id_level_admin`, `nama_level`) VALUES
(1, 'Super Admin'),
(2, 'Admin'),
(3, 'Author');

-- --------------------------------------------------------

--
-- Struktur dari tabel `link_terkait`
--

CREATE TABLE `link_terkait` (
  `id` int(11) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `gbr` varchar(100) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `softdelete_status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `aktivitas` longtext NOT NULL,
  `date_timestamp` datetime NOT NULL,
  `ip_address` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `log`
--

INSERT INTO `log` (`id`, `username`, `aktivitas`, `date_timestamp`, `ip_address`) VALUES
(1, 'superadmin', 'Melakukan SOFT DELETE BERITA Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-11 04:27:11', '::1'),
(2, 'superadmin', 'Melakukan SOFT DELETE BERITA Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-11 04:27:31', '::1'),
(3, 'superadmin', 'Melakukan SOFT DELETE BERITA Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-11 04:27:33', '::1'),
(4, 'superadmin', 'Melakukan SOFT DELETE BERITA Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-11 04:27:36', '::1'),
(5, 'superadmin', 'Melakukan RETURN BERITA Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-11 04:27:53', '::1'),
(6, 'superadmin', 'Melakukan SOFT DELETE BERITA Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-11 04:28:05', '::1'),
(7, 'superadmin', 'Melakukan MENGHAPUS BERITA Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-11 04:28:18', '::1'),
(8, 'superadmin', 'Melakukan MENGHAPUS BERITA Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-11 04:28:21', '::1'),
(9, 'superadmin', 'Melakukan MENGHAPUS BERITA Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-11 04:28:23', '::1'),
(10, 'superadmin', 'Melakukan MENGHAPUS BERITA Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-11 04:28:26', '::1'),
(11, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coba\",\"tanggal_create\":\"2018-04-11 04:29:17am\",\"admin_username\":\"superadmin\"}', '2018-04-11 04:29:17', '::1'),
(12, 'superadmin', 'Melakukan MENAMBAH FOTO Data dengan Rincian Data Sebagai Berikut || {\"nama\":\"lapor.jpg\",\"tanggal_posting\":null,\"nama_album\":\"coba\",\"admin_username\":\"superadmin\",\"token\":\"0.7302387540224333\",\"softdelete_status\":\"0\"}', '2018-04-11 04:29:24', '::1'),
(13, 'superadmin', 'Melakukan MENAMBAH FOTO Data dengan Rincian Data Sebagai Berikut || {\"nama\":\"PPID.jpg\",\"tanggal_posting\":null,\"nama_album\":\"coba\",\"admin_username\":\"superadmin\",\"token\":\"0.14643789885114455\",\"softdelete_status\":\"0\"}', '2018-04-11 04:29:24', '::1'),
(14, 'superadmin', 'Melakukan MENAMBAH FOTO Data dengan Rincian Data Sebagai Berikut || {\"nama\":\"sirup.jpg\",\"tanggal_posting\":null,\"nama_album\":\"coba\",\"admin_username\":\"superadmin\",\"token\":\"0.30420552539462653\",\"softdelete_status\":\"0\"}', '2018-04-11 04:29:24', '::1'),
(15, 'superadmin', 'Melakukan MENGHAPUS FOTO Data dengan Rincian Data Sebagai Berikut || \"lapor.jpg\"', '2018-04-11 04:29:31', '::1'),
(16, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coba\"', '2018-04-11 04:29:47', '::1'),
(17, 'superadmin', 'Melakukan SOFT DELETE FILE DOWNLOAD Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-11 04:46:45', '::1'),
(18, 'superadmin', 'Melakukan MENGHAPUS FILE DOWNLOAD Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-11 04:47:10', '::1'),
(19, 'superadmin', 'Melakukan SOFT DELETE FILE DOWNLOAD Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-11 04:47:43', '::1'),
(20, 'superadmin', 'Melakukan MENGHAPUS FILE DOWNLOAD Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-11 04:47:49', '::1'),
(21, 'superadmin', 'Melakukan LOGIN Pada System', '2018-04-11 12:51:48', '::1'),
(22, 'superadmin', 'Melakukan LOGIN Pada System', '2018-04-15 12:06:41', '::1'),
(23, 'superadmin', 'Melakukan LOGIN Pada System', '2018-04-16 02:06:59', '::1'),
(24, 'superadmin', 'Melakukan LOGIN Pada System', '2018-04-16 05:14:23', '::1'),
(25, 'superadmin', 'Melakukan LOGIN Pada System', '2018-04-17 02:43:52', '::1'),
(26, 'superadmin', 'Melakukan SOFT DELETE FILE DOWNLOAD Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-17 03:04:12', '::1'),
(27, 'superadmin', 'Melakukan LOGIN Pada System', '2018-04-19 04:39:57', '::1'),
(28, 'superadmin', 'Melakukan SOFT DELETE FILE DOWNLOAD Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-19 04:40:04', '::1'),
(29, 'superadmin', 'Melakukan MENGEMBALIKAN LINK TERKAIT DENGAN ID = 8 Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-19 05:24:23', '::1'),
(30, 'superadmin', 'Melakukan SOFT DELETE FILE DOWNLOAD Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-19 05:27:00', '::1'),
(31, 'superadmin', 'Melakukan MENGHAPUS FILE DOWNLOAD Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-19 05:27:10', '::1'),
(32, 'superadmin', 'Melakukan SOFT DELETE FILE DOWNLOAD Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-19 05:32:52', '::1'),
(33, 'superadmin', 'Melakukan MENGHAPUS FILE DOWNLOAD Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-19 05:33:06', '::1'),
(34, 'superadmin', 'Melakukan SOFT DELETE FILE DOWNLOAD Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-19 05:36:40', '::1'),
(35, 'superadmin', 'Melakukan MENGHAPUS FILE DOWNLOAD Data dengan Rincian Data Sebagai Berikut || \"\"', '2018-04-19 05:36:52', '::1'),
(36, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coba\",\"tanggal_create\":\"2018-04-19 05:42:20am\",\"admin_username\":\"superadmin\"}', '2018-04-19 05:42:20', '::1'),
(37, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coba\"', '2018-04-19 05:42:23', '::1'),
(38, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"tes\",\"tanggal_create\":\"2018-04-19 06:55:08am\",\"admin_username\":\"superadmin\"}', '2018-04-19 06:55:08', '::1'),
(39, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/tes\"', '2018-04-19 06:55:30', '::1'),
(40, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coba\",\"tanggal_create\":\"2018-04-19 06:57:54am\",\"admin_username\":\"superadmin\"}', '2018-04-19 06:57:54', '::1'),
(41, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coba\"', '2018-04-19 06:58:02', '::1'),
(42, 'superadmin', 'Melakukan LOGIN Pada System', '2018-04-20 02:52:22', '::1'),
(43, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coba\",\"tanggal_create\":\"2018-04-20 02:52:46am\",\"admin_username\":\"superadmin\"}', '2018-04-20 02:52:46', '::1'),
(44, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coba\"', '2018-04-20 02:52:47', '::1'),
(45, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coba\",\"tanggal_create\":\"2018-04-20 03:08:46am\",\"admin_username\":\"superadmin\"}', '2018-04-20 03:08:46', '::1'),
(46, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coba\"', '2018-04-20 03:08:57', '::1'),
(47, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coba\",\"tanggal_create\":\"2018-04-20 03:15:32am\",\"admin_username\":\"superadmin\"}', '2018-04-20 03:15:32', '::1'),
(48, 'superadmin', 'Melakukan MENGUBAH NAMA ALBUM Data dengan Rincian Data Sebagai Berikut || null', '2018-04-20 03:19:28', '::1'),
(49, 'superadmin', 'Melakukan MENGUBAH NAMA ALBUM Data dengan Rincian Data Sebagai Berikut || null', '2018-04-20 03:28:44', '::1'),
(50, 'superadmin', 'Melakukan MENGUBAH NAMA ALBUM Data dengan Rincian Data Sebagai Berikut || null', '2018-04-20 03:34:21', '::1'),
(51, 'superadmin', 'Melakukan MENGUBAH NAMA ALBUM Data dengan Rincian Data Sebagai Berikut || null', '2018-04-20 03:34:33', '::1'),
(52, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coba\"', '2018-04-20 03:56:52', '::1'),
(53, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coba\",\"tanggal_create\":\"2018-04-20 03:57:30am\",\"admin_username\":\"superadmin\"}', '2018-04-20 03:57:30', '::1'),
(54, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coba\"', '2018-04-20 04:00:05', '::1'),
(55, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coba\",\"tanggal_create\":\"2018-04-20 04:17:05am\",\"admin_username\":\"superadmin\"}', '2018-04-20 04:17:05', '::1'),
(56, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coba\"', '2018-04-20 04:17:08', '::1'),
(57, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coba\",\"tanggal_create\":\"2018-04-20 04:31:34am\",\"admin_username\":\"superadmin\"}', '2018-04-20 04:31:34', '::1'),
(58, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coba\"', '2018-04-20 04:31:36', '::1'),
(59, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"caon\",\"tanggal_create\":\"2018-04-20 04:37:04am\",\"admin_username\":\"superadmin\"}', '2018-04-20 04:37:04', '::1'),
(60, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/caon\"', '2018-04-20 04:37:41', '::1'),
(61, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coba\",\"tanggal_create\":\"2018-04-20 04:38:40am\",\"admin_username\":\"superadmin\"}', '2018-04-20 04:38:40', '::1'),
(62, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coba\"', '2018-04-20 04:38:42', '::1'),
(63, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coba\",\"tanggal_create\":\"2018-04-20 04:39:01am\",\"admin_username\":\"superadmin\"}', '2018-04-20 04:39:01', '::1'),
(64, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coba\"', '2018-04-20 04:39:03', '::1'),
(65, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coba\",\"tanggal_create\":\"2018-04-20 04:39:36am\",\"admin_username\":\"superadmin\"}', '2018-04-20 04:39:36', '::1'),
(66, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coba\"', '2018-04-20 04:40:05', '::1'),
(67, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coba\",\"tanggal_create\":\"2018-04-20 05:04:10am\",\"admin_username\":\"superadmin\"}', '2018-04-20 05:04:10', '::1'),
(68, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coba\"', '2018-04-20 05:04:12', '::1'),
(69, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coab\",\"tanggal_create\":\"2018-04-20 05:04:30am\",\"admin_username\":\"superadmin\"}', '2018-04-20 05:04:30', '::1'),
(70, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coab\"', '2018-04-20 05:05:24', '::1'),
(71, 'superadmin', 'Melakukan LOGIN Pada System', '2018-04-20 10:12:59', '::1'),
(72, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"coba\",\"tanggal_create\":\"2018-04-20 10:13:08am\",\"admin_username\":\"superadmin\"}', '2018-04-20 10:13:08', '::1'),
(73, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/coba\"', '2018-04-20 10:13:09', '::1'),
(74, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"asd\",\"tanggal_create\":\"2018-04-20 10:13:45am\",\"admin_username\":\"superadmin\"}', '2018-04-20 10:13:45', '::1'),
(75, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/asd\"', '2018-04-20 10:14:07', '::1'),
(76, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"asd\",\"tanggal_create\":\"2018-04-20 10:14:29am\",\"admin_username\":\"superadmin\"}', '2018-04-20 10:14:29', '::1'),
(77, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/asd\"', '2018-04-20 10:17:11', '::1'),
(78, 'superadmin', 'Melakukan MEMBUAT ALBUM Data dengan Rincian Data Sebagai Berikut || {\"judul\":\"asd\",\"tanggal_create\":\"2018-04-20 10:18:05am\",\"admin_username\":\"superadmin\"}', '2018-04-20 10:18:05', '::1'),
(79, 'superadmin', 'Melakukan MENGHAPUS ALBUM Data dengan Rincian Data Sebagai Berikut || \"C:\\\\xampp\\\\htdocs\\\\web_master2\\\\media\\/gallery\\/asd\"', '2018-04-20 10:18:06', '::1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(45) DEFAULT NULL,
  `icon` varchar(45) DEFAULT NULL,
  `slug` varchar(45) DEFAULT NULL,
  `number` varchar(45) DEFAULT NULL,
  `id_parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu_temp`
--

CREATE TABLE `menu_temp` (
  `menu` varchar(500) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `page`
--

CREATE TABLE `page` (
  `id_page` int(11) NOT NULL,
  `judul_page` varchar(45) DEFAULT NULL,
  `isi_page` tinytext,
  `created_time` date DEFAULT NULL,
  `updated_time` date DEFAULT NULL,
  `tipe_page` varchar(10) DEFAULT NULL,
  `admin_username` varchar(30) NOT NULL,
  `kode_page` varchar(45) DEFAULT NULL,
  `kategori` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `page`
--

INSERT INTO `page` (`id_page`, `judul_page`, `isi_page`, `created_time`, `updated_time`, `tipe_page`, `admin_username`, `kode_page`, `kategori`) VALUES
(18, 'Beranda', NULL, NULL, NULL, NULL, 'admin', NULL, NULL),
(19, 'Berita', NULL, NULL, NULL, NULL, 'admin', 'berita', NULL),
(20, 'Gallery', NULL, NULL, NULL, NULL, 'admin', 'gallery', NULL),
(21, 'Download', NULL, NULL, NULL, NULL, 'admin', 'download', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `static`
--

CREATE TABLE `static` (
  `id_static` int(11) NOT NULL,
  `nama_dinas` varchar(100) DEFAULT NULL COMMENT 'Nama site setiap dinas',
  `home_gbr_1` varchar(150) DEFAULT NULL COMMENT 'URL Untuk Gambar Home Depan\n',
  `home_gbr_2` varchar(150) DEFAULT NULL COMMENT 'URL Untuk Gambar Home Depan\n',
  `home_gbr_3` varchar(150) DEFAULT NULL COMMENT 'URL Untuk Gambar Home Depan\n',
  `nama_kementerian` varchar(80) DEFAULT NULL,
  `alamat_kementerian` varchar(150) DEFAULT NULL,
  `link_kementerian` varchar(45) DEFAULT NULL,
  `nama_provinsi` varchar(80) DEFAULT NULL,
  `alamat_provinsi` varchar(150) DEFAULT NULL,
  `link_provinsi` varchar(45) DEFAULT NULL,
  `link_kabupaten` varchar(45) DEFAULT NULL,
  `logo_kementrian` varchar(150) DEFAULT NULL,
  `logo_provinsi` varchar(150) DEFAULT NULL,
  `logo_instansi` varchar(150) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `telpon` varchar(45) DEFAULT NULL,
  `jam_kerja` varchar(70) DEFAULT NULL,
  `warna` varchar(10) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `deskripsi_dinas` varchar(200) DEFAULT NULL,
  `facebook_dinas` varchar(100) DEFAULT NULL,
  `twitter_dinas` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `static`
--

INSERT INTO `static` (`id_static`, `nama_dinas`, `home_gbr_1`, `home_gbr_2`, `home_gbr_3`, `nama_kementerian`, `alamat_kementerian`, `link_kementerian`, `nama_provinsi`, `alamat_provinsi`, `link_provinsi`, `link_kabupaten`, `logo_kementrian`, `logo_provinsi`, `logo_instansi`, `email`, `telpon`, `jam_kerja`, `warna`, `alamat`, `deskripsi_dinas`, `facebook_dinas`, `twitter_dinas`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_admin_level_admin_idx` (`id_level_admin`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id_album`),
  ADD UNIQUE KEY `judul` (`judul`),
  ADD KEY `fk_album_admin1_idx` (`admin_username`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`id_download`),
  ADD KEY `fk_download_admin1_idx` (`admin_username`);

--
-- Indexes for table `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`id_foto`),
  ADD KEY `fk_foto_admin1_idx` (`admin_username`),
  ADD KEY `nama_album` (`nama_album`);

--
-- Indexes for table `kategori_berita`
--
ALTER TABLE `kategori_berita`
  ADD PRIMARY KEY (`id_kategori_berita`);

--
-- Indexes for table `level_admin`
--
ALTER TABLE `level_admin`
  ADD PRIMARY KEY (`id_level_admin`);

--
-- Indexes for table `link_terkait`
--
ALTER TABLE `link_terkait`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `fk_menu_menu1_idx` (`id_parent`);

--
-- Indexes for table `menu_temp`
--
ALTER TABLE `menu_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id_page`),
  ADD KEY `fk_page_admin1_idx` (`admin_username`);

--
-- Indexes for table `static`
--
ALTER TABLE `static`
  ADD PRIMARY KEY (`id_static`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id_album` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `download`
--
ALTER TABLE `download`
  MODIFY `id_download` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `foto`
--
ALTER TABLE `foto`
  MODIFY `id_foto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori_berita`
--
ALTER TABLE `kategori_berita`
  MODIFY `id_kategori_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `level_admin`
--
ALTER TABLE `level_admin`
  MODIFY `id_level_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `link_terkait`
--
ALTER TABLE `link_terkait`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu_temp`
--
ALTER TABLE `menu_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id_page` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `static`
--
ALTER TABLE `static`
  MODIFY `id_static` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `fk_admin_level_admin` FOREIGN KEY (`id_level_admin`) REFERENCES `level_admin` (`id_level_admin`);

--
-- Ketidakleluasaan untuk tabel `album`
--
ALTER TABLE `album`
  ADD CONSTRAINT `fk_album_admin1` FOREIGN KEY (`admin_username`) REFERENCES `admin` (`username`);

--
-- Ketidakleluasaan untuk tabel `download`
--
ALTER TABLE `download`
  ADD CONSTRAINT `fk_download_admin1` FOREIGN KEY (`admin_username`) REFERENCES `admin` (`username`);

--
-- Ketidakleluasaan untuk tabel `foto`
--
ALTER TABLE `foto`
  ADD CONSTRAINT `fk_foto_admin1` FOREIGN KEY (`admin_username`) REFERENCES `admin` (`username`),
  ADD CONSTRAINT `foto_ibfk_1` FOREIGN KEY (`nama_album`) REFERENCES `album` (`judul`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `log_ibfk_1` FOREIGN KEY (`username`) REFERENCES `admin` (`username`);

--
-- Ketidakleluasaan untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `fk_menu_menu1` FOREIGN KEY (`id_parent`) REFERENCES `menu` (`id_menu`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `fk_page_admin1` FOREIGN KEY (`admin_username`) REFERENCES `admin` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
