<?php
/*** set the content type header ***/
/*** Without this header, it wont work ***/
header("Content-type: text/css");

$warna_baru = '#'.$_REQUEST['warna'];


?>

/*You can add your won color also */
a:hover {
  color: <?= $warna_baru ?>;
}
.section-title span {
  color: <?= $warna_baru ?>;
}
.section-title:before {
  background: <?= $warna_baru ?>;
}
.btn-effect {
  background: <?= $warna_baru ?>;
}
.btn-effect:hover {
  background: <?= $warna_baru ?>;
}
.btn-effect:after {
  background: rgba(255, 255, 255, 0.2);
}
.btn-border:hover {
  color: <?= $warna_baru ?>;
}
.btn-wite:hover {
  color: <?= $warna_baru ?>;
}
.form-control:focus,
textarea:focus {
  box-shadow: none;
  border-bottom: 2px solid <?= $warna_baru ?>;
}
a.main-button,
input[type="submit"] {
  background-color: <?= $warna_baru ?>;
}
.carousel-control i:hover {
  color: <?= $warna_baru ?>;
  border-color: <?= $warna_baru ?>;
}
.slicknav_btn {
  border-color: <?= $warna_baru ?>;
}
.slicknav_btn:hover {
  background: <?= $warna_baru ?>;
}
.slicknav_nav a:hover,
.slicknav_nav .active {
  color: <?= $warna_baru ?>;
}
.slicknav_menu .slicknav_icon-bar {
  background: <?= $warna_baru ?>;
}
.navbar-default .navbar-nav > li:hover > a,
.navbar-default .navbar-nav > li > a.active {
  color: <?= $warna_baru ?>;
}
.dropdown > li:hover > a,
.sup-dropdown li:hover > a {
  color: #fff;
  background-color: <?= $warna_baru ?>;
}
.navbar-default .navbar-nav .dropdown {
  border-top: 3px solid <?= $warna_baru ?>;
}
.full-search {
  border-top: 3px solid <?= $warna_baru ?>;
}
.dropdown li a.active,
.sup-dropdown li a.active {
  color: #fff;
  background-color: <?= $warna_baru ?>;
}
.tp-caption .small-title span {
  color: <?= $warna_baru ?>;
}
.tparrows.preview1:hover:after {
  color: <?= $warna_baru ?>;
}
.tp-bullets.preview1 .bullet:hover,
.tp-bullets.preview1 .bullet.selected {
  background: <?= $warna_baru ?> !important;
}
.boxed {
  background: <?= $warna_baru ?>;
}
.controls a:hover,
.controls a.active {
  color: #fff;
  background: <?= $warna_baru ?>;
  border: 1px solid <?= $warna_baru ?>;
}
.portfolio-item .icon i {
  background: <?= $warna_baru ?>;
}
.portfolio-item .content {
  background: <?= $warna_baru ?>;
}
.featured-header i {
  color: <?= $warna_baru ?>;
  border: 2px solid <?= $warna_baru ?>;
}
.featured-header .featured-title p {
  color: <?= $warna_baru ?>;
}
.featured-content:hover i {
  background: <?= $warna_baru ?>;
  color: #fff;
}
.team-member .info {
  background: <?= $warna_baru ?>;
}
#counter:before {
  background: rgba(253, 197, 2, 0.7);
}
#testimonial .img-member {
  border: 4px solid <?= $warna_baru ?>;
}
#testimonial .client-info {
  background: <?= $warna_baru ?>;
}
#testimonial .owl-theme .owl-controls .owl-buttons div {
  background: <?= $warna_baru ?>;
}
#testimonial::before {
  background: rgba(253, 197, 2, 0.7);
}
.owl-theme .owl-controls .owl-page.active span,
.owl-theme .owl-controls.clickable .owl-page:hover span {
  background: <?= $warna_baru ?>;
}
.hours-widget .contact-info ul.hours span {
  color: <?= $warna_baru ?>;
}
.copyright-section p a {
  color: <?= $warna_baru ?>;
}
.back-to-top i {
  background-color: <?= $warna_baru ?>;
}
.switcher-box .open-switcher {
  background: <?= $warna_baru ?>;
}
.cta {
  background: <?= $warna_baru ?>;
}
.item-wrapper .icon i {
  color: <?= $warna_baru ?>;
  border: 2px solid <?= $warna_baru ?>;
}
.item-wrapper:hover i {
  background: <?= $warna_baru ?>;
  color: #fff;
}
.company-history .content-history-header .year {
  color: <?= $warna_baru ?>;
}
.partners .logo:hover {
  border-color: <?= $warna_baru ?>;
  background: #fff;
}
.partners .partners-text h3 a:hover {
  color: <?= $warna_baru ?>;
}
.pricing-tables .pricing-table .plan-name .icon i {
  color: <?= $warna_baru ?>;
  border: 2px solid <?= $warna_baru ?>;
}
.pricing-tables .pricing-table .plan-price {
  background: <?= $warna_baru ?>;
}
.service-box .service-head .icon i {
  color: <?= $warna_baru ?>;
}
.service-box .service-head:after {
  background: <?= $warna_baru ?>;
}
.service-box:hover .icon i {
  color: #fff;
  background: <?= $warna_baru ?>;
}
.services-2 .tab-menu .list-group li a:hover,
.services-2 .tab-menu .list-group li.active a {
  background: <?= $warna_baru ?>;
  color: #fff;
  border-color: <?= $warna_baru ?>;
}
.services-2 .tabs-content .services-list .list li span {
  background: <?= $warna_baru ?>;
}
.service-3 .service-item .icon i {
  color: <?= $warna_baru ?>;
  border: 2px solid <?= $warna_baru ?>;
}
.service-3 .service-item h3 a {
  color: <?= $warna_baru ?>;
}
.service-3 .service-item:hover {
  background: <?= $warna_baru ?>;
  border-color: <?= $warna_baru ?>;
}
.nav-tabs > li.active > a,
.nav-tabs > li.active > a:focus,
.nav-tabs > li > a:hover {
  background-color: <?= $warna_baru ?> !important;
  border-color: <?= $warna_baru ?> !important;
  color: #fff;
}
.panel-heading .panel-title a {
  background-color: <?= $warna_baru ?>;
}
.panel-heading .panel-title a.collapsed:hover {
  background-color: <?= $warna_baru ?>;
  color: #ffffff;
}
.post-title a:hover {
  color: <?= $warna_baru ?>;
}
.blog-post .post-thumb .hover-wrap .link i {
  color: <?= $warna_baru ?>;
  border: 2px solid <?= $warna_baru ?>;
}
.blog-post .post-thumb .hover-wrap .link i:hover {
  background: <?= $warna_baru ?>;
  color: #fff;
}
#pagination span.current,
#pagination a:hover {
  color: <?= $warna_baru ?>;
}
.widget-title:before {
  border-bottom: 1px solid <?= $warna_baru ?>;
}
#sidebar .cat-list li a:hover {
  color: <?= $warna_baru ?>;
}
#sidebar .posts-list .widget-content a:hover {
  color: <?= $warna_baru ?>;
}
#sidebar .tag a:hover {
  color: #fff;
  background: <?= $warna_baru ?>;
  border-color: <?= $warna_baru ?>;
}
blockquote {
  border-left-color: <?= $warna_baru ?>;
}
.reply-link:hover {
  background: <?= $warna_baru ?>;
  color: #fff;
}
.product .post-featured .esg-overlay .icon i {
  color: <?= $warna_baru ?>;
  border: 2px solid <?= $warna_baru ?>;
}
.product .post-featured .esg-overlay .icon i:hover {
  background: <?= $warna_baru ?>;
  color: #fff;
}
.product .post-content .product-price .amount {
  color: <?= $warna_baru ?>;
}
.product .btn-cart {
  border: 1px solid <?= $warna_baru ?>;
}
.product .btn-cart:hover {
  background: <?= $warna_baru ?>;
}
.page-header {
  background: rgba(253, 197, 2, 0.7);
}
.breadcrumb a:hover {
  color: <?= $warna_baru ?>;
}
.square-spin > div {
  background: <?= $warna_baru ?>;
  border: 1px solid <?= $warna_baru ?>;
}
