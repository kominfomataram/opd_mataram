/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
  config.filebrowserBrowserUrl = '../../../assets/bower_components/ckeditor_ext/imageuploader/upload.php?type=files';
  config.filebrowserImageBrowseUrl = '../../../assets/bower_components/ckeditor_ext/imageuploader/upload.php?type=images';
  config.filebrowserUploadUrl = '../../../assets/bower_components/ckeditor_ext/imageuploader/upload.php?type=files';
  config.filebrowserImageUploadUrl = '../../../assets/bower_components/ckeditor_ext/imageuploader/upload.php?type=images';
  //
};
