<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        BERITA
        <small>Data Berita</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <?php $msg1 = $this->session->flashdata('msg1'); if((isset($msg1)) && (!empty($msg1))) { ?>
          <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <?php print_r($msg1); ?>
          </div>
        <?php } ?>

        <div class="box-header with-border">
          <i class="fa fa-list"></i><h3 class="box-title">Daftar Berita</h3>
        </div>
       <!-- /.box-header -->
        <div class="box-body">
          <div class="form-group">
          </div>
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th><center>No.</center></th>
                <th><center>Judul</center></th>
                <th><center>Isi Pesan</center></th>
                <th><center>Tanggal</center></th>
                <th><center>Admin Pusat</center></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
              <tr>
                <th><center>No.</center></th>
                <th><center>Judul</center></th>
                <th><center>Isi Pesan</center></th>
                <th><center>Tanggal</center></th>
                <th><center>Admin Pusat</center></th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
