<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/back/bower_components/raphael/raphael.min.js')?>"></script>
<script src="<?php echo base_url('assets/back/bower_components/morris.js/morris.min.js')?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/back/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/back/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')?>"></script>
<script src="<?php echo base_url('assets/back/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/back/bower_components/jquery-knob/dist/jquery.knob.min.js')?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/back/bower_components/moment/min/moment.min.js')?>"></script>
<script src="<?php echo base_url('assets/back/bower_components/bootstrap-daterangepicker/daterangepicker.js')?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/back/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/back/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('assets/back/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/back/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/back/dist/js/pages/dashboard.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/back/dist/js/demo.js')?>"></script>

<script src="<?php echo base_url('assets/back/bower_components/chart.js/Chart2.min.js') ?>"></script>
<script>
$.getJSON("<?php echo base_url('JSON/berita_statistik_by_month') ?>", function(data){
    var bulan = []
    var data_statistik = []
    $.each(data, function (index, value) {
     //console.log("DATA MASUK = " + value.bulan);
     bulan.push(value.bulan);
     data_statistik.push(value.data);
      });
        console.log(bulan)
        console.log(data_statistik);
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: bulan,
            datasets: [{
              label: 'Jumlah Berita',
              data: data_statistik,
              backgroundColor: "rgba(153,255,51,0.4)"
            }]
          }
        });
    }); //batas getJSON
 </script>

 <script>
 $.getJSON("<?php echo base_url('JSON/berita_statistik_by_author') ?>", function(data){
     var author = []
     var data_statistik = []
     $.each(data, function (index, value) {
      console.log("DATA MASUK = " + value.author);
      author.push(value.author);
      data_statistik.push(value.data);
       });
         var ctx = document.getElementById('myChart2').getContext('2d');
         var myChart = new Chart(ctx, {
           type: 'pie',
           data: {
             labels: author,
             datasets: [{
               label: 'Jumlah Berita',
               data: data_statistik,
               backgroundColor: "rgba(255, 82, 51, 0.4)"
             }]
           }
         });
     }); //batas getJSON
  </script>
