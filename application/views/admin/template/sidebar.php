
  <?php
  $status_user = $this->session->userdata('is_logged_in');
  if ($status_user == 1) {
      $this->load->view('admin/template/sidebar/sidebar_superadmin');
  }
  elseif ($status_user == 2) {
      $this->load->view('admin/template/sidebar/sidebar_admin');
  }
  elseif ($status_user == 3) {
      $this->load->view('admin/template/sidebar/sidebar_author');
  }
  else {
      echo 'Periksa Kembali Status Admin Anda';
  }

  ?>
