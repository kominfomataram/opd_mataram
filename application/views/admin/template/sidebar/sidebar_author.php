<!-- SIDEBAR SUPERADMIN  -->
<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active"><a href="<?php echo base_url('index.php/admin/Dashboard') ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

        <li class="treeview ">
          <a href="#"><i class="fa fa-newspaper-o"></i>
            <span>Berita</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="<?php echo base_url('admin/berita/tambah_berita') ?>"><i class="fa fa-plus"></i> <span>Tambah Berita</span></a></li>
            <li class=""><a href="<?php echo base_url('admin/berita/semua_data_berita') ?>"><i class="fa fa-square"></i> <span>Data Berita</span></a></li>
            <li class=""><a href="<?php echo base_url('admin/berita/kategori_berita') ?>"><i class="fa fa-plus"></i> <span>Kategori Berita</span></a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="<?php echo base_url('admin/gallery') ?>"><i class="fa fa-photo"></i>
            <span>Gallery</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo base_url('admin/gallery') ?>"><i class="fa fa-photo"></i>
              <span>Foto</span>
            </a></li>
        </ul>
        </li>
        <li class="treeview ">
          <a href="#"><i class="fa fa-file"></i>
            <span>Download</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="<?php echo base_url('admin/download/tambah_download') ?>"><i class="fa fa-plus"></i> <span>Tambah File Download</span></a></li>
            <li><a href="<?php echo base_url('admin/download') ?>"><i class="fa fa-download"></i> <span>Data File Download</span></a></li>
            <li class=""><a href="<?php echo base_url('admin/download/trash_download') ?>"><i class="fa fa-trash"></i> <span>Trash Download</span></a></li>
          </ul>
        </li>
        <li class="treeview ">
          <a href="#"><i class="fa fa-link"></i>
            <span>Link Terkait</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="<?php echo base_url('admin/link_terkait/tambah_link_terkait') ?>"><i class="fa fa-plus"></i> <span>Tambah Link Terkait</span></a></li>
            <li><a href="<?php echo base_url('admin/link_terkait') ?>"><i class="fa fa-link"></i> <span>Data Link Terkait</span></a></li>
          </ul>
        </li>
        <li class="treeview ">
          <a href="#"><i class="fa fa-book"></i>
            <span>Halaman</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="<?php echo base_url('admin/halaman/tambah_halaman') ?>"><i class="fa fa-plus"></i> <span>Tambah Halaman</span></a></li>
            <li class=""><a href="<?php echo base_url('admin/halaman/data_halaman') ?>"><i class="fa fa-square"></i> <span>Data Halaman</span></a></li>
          </ul>
        </li>        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
