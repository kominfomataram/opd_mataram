<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url('') ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>OPD</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>OPD</b> - CMS</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->

          <?php $avatar = $this->db->query("SELECT gambar FROM admin WHERE username = '".$this->session->userdata('username')."'")->row()->gambar;  ?>

          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">              
              <span class="hidden-xs"><?php echo $this->session->userdata('username');?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">              
                <p>
                  <?php echo $this->session->userdata('username');?> - <?php echo $this->session->userdata('nama_level_admin');?>
                </p>
              </li>
              <!-- Menu Footer-->

              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url('/admin/pengaturan/'.$this->session->userdata('username')) ?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('admin/Login/logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
