<!-- jQuery 3 -->
<script src="http://localhost:8001/web_master/assets/back/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="http://localhost:8001/web_master/assets/back/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<script>
    $(document).ready(function() {
        // Untuk sunting
        $('#modal-edit').on('show.bs.modal', function (event) {
            var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
            var modal          = $(this)

            // Isi nilai pada field
            modal.find('#id').attr("value",div.data('id'));
            modal.find('#judul_lama').attr("value",div.data('judul_lama'));
            modal.find('#judul_baru').attr("value",div.data('judul_baru'));
        });
    });
</script>

