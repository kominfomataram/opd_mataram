
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        GALLERY
        <small>Gallery untuk menambahkan file </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Gallery</a></li>
        <li class="active">Folder Gallery</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <?php $msg1 = $this->session->flashdata('msg1'); if((isset($msg1)) && (!empty($msg1))) { ?>
          <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <?php print_r($msg1); ?>
          </div>
        <?php } ?>
        <?php $msg1 = $this->session->flashdata('msg2'); if((isset($msg1)) && (!empty($msg1))) { ?>
          <div class="alert alert-danger">
            <button class="close" data-dismiss="alert">x</button>
            <?php print_r($msg1); ?>
          </div>
        <?php } ?>
        <div class="box-header with-border">
          <h3 class="box-title">Folder - Folder Gallery</h3>
        </div>

        <div class="box-body">
          <div class="col-md-12">
            <!-- <h2>Multiple Image upload</h2>
            <form action="<?php echo base_url('index.php/admin/Gallery/imageUploadPost') ?>" enctype="multipart/form-data" class="dropzone" id="image-upload" method="POST">
              <div>

              </div>
            </form> -->

          </div>

          <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-default">
            <span class="fa fa-folder"></span> Tambah Folder
          </button>
          <br>
          <br>
          <table id="list_admin" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Album</th>
                <th>Tanggal Dibuat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $no = 1;
                foreach ($gallery as $g) {
                  # code...
                  $notif="Apakah anda yakin ingin menghapus data berikut?";
                  ?>
                  <tr>
                    <td><?php echo $no++ ?></td>
                    <td><img src="<?php echo base_url('assets/back/img/foldericon.png') ?>" alt="" style="max-width: 4%; height: auto;"><span></span><?php echo $g['judul']?></td>
                    <td><?php echo $g['tanggal_create']?></td>
                 
                  <td><center>
                  <a href="<?php echo base_url('admin/gallery/tambah_foto/').$g['id_album']?>" class="btn btn-success"><span class="fa fa-plus"></span> Foto</a>
                  <a
                      href="javascript:;"
                      data-id="<?php echo $g['id_album'] ?>"
                      data-judul_lama="<?php echo $g['judul']?>"
                      data-judul_baru="<?php echo $g['judul']?>"
                      data-toggle="modal" data-target="#modal-edit">
                      <button  data-toggle="modal" data-target="#ubah-data" class="btn btn-warning"><span class="fa fa-pencil"></span>Update</button>
                  </a>

                  <a 
                    onClick="return confirm('Apakah anda yakin ingin menhapus data berikut?');" 
                    href="<?php echo base_url('admin/gallery/delete_gallery/').$g['id_album'].'/'. bersihkan_kata(str_replace(" ", "-" , strtolower($g['judul'])))?>"
                    class="btn btn-danger"
                    ><span class="fa fa-times"></span> Hapus</a>
                  </center></td>
                  <?php echo '</tr>';
                }
              ?>
            </tbody>

            <tfoot>
              <tr>
                <th>No</th>
                <th>Album</th>
                <th>Tanggal Dibuat</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view('admin/gallery/gallery_tambah_modal'); ?>
  <?php $this->load->view('admin/gallery/gallery_edit_modal'); ?>
