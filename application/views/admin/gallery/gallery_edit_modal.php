<div class="modal fade" id="modal-edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Edit Folder</h4>
      </div>
      <div class="modal-body">
        <form class="" action="<?php echo base_url() ?>admin/gallery/update_gallery" method="post">
          <div class="form-group has-feedback">
            <label for="">Nama Album</label><br>
            <input type="hidden" id="id" name="id">
            <input type="hidden" id="judul_lama" name="judul_lama" placeholder="Nama Album" class="form-control" id="edit_input">
            <input type="text" id="judul_baru" name="judul_baru" placeholder="Nama Album" class="form-control" id="edit_input">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-warning" onClick="return confirm('Apakah anda yakin ingin mengubah data berikut?');" >Update</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
