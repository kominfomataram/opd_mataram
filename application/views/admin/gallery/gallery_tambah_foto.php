<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        BERITA
        <small>Data Kategori Berita</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php $msg1 = $this->session->flashdata('msg1'); if ((isset($msg1)) && (!empty($msg1))) { ?>
        <div class="alert alert-success">
          <button class="close" data-dismiss="alert">x</button>
          <?php print_r($msg1); ?>
        </div>
      <?php } ?>

      <div class="row">
        <!-- Default box -->
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <i class="fa fa-list"></i><h3 class="box-title">Tambah Foto</h3>
            </div>
           <!-- /.box-header -->
            <div class="box-body">
              <div class="dropzone">
                <div class="dz-message">
                 <h3> Klik atau Drop gambar disini</h3>
                </div>
              </div>
              <div class="form-group">
                <br><a onclick="selesai()" class="btn btn-primary pull-right" > SELESAI </a>
              </div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <i class="fa fa-list"></i><h3 class="box-title">Daftar Foto</h3>
            </div>
           <!-- /.box-header -->

           <div class="box-body">
              <?php foreach ($foto as $f) {
        ?>
                  <div class="col-md-2 col-lg-2 col-sm-6 col-xs-12">
                    <div class="thumbnail">
                      <img style="" src="<?php echo base_url('media/gallery/').$nama_folder->judul.'/'.$f['nama'] ?>" alt="">
                    </div>
                    <div class="form-group text-center">
                        <h5><?php echo $f['nama'] ?></a></h5>
                        <a onclick="delete_foto('<?php echo $nama_folder->judul; ?>','<?php echo $f['nama']?>')" class="btn btn-danger fa fa-trash" > Delete </a>
                      </div>
                  </div>

              <?php
    }  ?>
           </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
