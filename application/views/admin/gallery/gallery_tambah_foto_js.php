<script type="text/javascript">

	Dropzone.autoDiscover = false;
	var nama_folder = "<?php echo $nama_folder->judul ?>";
	var foto_upload= new Dropzone(".dropzone",{
		url: "<?php echo base_url('admin/gallery/upload_gallery/') ?>"+nama_folder,
		maxFilesize: 2,
		method:"post",
		acceptedFiles:"image/*",
		paramName:"userfile",
		dictInvalidFileType:"Type file ini tidak dizinkan",
		addRemoveLinks:true,
	});

	//Event ketika Memulai mengupload
	foto_upload.on("sending",function(a,b,c){
		a.token=Math.random();
		c.append("token_foto",a.token); //Menmpersiapkan token untuk masing masing foto
	});


	//Event ketika foto dihapus
	foto_upload.on("removedfile",function(a){
		var token=a.token;
		var nama_folder = "<?php echo $nama_folder->judul ?>";
		$.ajax({
			type:"post",
			data:{token:token},
			url:"<?php echo base_url('admin/gallery/remove_gallery/') ?>"+nama_folder,
			cache:false,
			dataType: 'json',
			success: function(){
				console.log("Foto terhapus");
			},
			error: function(){
				console.log("Error");

			}
		});
	});
</script>
<script type="text/javascript">
  $(document).ready( function () {
  } );

    function delete_foto(folder,nama_foto)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo base_url('admin/gallery/delete_foto/')?>"+folder+'/'+nama_foto,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(' Error deleting data '+nama_foto + errorThrown + textStatus +jqXHR);
            }
        });

      }
    }

    function selesai() {
    	location.reload();
    }
</script>
