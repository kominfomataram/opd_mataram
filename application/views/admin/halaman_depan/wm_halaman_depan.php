<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Halaman Depan Website
      <small>Pengaturan untuk halaman depan website</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li>Web Master</li>
      <li class="active">Halaman Depan</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
       <!-- Main row -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-default">
          <div class="box-header with-border">
            <i class="fa fa-pagelines"></i><h3 class="box-title">Halaman Depan <?php echo static_data('nama_dinas') ?></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <?php $msg = $this->session->flashdata('pesan'); if((isset($msg)) && (!empty($msg))) { ?>
              <div class="alert alert-danger">
                <button class="close" data-dismiss="alert">x</button>
                <?php print_r($msg); ?>
              </div>
            <?php } ?>
             <?php $msg = $this->session->flashdata('berhasil'); if((isset($msg)) && (!empty($msg))) { ?>
              <div class="alert alert-success">
                <button class="close" data-dismiss="alert">x</button>
                <?php print_r($msg); ?>
              </div>
            <?php } ?>



        <form role="form" method="post" action="<?php echo base_url('admin/web_master/update_web_master'); ?>" enctype="multipart/form-data">
              <div class="col col-md-6">

                <?php
                    $i=0;
                    $value;
                    $param = null;
                    if (static_data($param == null)) {
                      # code...
                      $value = 'value =""';
                    } else {
                      $value = 'value = "'.static_data($param)."'";
                    }
                ?>

                <div class="form-group has-feedback">
                  <label for="tile">Nama Dinas</label>
                  <input type="text" class="form-control" name="title" placeholder="Tuliskan Nama Dinas Pengguna Website"  value="<?php echo static_data('nama_dinas') ?>" >
                  <span class="fa fa-angle-double-up form-control-feedback"></span>
                  <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                </div>

                <div class="form-group has-feedback">
                  <label for="tile">Email</label>
                  <input type="email" class="form-control" name="email" placeholder="Email Umum Instansi" value="<?php echo static_data('email') ?>">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                </div>

                <div class="form-group has-feedback">
                  <label for="tile">Telpon</label>
                  <input type="text" class="form-control" name="telpon" placeholder="Telpon Insansi Terkait" value="<?php echo static_data('telpon') ?>">
                  <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                  <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                </div>

                <div class="form-group has-feedback">
                  <label for="tile">Jam Kerja</label>
                  <input type="text" class="form-control" name="jam" placeholder="Jam Kerja Instansi" value="<?php echo static_data('jam_kerja') ?>">
                  <span class="glyphicon glyphicon-time form-control-feedback"></span>
                  <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                </div>

                <div class="form-group has-feedback">
                  <label for="tile">Alamat</label>
                  <input type="text" class="form-control" name="alamat" placeholder="Alamat Instansi" value="<?php echo static_data('alamat') ?>">
                  <span class="glyphicon glyphicon-home form-control-feedback"></span>
                  <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                </div>

                <div class="form-group has-feedback">
                  <label for="tile">Deskripis Dinas</label>  <input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi Dinas" value="<?php echo static_data('deskripsi_dinas') ?>">
                    <span class="fa fa-gear form-control-feedback"></span>
                    <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                </div>

                <div class="form-group has-feedback">
                  <label for="tile">Alamat Facebook Resmi Dinas</label>  <input type="text" class="form-control" name="facebook" placeholder="Deskripsi Dinas" value="<?php echo static_data('facebook_dinas')  ?>">
                    <span class="fa fa-facebook form-control-feedback"></span>
                    <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                </div>

                <div class="form-group has-feedback">
                  <label for="tile">Alamat Twitter Resmi Dinas</label>  <input type="text" class="form-control" name="twitter" placeholder="Deskripsi Dinas" value="<?php echo static_data('twitter_dinas') ?>">
                    <span class="fa fa-twitter form-control-feedback"></span>
                    <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                </div>

                <div class="form-group has-feedback">
                  <label for="tile">Warna Untuk Website</label> : <input type="color" name="warna" value="<?php echo static_data('warna') ?>">
                  <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                </div>

              </div>

              <div class="col col-md-6">

                <div class="form-group has-feedback">
                  <label for="tile">Logo Instansi</label>
                  <input type="file" class="form-control" name="logo<?php echo $i++;?>" id="logo_instansi">
                  <span class="glyphicon glyphicon-picture form-control-feedback"></span>
                  <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                </div>

                <?php if (static_data('logo_instansi') == 'Data Tidak Ada')  {  ?>
                  <div class="form-group has-feedback">
                    <center>
                      <img src="<?php echo assets('skpd/img' , 'additional/gambar404.png') ?>" alt="" height="60%" width="40%">
                    </center>
                  </div>
                <?php } else { ?>
                  <div class="form-group has-feedback">
                    <center>
                      <img src="<?php echo base_url('media/static/'.static_data('logo_instansi')) ?>" alt="" height="60%" width="40%">
                    </center>
                  </div>
                <?php } ?>

              </div>
              <!-- /.col -->

          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <!--
            <div class="col col-md-6">
                  <div class="form-group has-feedback">
                    <label for="tile">Nama Kementerian</label>
                    <input type="text" class="form-control" name="nama_kementerian" placeholder="Link Kementerian Pusat Dinas Terkait" value="<?php echo static_data('nama_kementerian') ?>">
                    <span class="glyphicon glyphicon-link form-control-feedback"></span>
                    <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="tile">Link Kementerian</label>
                    <input type="text" class="form-control" name="link_kementerian" placeholder="Link Kementerian Pusat Dinas Terkait" value="<?php echo static_data('link_kementerian') ?>">
                    <span class="glyphicon glyphicon-link form-control-feedback"></span>
                    <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="tile">Logo Kementerian</label>
                    <input type="file" class="form-control" name="logo<?php echo $i++;?>" id="logo_menteri">
                    <span class="glyphicon glyphicon-picture form-control-feedback"></span>
                    <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                  </div>

                  <?php if (static_data('logo_kementrian') == 'Data Tidak Ada')  {  ?>
                    <div class="form-group has-feedback">
                      <center>
                        <img src="<?php echo assets('skpd/img' , 'additional/gambar404.png') ?>" alt="" height="60%" width="40%">
                      </center>
                    </div>
                  <?php } else { ?>
                    <div class="form-group has-feedback">
                      <center>
                        <img src="<?php echo base_url('media/static/'.static_data('logo_kementrian')) ?>" alt="" height="60%" width="40%">
                      </center>
                    </div>
                  <?php } ?>

                </div>

                <div class="col col-md-6">

                  <div class="form-group has-feedback">
                    <label for="tile">Nama Provinsi</label>
                    <input type="text" class="form-control" name="nama_provinsi" placeholder="Link Kementerian Pusat Dinas Terkait" value="<?php echo static_data('nama_provinsi') ?>">
                    <span class="glyphicon glyphicon-link form-control-feedback"></span>
                    <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="tile">Link Provinsi</label>
                    <input type="text" class="form-control" name="link_provinsi" placeholder="Link Kementerian Pusat Dinas Terkait" value="<?php echo static_data('link_provinsi') ?>">
                    <span class="glyphicon glyphicon-link form-control-feedback"></span>
                    <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="tile">Logo Provinsi</label>
                    <input type="file" class="form-control" name="logo<?php echo $i++;?>" id="logo_provinsi">
                    <span class="glyphicon glyphicon-picture form-control-feedback"></span>
                    <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                  </div>

                  <?php if (static_data('logo_provinsi') == 'Data Tidak Ada')  {  ?>
                    <div class="form-group has-feedback">
                      <center>
                        <img src="<?php echo assets('skpd/img' , 'additional/gambar404.png') ?>" alt="" height="60%" width="40%">
                      </center>
                    </div>
                  <?php } else { ?>
                    <div class="form-group has-feedback">
                      <center>
                        <img src="<?php echo base_url('media/static/'.static_data('logo_provinsi')) ?>" alt="" height="60%" width="40%">
                      </center>
                    </div>
                  <?php } ?>

                </div>

            </div>
            -->

            <div class="box-footer">
              <div class="col col-md-12">
                <table>
                <tr>
                  <td>
                    <div class="form-group has-feedback">
                      <label for="tile">Gambar Slide I</label>
                      <input type="file" class="form-control" name="logo<?php echo $i++;?>" >
                      <span class="glyphicon glyphicon-picture form-control-feedback"></span>
                      <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                    </div>
                  </td>

                  <td>
                    <div class="form-group has-feedback">
                      <label for="tile">Gambar Slide II</label>
                      <input type="file" class="form-control" name="logo<?php echo $i++;?>" >
                      <span class="glyphicon glyphicon-picture form-control-feedback"></span>
                      <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                    </div>
                  </td>

                  <td>
                    <div class="form-group has-feedback">
                      <label for="tile">Gambar Slide III</label>
                      <input type="file" class="form-control" name="logo<?php echo $i++;?>" >
                      <span class="glyphicon glyphicon-picture form-control-feedback"></span>
                      <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
                    </div>
                  </td>
                </tr>

                <tr>

                  <?php if (static_data('home_gbr_1') == 'Data Tidak Ada')  {  ?>
                    <td>
                      <center>
                      <img src="<?php echo assets('skpd/img' , 'additional/gambar404panjang.png') ?>" alt="" height="60%" width="50%">
                      </center>
                    </td>
                  <?php } else { ?>
                    <td>
                      <center>
                      <img src="<?php echo base_url('/media/static/'.static_data('home_gbr_1')) ?>" alt="" height="60%" width="50%">
                      </center>
                    </td>
                  <?php } ?>


                  <?php if (static_data('home_gbr_2') == 'Data Tidak Ada')  {  ?>
                    <td>
                      <center>
                      <img src="<?php echo assets('skpd/img' , 'additional/gambar404panjang.png') ?>" alt="" height="60%" width="50%">
                      </center>
                    </td>
                  <?php } else { ?>
                    <td>
                      <center>
                      <img src="<?php echo base_url('/media/static/'.static_data('home_gbr_2')) ?>" alt="" height="60%" width="50%">
                      </center>
                    </td>
                  <?php } ?>

                  <?php if (static_data('home_gbr_3') == 'Data Tidak Ada')  {  ?>
                    <td>
                      <center>
                      <img src="<?php echo assets('skpd/img' , 'additional/gambar404panjang.png') ?>" alt="" height="60%" width="50%">
                      </center>
                    </td>
                  <?php } else { ?>
                    <td>
                      <center>
                      <img src="<?php echo base_url('/media/static/'.static_data('home_gbr_3')) ?>" alt="" height="60%" width="50%">
                      </center>
                    </td>
                  <?php } ?>

                </tr>
                </table>
              </div>
            </div>

            <div class="box-footer">
              <button type="submit" name="button" class="btn btn-default">UPDATE</button>
            </div>
          </div>
          </form>
        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->
    </div>
    <!-- /.row (main row) -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
