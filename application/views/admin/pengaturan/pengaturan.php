<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Pengaturan
      <small>Biodata Admin</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li>Dashboard</li>
      <li class="active">Edit Admin</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
       <!-- Main row -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-default">
          <div class="box-header with-border">
            <i class="fa fa-user"></i><h3 class="box-title">Edit Admin - <?php echo $this->session->userdata('username') ?></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <?php $msg = $this->session->flashdata('pesan'); if((isset($msg)) && (!empty($msg))) { ?>
              <div class="alert alert-danger">
                <button class="close" data-dismiss="alert">x</button>
                <?php print_r($msg); ?>
              </div>
            <?php } ?>
             <?php $msg = $this->session->flashdata('berhasil'); if((isset($msg)) && (!empty($msg))) { ?>
              <div class="alert alert-success">
                <button class="close" data-dismiss="alert">x</button>
                <?php print_r($msg); ?>
              </div>
            <?php } ?>

             <form action="<?php echo base_url('admin/update_admin_action_pengaturan') ?>" method="post">


              <input type="hidden" name="username_lama" value="<?php echo $userdata[0]['username'] ?>">

              <div class="form-group has-feedback">
                <input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo $userdata[0]['email'] ?>" required>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                <span class="text-whitw" style="color: red;"><?php echo form_error('email'); ?></span>
              </div>

              <div class="form-group has-feedback">
                <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $userdata[0]['username'] ?>" required>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                <span class="text-whitw" style="color: red;"><?php echo form_error('username'); ?></span>
              </div>

              <div class="form-group has-feedback">
                <input type="text" class="form-control" name="nama" placeholder="Nama" value="<?php echo $userdata[0]['nama'] ?>" required>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                <span class="text-whitw" style="color: red;"><?php echo form_error('username'); ?></span>
              </div>


              <div class="form-group has-feedback">
                <input type="text" class="form-control" name="telpon" placeholder="Telpon" value="<?php echo $userdata[0]['telpon'] ?>" required>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                <span class="text-whitw" style="color: red;"><?php echo form_error('username'); ?></span>
              </div>

              <div class="form-group has-feedback">
                <input type="text" class="form-control" name="alamat" placeholder="Alamat" value="<?php echo $userdata[0]['alamat'] ?>" required>
                <span class="glyphicon glyphicon-home form-control-feedback"></span>
                <span class="text-whitw" style="color: red;"><?php echo form_error('username'); ?></span>
              </div>

              <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="Password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <span class="text-whitw" style="color: red;"><?php echo form_error('password'); ?></span>
              </div>

              <div class="form-group has-feedback">
                <input type="password" class="form-control" name="confirm_password" placeholder="Retype password" required>
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                <span class="text-whitw" style="color: red;"><?php echo form_error('repassword'); ?></span>
              </div>



              <div class="form-group has-feedback">
                <div class="radio-inline">
                  <label> Jenis Kelamin :</label>
                </div>
                <div class="radio-inline">
                  <label><input type="radio" name="jk" id="jenkel1" value="1"> Laki - Laki</label>
                </div>
                <div class="radio-inline">
                  <label><input type="radio" name="jk" id="jenkel1" value="2"> Perempuan</label>
                </div>        
              </div>

              <input type="hidden" name='level' value="<?php echo $this->session->userdata('id_level_admin') ?>">

              <div class="col-xs-12">
                <button type="submit" class="btn btn-success pull-right btn-flat">Update</button>
              </div>
              <!-- /.col -->
              </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row (main row) -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
