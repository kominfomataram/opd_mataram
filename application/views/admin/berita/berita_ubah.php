<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        BERITA
        <small>Ubah Berita</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-book"></i><h3 class="box-title">Ubah Berita</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php $msg = $this->session->flashdata('pesan'); if((isset($msg)) && (!empty($msg))) { ?>
                <div class="alert alert-danger">
                  <button class="close" data-dismiss="alert">x</button>
                  <?php print_r($msg); ?>
                </div>
              <?php } ?>
               <?php $msg = $this->session->flashdata('berhasil'); if((isset($msg)) && (!empty($msg))) { ?>
                <div class="alert alert-success">
                  <button class="close" data-dismiss="alert">x</button>
                  <?php print_r($msg); ?>
                </div>
              <?php } ?>

              <form action="<?php echo base_url('admin/berita/update_berita/'.$data_berita->id_berita) ?>" method="post" onsubmit="return confirm('Apakah anda yakin ingin menyimpan berita?');">
                <div class="form-group has-feedback">
                  <label for="judul">Judul Berita</label>
                  <input type="text" class="form-control" id="judul_berita" name="judul" placeholder="Judul Berita" v-model="message" placeholder="Masukkan Judul Berita" value="<?php echo $data_berita->judul_berita ?>">
                  <span class="glyphicon glyphicon-book form-control-feedback"></span>
                  <!-- <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span> -->
                </div>

                <div class="form-group has-feedback">
                  <label for="judul">Kode Berita</label>
                  <input type="text" id="kode_berita" class="form-control" name="kode" placeholder="Kode Halaman Akan Di Generate Sendiri Dari Judul" value="<?php echo $data_berita->kode_berita ?>" disabled>
                  <span class="fa fa-chain form-control-feedback"></span>
                  <!-- <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span> -->
                </div>

                <div class="form-group has-feedback">
                  <label>Kategori Berita</label>
                  <select class="form-control" name="kategori">
                    <?php foreach ($kategori as $kat): ?>
                      <option <?php if($data_berita->id_kategori_berita == $kat['id_kategori_berita']){echo "selected";} ?> value="<?php echo $kat['id_kategori_berita'];?>"><?php echo $kat['nama_kategori'];?></option>
                    <?php endforeach ?>
                  </select>
                </div>

                <div class="form-group has-feedback">
                  <label for="editor">Isi Berita</label>
                  <!-- <textarea id="editor_tambah_berita" name="editor_tambah_berita" rows="10" cols="80" placeholder="Silahkan Isi Berita Anda Di Sini"></textarea> -->
                  <textarea class="ckeditor" name="isi"><?php echo $data_berita->isi_berita ?></textarea>
                </div>

                <div class="form-group has-feedback">
                  <label for="judul">Kata Kunci</label>
                  <input type="text" class="form-control" id="kata_kunci" name="kata_kunci" v-model="message" placeholder="Masukkan kata kunci berita" value="<?php echo $data_berita->keyword ?>">
                  <span class="glyphicon glyphi<?php echo $data_berita->isi_berita ?>con-tags form-control-feedback"></span>
                  <!-- <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span> -->
                </div>

                <div class="col-xs-12">                  
                  <button type="submit" class="btn btn-primary pull-right btn-flat">Simpan</button>
                </div>
                <!-- /.col -->
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
