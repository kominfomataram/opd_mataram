<!-- DataTables -->
<script src="<?php echo base_url('assets/back/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/back/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<script>
  $(function () {
    $('#data_berita').DataTable()
  })
</script>

<script type="text/javascript">
  $(document).ready( function () {
  } );

    function delete_berita(id , type)
    {
      var notif ='' ;
      if (type == '1') {
        notif = 'Hapus Permanen';
      }
      else if (type == '2') {
        notif = 'Return';
      }
      else if ('0') {
        notif = 'Hapus';
      }
      if(confirm('Apakah anda yakin melakukan aksi '+notif+' pada data ini?'))
      {
        // ajax delete data from database
        var type2 =  "<?php echo base_url('admin/berita/delete_berita/')?>"+id+'/'+type;
          $.ajax({
            url : type2,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               console.log("Berhasil"+id+type)
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(id+' Error deleting data');
                console.log(type2);
            }
        });

      }
    }
</script>

<script type="text/javascript">
  $(document).ready( function () {
  } );

    function status_berita(id,$status)
    {
      if($status=='publish'){
  		  if(confirm('Are you sure publish this data? '+id)){
          $.ajax({
  		        url : "<?php echo base_url('admin/berita/publish_berita/') ?>"+id,
  		        type: "POST",
  		        dataType: "JSON",
  		        success: function(data)
  		        {
                location.reload();
  		        },
  		        error: function (jqXHR, textStatus, errorThrown)
  		        {
  		            alert(id+'Error publish data');
  		        }
  		    });
      	}
      }
      else if($status=='suspen'){
  	    if(confirm('Are you sure suspen this data?')){
          $.ajax({
              url : "<?php echo base_url('admin/berita/suspen_berita/') ?>"+id,
	            type: "POST",
	            dataType: "JSON",
	            success: function(data)
	            {
	              location.reload();
	            },
	            error: function (jqXHR, textStatus, errorThrown)
	            {
	                alert(id+'Error suspen data');
	            }
	        });
  	    }
      }
    }
  </script>
