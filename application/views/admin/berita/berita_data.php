<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        BERITA
        <small>Data Berita</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <?php $msg1 = $this->session->flashdata('msg1'); if((isset($msg1)) && (!empty($msg1))) { ?>
          <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <?php print_r($msg1); ?>
          </div>
        <?php } ?>

        <div class="box-header with-border">
          <i class="fa fa-list"></i><h3 class="box-title">Daftar Berita</h3>
        </div>
       <!-- /.box-header -->
        <div class="box-body">
          <div class="form-group">
            <a href="<?php echo base_url('admin/berita/tambah_berita') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Berita</a>
          </div>
          <table id="data_berita" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th><center>No.</center></th>
                <th><center>Judul</center></th>
                <th><center>Ketegori</center></th>
                <th><center>Author</center></th>
                <th><center>Tanggal Pembuatan</center></th>
                <th><center>Tanggal Perubahan</center></th>
                <th><center>Aksi</center></th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1;
              foreach ($data_berita as $db): ?>
                <tr>
                  <td><center><?php echo $no++; ?></center></td>
                  <td><?php echo $db['judul_berita']; ?></td>
                  <td><center><?php echo $db['nama_kategori']; ?></center></td>
                  <td><center><?php echo $db['author']; ?></center></td>
                  <td><center><?php echo $db['created_time']; ?></center></td>
                  <td><center><?php echo $db['updated_time']; ?></center></td>
                  <td>
                    <center>
                      <a onclick="delete_berita(<?php echo $db['id_berita'];?>,0)" class="btn btn-danger" ><i class="fa fa-trash"></i> Soft Delete </a>
                      <a href="<?php echo base_url('admin/berita/ubah_berita/'.$db['id_berita']) ?>" class="btn btn-info" ><i class="fa fa-pencil"></i>  Edit </a>

                      <?php if($db['status']==1){ ?>
                      <a onclick="status_berita(<?php echo $db['id_berita'];?>,'suspen')" class="btn btn-success" > <i class=" fa fa-check"></i>Published</a>
                      <?php }elseif($db['status']==0){ ?>
                      <a onclick="status_berita(<?php echo $db['id_berita'];?>,'publish')" class="btn btn-warning" >
                        <i class=" fa fa-remove"></i> Suspended</a>
                      <?php } ?>

                    </center>
                  </td>

                </tr>
              <?php endforeach ?>

            </tbody>
            <tfoot>
              <tr>
                <th><center>No.</center></th>
                <th><center>Judul</center></th>
                <th><center>Ketegori</center></th>
                <th><center>Author</center></th>
                <th><center>Tanggal Pembuatan</center></th>
                <th><center>Tanggal Perubahan</center></th>
                <th><center>Aksi</center></th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
