<!-- DataTables -->
<script src="<?php echo base_url('assets/back/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/back/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<script>
  $(function () {
    $('#data_kategori_berita').DataTable()
  })
</script>

<script type="text/javascript">
  $(document).ready( function () {
  } );

  var save_method; //for save method string
  var table;


  function add_kategori()
  {
    url = "<?php echo base_url('admin/berita/create_kategori_berita')?>";
    error="Proses tambah data kategori gagal!";
    $.ajax({
      url : url,
      type: "POST",
      data: $('#add_kategori').serialize(),
      dataType: "JSON",
      success: function(data)
      {
        //if success close modal and reload ajax table
        location.reload();// for reload a page
        alert('Data berhasil diproses!');  
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert(error);
      }
    });
  }
  
  function delete_kategori_berita(id)
  {
    if(confirm('Are you sure delete this data?'))
    {
      // ajax delete data from database
        $.ajax({
          url : "<?php echo base_url('admin/berita/delete_kategori_berita/')?>"+id,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
             location.reload();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert(id+'Error deleting data');
          }
      });

    }
  }

  function edit_kategori(id)
  {
    $('#kategori')[0].reset(); // reset form on modals

    url = "<?php echo base_url('admin/berita/get_kategori/')?>"+id;
    error="Proses ubah data kategori gagal!"

    $.ajax({
      url : url,
      type: "POST",
      data: $('#add_kategori').serialize(),
      dataType: "JSON",
      success: function(data)
      {
            $('[name="id_kategori_berita"]').val(data.id_kategori_berita);
            $('[name="nama_kategori"]').val(data.nama_kategori);
            $('[name="deskripsi_kategori"]').val(data.deskripsi_kategori);
            
            $('#edit_kategori').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Kategori Berita'); // Set title to Bootstrap modal title

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert(error);
      }
    });
  }  
   function save()
    {
      var url;
      var error;
      url = "<?php echo base_url('admin/berita/update_kategori/')?>";
      error="Proses ubah data kategori gagal!"
     
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#kategori').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#edit_kategori').modal('hide');
              location.reload();// for reload a page
              alert('Data berhasil diproses!');  
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(error);
            }
        });
    }
</script>

<!--  modal edit kategori -->

<div class="modal fade" id="edit_kategori" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">UBAH KATEGORI</h3>
      </div>
      <div class="modal-body ">
        <section>
        <form action="#" id="kategori" method="post">
          <div class="form-group has-feedback">
            <input type="hidden" class="form-control" name="id_kategori_berita">
            <label for="nama_kategori">Nama Kategori</label>
            <input type="text" class="form-control" id="nama_kategori" name="nama_kategori" placeholder="Nama Kategori">
          </div>

          <div class="form-group has-feedback">
            <label for="editor">Deskripsi Kategori</label><br>
            <textarea id="deskripsi_kategori" name="deskripsi_kategori" style="width: 100%;" rows="5" "></textarea>
          </div>
        </form>
        </section>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal-edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Edit Folder</h4>
      </div>
      <div class="modal-body">
        <form class="" action="<?php echo base_url() ?>admin/gallery/update_gallery" method="post">
          <div class="form-group has-feedback">
            <label for="">Nama Album</label><br>
            <input type="hidden" id="id" name="id">
            <input type="hidden" id="judul_lama" name="judul_lama" placeholder="Nama Album" class="form-control" id="edit_input">
            <input type="text" id="judul_baru" name="judul_baru" placeholder="Nama Album" class="form-control" id="edit_input">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-warning">Update</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
