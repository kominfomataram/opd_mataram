<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        BERITA
        <small>Data Kategori Berita</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php $msg1 = $this->session->flashdata('msg1'); if((isset($msg1)) && (!empty($msg1))) { ?>
        <div class="alert alert-success">
          <button class="close" data-dismiss="alert">x</button>
          <?php print_r($msg1); ?>
        </div>
      <?php } ?>

      <!-- Default box -->
      <div class="col-xs-4">
        <div class="box">
          <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">Tambah Kategori Berita</h3>
          </div>
         <!-- /.box-header -->
          <div class="box-body">
            <form action="#" id="add_kategori" method="post" >
              <div class="form-group has-feedback">
                <label for="nama_kategori">Nama Kategori</label>
                <input type="text" class="form-control" id="nama_kategori" name="nama_kategori" placeholder="Nama Kategori">
                <!-- <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span> -->
              </div>

              <div class="form-group has-feedback">
                <label for="editor">Deskripsi Kategori</label><br>
                <textarea id="deskripsi_kategori_berita" name="deskripsi_kategori_berita" style="width: 100%;" rows="5" placeholder="Silahkan Isi Berita Anda Di Sini"></textarea>
              </div>
            </form>
            <button type="button" id="btnSave" onclick="add_kategori()" class="btn btn-primary pull-right">Tambahkan</button>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>

      <div class="col-xs-8">
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">Daftar Kategori Berita</h3>
          </div>
         <!-- /.box-header -->
          <div class="box-body">
            <table id="data_kategori_berita" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th><center>No.</center></th>
                  <th><center>Nama Ketegori</center></th>
                  <th><center>Deskripsi</center></th>
                  <th><center>Aksi</center></th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1;
                foreach ($kategori_berita as $kb): ?>
                  <tr>
                    <td><center><?php echo $no++; ?></center></td>
                    <td><center><?php echo $kb['nama_kategori']; ?></center></td>
                    <td><center><?php echo $kb['deskripsi_kategori']; ?></center></td>
                    <td>
                      <center>
                        <a onclick="delete_kategori_berita(<?php echo $kb['id_kategori_berita'];?>)" class="btn btn-danger" > <i class="fa fa-trash"></i> Delete </a>
                        <a onclick="edit_kategori(<?php echo $kb['id_kategori_berita'];?>)" class="btn btn-warning " ><i class="fa fa-pencil"></i> Edit </a>
                      </center>
                    </td>

                  </tr>
                <?php endforeach ?>

              </tbody>
              <tfoot>
                <tr><th><center>No.</center></th>
                  <th><center>Nama Ketegori</center></th>
                  <th><center>Deskripsi</center></th>
                  <th><center>Aksi</center></th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
