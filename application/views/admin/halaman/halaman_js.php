<!-- CK Editor -->
<script src="<?php echo base_url('assets/back/bower_components/ckeditor_ext/ckeditor.js')?>"></script>
<script src="<?php echo base_url('assets/back/bower_components/vue/vue.min.js')?>"></script>

<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'editor1', {
      extraPlugins: 'imageuploader'
    });
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
    extraPlugins: 'imageuploader';
  })
</script>

<script>
var form_tambah_halaman = new Vue({
    el: '#form_tambah_halaman',
    data: {
      message: ''
    }
  })
</script>

<script type="text/javascript">
$( "#judul_web" ).keyup(function() {
  var coba = $("#judul_web").val().toLowerCase().replace(/ /g , "_");
  $("#kode_web").val(coba)
  console.log(coba);
});
</script>
