  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Halaman
        <small>Data - Data Halaman</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
         <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-book"></i><h3 class="box-title">Data Halaman</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <a href=" <?php echo base_url('admin/halaman/tambah_halaman') ?> " class="btn btn-info"><i class="fa fa-plus"></i> Tambah Halaman</a> <br> <br>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Judul</th>
                  <th>Link</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>

                  <?php
                      $no = 1;
                      foreach ($halaman as $h) {
                        # code...
                        echo '<tr>';
                          echo '<td>'.$no++.'</td>';
                          echo '<td>'.$h['judul_page'].'</td>';
                          echo '<td><a href="'.base_url($h['kode_page']).'" class="btn btn-info"> CEK LINK</a></td>';
                            echo '<td><center>';
                            if ($h['kode_page'] == 'berita' || $h['kode_page'] == 'gallery' || $h['kode_page'] == 'download' ||  $h['kode_page'] == '') {
                              # code...
                              echo '<button type="button" name="button" class="btn btn-default">DEFAULT PAGE</button>';
                            } else {
                              # code...
                              echo '<a href="'.base_url('admin/halaman/detail_halaman/'.$h['kode_page']).'" class="btn btn-small btn-success"><i class="fa fa-fw fa-pencil-square-o"></i> Detail</a>';
                              echo '<span>  </span>';
                              echo '<a href="'.base_url('admin/halaman/delete_halaman/'.$h['kode_page']).'" class="btn btn-small btn-danger"><i class="fa fa-fw fa-remove"></i> Hapus</a>';
                              echo '</center></td>';
                            }

                        echo '</tr>';
                      }
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Link</th>
                    <th>Aksi</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
