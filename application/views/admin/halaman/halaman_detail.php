<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Halaman
      <small>Data - Data Halaman</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
       <!-- Main row -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-default">
          <div class="box-header with-border">
            <i class="fa fa-book"></i><h3 class="box-title">Tambah Data Halaman</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <?php $msg = $this->session->flashdata('pesan'); if((isset($msg)) && (!empty($msg))) { ?>
              <div class="alert alert-danger">
                <button class="close" data-dismiss="alert">x</button>
                <?php print_r($msg); ?>
              </div>
            <?php } ?>
             <?php $msg = $this->session->flashdata('berhasil'); if((isset($msg)) && (!empty($msg))) { ?>
              <div class="alert alert-success">
                <button class="close" data-dismiss="alert">x</button>
                <?php print_r($msg); ?>
              </div>
            <?php } ?>

            <form action="<?php echo base_url('admin/halaman/update_halaman') ?>" method="post">

              <div class="form-group has-feedback">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" id="judul_web" name="judul" placeholder="Judul Halaman" v-model="message" placeholder="Masukkan Judul Halaman"  value="<?php echo $halaman[0]['judul_page'] ?>">
                <span class="glyphicon glyphicon-book form-control-feedback"></span>
                <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
              </div>

              <input type="hidden" name="id_page" value="<?php echo $halaman[0]['id_page'] ?>">

              <div class="form-group has-feedback">
                <label for="judul">Kode Halaman</label>
                <input type="text" id="kode_web" class="form-control" name="kode" placeholder="Kode Halaman Akan Di Generate Sendiri Dari Judul" value=""    disabled>
                <span class="fa fa-chain form-control-feedback"></span>
                <span class="text-white" style="color: red;"><?php echo form_error('email'); ?></span>
              </div>

              <div class="form-group has-feedback">
                <label for="judul">Kategori Halaman</label>
                <select class="form-control" name="kategori">
                  <option value="1">SUB BIDANG</option>
                  <option value="2">HALAMAN TAMBAHAN</option>
                  <?php $query = $this->db->query("SELECT * FROM page WHERE id_page <> '1' AND id_page <> '2' AND id_page <> '3' AND id_page <> '4' ")->result_array(); ?>
                  <?php foreach ($query as $q): ?>
                    <option value="<?php echo  $q['id_page'] ?>"> SUB HALAMAN ==> <b> <?php echo $q['judul_page'] ?></b></option>
                  <?php endforeach; ?>
                </select>
              </div>

              <div class="form-group has-feedback">
                <label for="judul">Isi Halaman</label>
                <textarea id="" name="editor1" rows="10" cols="80">
                                          <?php echo $halaman[0]['isi_page'] ?>
                </textarea>
              </div>

              <input type="hidden" name="tipe" value="default">

              <div class="col-xs-12">
                <button id="tombol_kembali" class="btn btn-danger pull-right btn-flat">Kembali</button>
                <button type="submit" class="btn btn-primary pull-right btn-flat">Update</button>
              </div>
              <!-- /.col -->
            </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row (main row) -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
