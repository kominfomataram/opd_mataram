<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Link Terkait
        <small>Trash Link Terkait</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">Trash Link Terkait</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Trash Link Terkait</h3>
        </div>
        <div class="box-body table-responsive">
          <?php $msg = $this->session->flashdata('pesan'); if((isset($msg)) && (!empty($msg))) { ?>
            <div class="alert alert-danger">
              <button class="close" data-dismiss="alert">x</button>
              <?php print_r($msg); ?>
            </div>
          <?php } ?>
           <?php $msg = $this->session->flashdata('berhasil'); if((isset($msg)) && (!empty($msg))) { ?>
            <div class="alert alert-success">
              <button class="close" data-dismiss="alert">x</button>
              <?php print_r($msg); ?>
            </div>
          <?php } ?>

          <div class="form-group">
            <a href="<?php echo base_url('admin/link_terkait') ?>" class="btn btn-primary"><i class="fa fa-list"></i> Data Link</a>
          </div>

          <table id="data_link" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>URL/Aalamat Link</th>
                <th>Gambar</th>
                <th>Teks Alternatif</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; foreach ($link as $l) { ?>
                <tr>
                <td><?php echo $no++ ?></td>
                <td>
                  <a href="<?php echo $l['link'] ?>"></i><?php echo $l['link'] ?></a>
                </td>
                <td>
                  <center><img style="height: 100px" src="<?php echo base_url('').$l['gbr'] ?>"></center>
                </td>
                <td><?php echo $l['text'] ?></td>
                <td><center>
                <a onclick="restore_link(<?php echo $l['id'];?>)" href="" class="btn btn-small btn-warning"><i class="fa fa-fw fa-remove"></i> Kembalikan</a>
                <span>  </span>
                <a onclick="delete_link(<?php echo $l['id'];?>,1)" href="" class="btn btn-small btn-danger"><i class="fa fa-fw fa-remove"></i> Hapus</a>
                </center></td>
                </tr>
              <?php   } ?>
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>URL/Aalamat Link</th>
                <th>Gambar</th>
                <th>Teks Alternatif</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
