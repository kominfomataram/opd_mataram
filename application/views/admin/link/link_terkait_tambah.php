<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Link Terkait
        <small>Tambah Data Link Terkait</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Link Terkait</a></li>
        <li class="active">Tambah Link Terkait</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Link Terkait</h3>
        </div>
        <div class="box-body">
          <!-- Main row -->
       <div class="row">
         <div class="col-md-12">
           <div class="box box-default">
             <div class="box-header with-border">
               <i class="fa fa-file"></i><h3 class="box-title">Tambah Data</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body">
               <?php $msg = $this->session->flashdata('pesan'); if((isset($msg)) && (!empty($msg))) { ?>
                 <div class="alert alert-danger">
                   <button class="close" data-dismiss="alert">x</button>
                   <?php print_r($msg); ?>
                 </div>
               <?php } ?>
                <?php $msg = $this->session->flashdata('berhasil'); if((isset($msg)) && (!empty($msg))) { ?>
                 <div class="alert alert-success">
                   <button class="close" data-dismiss="alert">x</button>
                   <?php print_r($msg); ?>
                 </div>
               <?php } ?>

               <form action="<?php echo base_url() ?>admin/link_terkait/link_terkait_create" method="post" enctype="multipart/form-data" onsubmit="return confirm('Apakah anda yakin ingin menyimpan data tersebut?');">
                 <div class="form-group has-feedback">
                   <input type="text" class="form-control" name="link" placeholder="URL/Alamat link">
                   <span class="glyphicon glyphicon-link form-control-feedback"></span>
                 </div>

                 <div class="form-group has-feedback">
                   <input type="text" class="form-control" name="text" placeholder="Teks Alternatif">
                   <span class="glyphicon glyphicon-pencil form-control-feedback"></span>
                 </div>

                 <div class="form-group has-feedback">
                   <input type="file" class="form-control" name="gbr" placeholder="Silahkan Upload gambar">
                   <span class="glyphicon glyphicon-picture form-control-feedback"></span>
                 </div>

                 <div class="col-xs-12">
                   <button type="submit" class="btn btn-primary pull-right btn-flat">Tambahkan</button>
                 </div>
                 <!-- /.col -->
               </form>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row (main row) -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
