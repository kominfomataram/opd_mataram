<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        DOWNLOAD
        <small>Data Download</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <?php $msg1 = $this->session->flashdata('msg1'); if((isset($msg1)) && (!empty($msg1))) { ?>
          <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <?php print_r($msg1); ?>
          </div>
        <?php } ?>

        <div class="box-header with-border">
          <i class="fa fa-trash "></i><h3 class="box-title">Daftar Trash Download</h3>
        </div>
       <!-- /.box-header -->
        <div class="box-body">

          <table id="data_berita" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th><center>No.</center></th>
                <th><center>Judul</center></th>
                <th><center>URL File</center></th>
                <th><center>Author</center></th>
                <th><center>Aksi</center></th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1;
              foreach ($download as $d): ?>
                <tr>
                  <td><center><?php echo $no++; ?></center></td>
                  <td><?php echo $d['judul']; ?></td>
                  <td><center><?php echo $d['url_file']; ?></center></td>
                  <td><center><?php echo $d['admin_username']; ?></center></td>
                  <td>
                    <center>
                      <a onclick="delete_file(<?php echo $d['id_download'];?>,1)" class="btn btn-danger"><i class="fa fa-times"></i> Hapus </a>
                      
                      <a onclick="restore_file(<?php echo $d['id_download'];?>)"  class="btn btn-warning"><i class="fa fa-arrow-circle-o-up"></i> Return </a>
                    </center>
                  </td>

                </tr>
              <?php endforeach ?>

            </tbody>
            <tfoot>
              <tr>
                <th><center>No.</center></th>
                <th><center>Judul</center></th>
                <th><center>URL File</center></th>
                <th><center>Author</center></th>
                <th><center>Aksi</center></th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
