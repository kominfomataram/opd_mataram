<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Download
        <small>Tambah File Download</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Download</a></li>
        <li class="active">Tambah File</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <?php $msg1 = $this->session->flashdata('msg1'); if((isset($msg1)) && (!empty($msg1))) { ?>
          <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <?php print_r($msg1); ?>
          </div>
        <?php } ?>
        <div class="box-header with-border">
          <h3 class="box-title">Tambah File Download</h3>
        </div>
        <div class="box-body">
          <a href=" <?php echo base_url('admin/download/tambah_download') ?> " class="btn btn-info"><i class="fa fa-plus"></i> Tambah File</a>
        </div>
        <div class="box-body">

          <table id="data_download" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama File</th>
                <th>URL File</th>
                <th>Author</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                  <?php $no = 1; foreach ($download as $d) { ?>
                    <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $d['judul'] ?></td>
                    <td>
                      <center><a class="btn btn-success" target="_blank" href="<?php echo base_url('media/file/').$d['url_file'] ?>"><i class="fa fa-fw fa-download"></i> Download</a></center>
                    </td>
                    <td><?php echo $d['admin_username'] ?></td>
                    <td><center>
                    <a href="<?php echo base_url('admin/download/detail_download/'.$d['id_download']) ?>" class="btn btn-small btn-warning"><i class="fa fa-fw fa-pencil"></i> Update File</a>
                    <span>  </span>
                    <a onclick="delete_file(<?php echo $d['id_download'];?>,0)" href="" class="btn btn-small btn-danger"><i class="fa fa-fw fa-remove"></i> Hapus</a>
                    </center></td>
                    </tr>
                  <?php   } ?>
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Nama File</th>
                <th>URL File</th>
                <th>Author</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Data - Data File
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
