
<!-- DataTables -->
<script src="<?php echo base_url('assets/back/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/back/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<script>
  $(function () {
    $('#data_download').DataTable()
  })
</script>

<script type="text/javascript">
  $(document).ready( function () {
  } );

    function delete_file(id , type)
    {
     var notif ='' ;
      if (type == '1') {
        notif = 'Hapus Permanen';
      }
      else if ('0') {
        notif = 'Hapus';
      }
      if(confirm('Apakah anda yakin melakukan aksi '+notif+' pada data ini?'))
      {
        // ajax delete data from database
        var type2 =  "<?php echo base_url('admin/download/delete_file/')?>"+id+'/'+type;
          $.ajax({
            url : type2,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               console.log("Berhasil"+id+type)
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(id+' Error deleting data');
                console.log(type2);
            }
        });

      }
    }

    function restore_file(id)
    {
      if(confirm('Are you sure to restore this data?'))
      {
        // ajax delete data from database
        var type2 =  "<?php echo base_url('admin/download/restore_file/')?>"+id;
          $.ajax({
            url : type2,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               console.log("Berhasil"+id)
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(id+' Error deleting data');
                console.log(type2);
            }
        });

      }
    }
</script>
