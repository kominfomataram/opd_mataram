  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Web Master
        <small>Daftar Aktivitas System</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
         <!-- Main row -->
      <div class="row">

        <div class="col-md-12">
           <div class="box">
            <div class="box-header with-border">
              <i class="fa fa-list"></i><h3 class="box-title">Daftar Log System</h3>
            </div>
           <!-- /.box-header -->
            <div class="box-body">
              <table id="list_admin" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Aktivitas</th>
                    <th>Waktu</th>
                    <th>IP Address</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $query = $this->db->query('SELECT * FROM log ORDER BY id DESC')->result_array();
                    $no = 1;
                    foreach ($query as $a) {
                      # code...
                      echo '<tr>';
                      echo '<td>'.$no++.'</td>';
                      echo '<td>'.$a['username'].'</td>';
                      echo '<td>'.$a['aktivitas'].'</td>';
                      echo '<td>'.$a['date_timestamp'].'</td>';
                      echo '<td>'.$a['ip_address'].'</td>';
                      echo '</tr>';
                    }

                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Aktivitas</th>
                    <th>Waktu</th>
                    <th>IP Address</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
