<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo title_website() ?> | Registrasi</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="#"><b>JUDUL</b>-WEBSITE</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Silahkan isi data akun anda terlebih dahulu!</p>

    <?php $msg = $this->session->flashdata('pesan'); if((isset($msg)) && (!empty($msg))) { ?>
    <div class="alert alert-danger">
     <button class="close" data-dismiss="alert">x</button>
     <?php print_r($msg); ?>
    </div>
    <?php } ?>

    <form action="<?php echo base_url() ?>index.php/admin/Admin/isi_data_akun" enctype="multipart/form-data" method="post">
      <div class="form-group">
        <label for="foto">Foto Profil</label>
        <input required type="file" name="foto" id="foto1">
        <p class="help-block">Example block-level help text here.</p>
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
        <span class="glyphicon  glyphicon-user form-control-feedback"></span>
        <span class="text-whitw" style="color: red;"><?php echo form_error('nama'); ?></span>
      </div>

      <div class="form-group has-feedback">
        <input type="number" class="form-control" name="no_tlp" placeholder="Nomor Telpon">
        <span class="glyphicon glyphicon glyphicon-earphone form-control-feedback"></span>
        <span class="text-whitw" style="color: red;"><?php echo form_error('no_tlp'); ?></span>
      </div>

      <label> Jenis Kelamin:</label>
      <div class="form-group has-feedback">
        <div class="radio-inline">
          <label>
            <input type="radio" name="jenkel" id="jenkel1" value="Laki-Laki" checked> Laki-Laki
          </label>
        </div>
        <div class="radio-inline">
          <label>
            <input type="radio" name="jenkel" id="jenkel2" value="Perempuan" > Perempuan
          </label>
        </div>
        <span class="text-whitw" style="color: red;"><?php echo form_error('jenkel'); ?></span>
      </div>

      <div class="form-group has-feedback">
        <textarea class="form-control" rows="3" name="alamat" placeholder="Alamat"></textarea>
        <span class="glyphicon glyphicon-home form-control-feedback"></span>
        <span class="text-whitw" style="color: red;"><?php echo form_error('alamat'); ?></span>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-flat pull-right">selesai</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!-- <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a>
    </div> -->
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url() ?>assets/back/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url() ?>assets/back/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url() ?>assets/back/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
