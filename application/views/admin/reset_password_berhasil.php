<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo title_website() ?> | RESET PASSWORD</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/dist/css/skins/_all-skins.min.css">
   <!-- Morris chart -->
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/bower_components/morris.js/morris.css">
   <!-- jvectormap -->
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/bower_components/jvectormap/jquery-jvectormap.css">
   <!-- Date Picker -->
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/bower_components/bootstrap-daterangepicker/daterangepicker.css">
   <!-- bootstrap wysihtml5 - text editor -->
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/back/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<style media="screen">
  /* .background-baru{
    background-image: url(<?php echo base_url('assets/skpd/img/logo/logo.png'); ?>);
    background-image: url(<?php echo base_url('media/static/'.static_data('logo_instansi')); ?>);
    background-repeat: repeat-y;
    background-repeat: space;
    background-size: 50px;
  } */
</style>

</head>

<body class="login-page background-baru">
  <div class="login-box">
    <div class="login-logo">
        <img src="<?php echo base_url('assets/skpd/img/logo/logo.png'); ?>" style="height:100px; width:auto">
        <img src="<?php echo base_url('media/static/'.static_data('logo_instansi')); ?>" style="height:100px; width:auto">
        <img alt="" src="<?php echo base_url('assets/skpd/img/logo/logo_kominfo_kota_tb.png')  ?>" style="height:100px; width:auto;">
    </div><!-- /.login-logo -->
    <h3><b><center>CONTROL PANEL</b><br>OPD - Organisasi Perangkat Daerah</a> <b> <br> <?php echo title_website() ?> </b></center></h3>
    <div class="login-box-body">
      <center><h2><?php echo $pesan ?></h2>      
      </center>
      <a href="<?php echo base_url('admin/Login') ?>" class="text-center">Kembali ke Halaman Login</a>
    </div>
    <!-- <a href="<?php echo base_url() ?>index.php/Forget/">I forgot my password</a><br> -->
  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<?php
  $this->load->view('admin/js/default');
?>
