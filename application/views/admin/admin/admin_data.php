  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Web Master
        <small>Daftar Akun Admin</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
         <!-- Main row -->
      <div class="row">

        <div class="col-md-12">
           <div class="box">
            <div class="box-header with-border">
              <i class="fa fa-list"></i><h3 class="box-title">Daftar Admin</h3>
            </div>

            <?php $bhs = $this->session->flashdata('berhasil_reset_password'); ?>
            <?php if ($bhs != null): ?>
              <div class="alert alert-info alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-info"></i> Berhasil !</h4>
                  Password Di Reset Secara Default
              </div>
            <?php else: ?>

            <?php endif; ?>

           <!-- /.box-header -->
            <div class="box-body">

            <?php $msg = $this->session->flashdata('berhasil'); if((isset($msg)) && (!empty($msg))) { ?>
              <div class="alert alert-success">
                <button class="close" data-dismiss="alert">x</button>
                <?php print_r($msg); ?>
              </div>
            <?php } ?>

              <a href=" <?php echo base_url('admin/Web_master/tambah_admin') ?> " class="btn btn-info"><i class="fa fa-plus"></i> Tambah Admin</a> <br><br>

              <table id="list_admin" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Nama</th>
                    <th>Telpon</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Jenis Kelamin</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; foreach ($admin as $a) {
                      # code...
                      echo '<tr>';
                      echo '<td>'.$no++.'</td>';
                      echo '<td>'.$a['username'].'</td>';
                      echo '<td>'.$a['nama'].'</td>';
                      echo '<td>'.$a['telpon'].'</td>';
                      echo '<td>'.$a['email'].'</td>';
                      echo '<td>'.$a['alamat'].'</td>';
                      if ($a['jenis_kelamin'] == '1') {
                        echo '<td>Laki - Laki</td>';
                      }
                      else {
                        echo '<td>Perempuan</td>';
                      }
                      if ($a['status'] == '1') {
                        # code...
                        echo '<td><a href="'.base_url('admin/Admin/ganti_status_admin/'.$a['username'].'/1').'"><span class="btn btn-success">Aktif</span></a></td>';
                      }
                      else {
                        echo '<td><a href="'.base_url('admin/Admin/ganti_status_admin/'.$a['username'].'/0').'"><span class="btn btn-danger">Tidak Aktif</span></a></td>';
                      }
                      echo '<td><center>';
                      echo '<a href="'.base_url('admin/reset_default_password/'.$a['username']).'" class="btn btn-info"><span class="fa fa-at"></span> Reset Password</a>';
                      echo '<a href="'.base_url('admin/update_admin/'.$a['username']).'" class="btn btn-warning"><span class="fa fa-pencil"></span> Update</a>';
                      echo '<a href="#" class="btn btn-danger"><span class="fa fa-times"></span> Delete</a>';
                      echo '</td></center>';
                      echo '</tr>';
                    }

                  ?>
                </tbody>

                

                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Nama</th>
                    <th>Telpon</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Jenis Kelamin</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
