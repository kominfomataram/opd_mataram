<div class="row">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Carousel indicators -->
          <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
              <div id="carousel-list">
              </div>
          </ol>
          <!-- Wrapper for carousel items -->
          <div class="carousel-inner" id="carousel-banner">
              <div class="item active">
                  <img src="<?php echo base_url('/media/static/'.static_data('home_gbr_1')) ?>" alt="First Slide" style="display:block;width:100%; height :400px; object-fit: cover;">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('/media/static/'.static_data('home_gbr_2')) ?>" alt="First Slide" style="display:block;width:100%; height :400px; object-fit: cover;">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('/media/static/'.static_data('home_gbr_3')) ?>" alt="First Slide" style="display:block;width:100%; height :400px; object-fit: cover;">
              </div>
              <div id="carousel-banner">
              </div>
          </div>
          <!-- Carousel controls -->
          <a class="carousel-control left" href="#myCarousel" data-slide="prev">
              <span class="fa fa-left"></span>
          </a>
          <a class="carousel-control right" href="#myCarousel" data-slide="next">
              <span class="fa fa-right"></span>
          </a>
        </div>
    </div>

<!-- Start Boxed Section -->
<div class="boxed">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-8">
        <h3><?php echo title_website(); ?></h3>
      </div>
    </div>
  </div>
</div>
<!-- End Boxed Section -->

<br>
<br>

<?php $this->load->view('front/template/link_terkait'); ?>

<?php $this->load->view('front/template/berita_terbaru'); ?>

<?php $this->load->view('front/template/gallery_terbaru'); ?>

<?php $this->load->view('front/template/bidang') ?>

<?php $this->load->view('front/template/file_terbaru') ?>
