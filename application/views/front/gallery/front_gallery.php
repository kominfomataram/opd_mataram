<br>
  <div class="row">
    <div class="col-md-12">
      <center><h4 class="big-title">Gallery Detail</h4></center>
    <!-- Start ISI -->
    <!-- Start Portfolio Section -->
    <section id="portfolio" class="section">
      <!-- Portfolio Recent Projects -->
      <div id="portfolio-list">
    <!-- Start ISI -->
    <ul>
      <?php
        #query menampilkan thumbnail album
        $gallery = $this->db->query("SELECT foto.nama , foto.nama_album , album.id_album , album.judul FROM foto LEFT JOIN album ON album.judul = foto.nama_album GROUP BY album.judul")->result_array();
       ?>
      <?php foreach ($gallery as $g) { ?>
        <!-- <li><h2><a href="<?php echo base_url('gallery/').$g['id_album'] ?>"><?php echo $g['judul'] ?></a></h2></li> -->
        <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 mix">
          <div class="portfolio-item">
            <img src="<?php echo base_url('media/gallery/').$g['judul'].'/'.$g['nama'] ?>" alt="" style="height:250px; width:700px;" />
            <div class="overlay">
              <div class="icon">
                <a href="<?php echo base_url('media/gallery/').$g['judul'].'/'.$g['nama'] ?>" class="lightbox"><i class="icon-picture right"></i></a>
              </div>
            </div>
            <div class="content">
              <h3><a href="<?php echo base_url('gallery/').$g['judul'] ?>"><?php echo $g['judul'] ?></a></h3>
            </div>
          </div>
        </div>
      <?php  } ?>
    </ul>
    <!-- End ISI -->
  </div>
  <!-- End Portfolio  -->
</section>
<!-- End Portfolio Section -->

<!-- End ISI -->

</div>
</div>

<br>
