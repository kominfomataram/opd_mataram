<br>
  <div class="row">
    <div class="col-md-12">
      <center><h2 class="big-title">Gallery Detail</h2></center>
    <!-- Start ISI -->
    <!-- Start Portfolio Section -->
    <section id="portfolio" class="section">
      <!-- Portfolio Recent Projects -->
      <div id="portfolio-list">
          <?php foreach ($gambar as $gbr ): ?>
          <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 mix">
            <div class="portfolio-item">
              <img src="<?php echo base_url('media/gallery/').$gbr['nama_album'].'/'.$gbr['nama'] ?>" alt="image" />
              <div class="overlay">
                <div class="icon">
                  <a href="<?php echo base_url('media/gallery/').$gbr['nama_album'].'/'.$gbr['nama'] ?>" class="lightbox"><i class="icon-magnifier right"></i></a>
                </div>
              </div>
              <div class="content">
                <h3><?php echo $gbr['nama'] ?></h3>
              </div>
            </div>
          </div>
          <?php endforeach ?>
      </div>
      <!-- End Portfolio  -->
    </section>
    <!-- End Portfolio Section -->

    <!-- End ISI -->

  </div>
</div>

<br>
