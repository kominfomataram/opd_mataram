<br>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h2 class="big-title"><?php echo $page[0]['judul_page'] ?></h2>

    <!-- Start ISI -->
    <table class="table">
      <tr>
        <thead>
          <th>No</th>
          <th>Nama File</th>
        </thead>
        <tbody>
          <?php $no = 1; foreach ($download as $d) { ?>
            <tr>
              <td><?php echo $no++ ?></td>
              <td><?php $icon = explode("." , $d['url_file']); $format_file ?>

                <?php
                    if ($icon[1] == 'jpg') {
                      $format_file = 'assets/skpd/img/icon/jpg.png';
                    } elseif ($icon[1] == 'pdf') {
                      $format_file = 'assets/skpd/img/icon/pdf.png';
                    } elseif ($icon[1] == 'ppt') {
                      $format_file = 'assets/skpd/img/icon/ppt.png';
                    } elseif ($icon[1] == 'doc') {
                      $format_file = 'assets/skpd/img/icon/doc.png';
                    } elseif ($icon[1] == 'xls') {
                      $format_file = 'assets/skpd/img/icon/xls.png';
                    } elseif ($icon[1] == 'zip') {
                      $format_file = 'assets/skpd/img/icon/zip.png';
                    }
                ?>
                <img src="<?php echo base_url($format_file); ?>" alt="" style="height:50px; width:auto">
                <a target="_blank" href="<?php echo base_url('media/file/'.$d['url_file']) ?>"><?php echo strtoupper($d['judul']) ?></a>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </tr>
    </table>
    <!-- End ISI -->

    </div>
  </div>
</div>

<br>
