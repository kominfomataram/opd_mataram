<br>
<div class="container">
  <div class="row">
    <div class="col-md-9">
      <h2 class="big-title"><?php echo $page[0]['judul_berita'] ?> </h2>
      <div class="pull-left">
        <p>Di posting oleh : <?php echo $page[0]['nama']  ?> - Tanggal : <?php echo $page[0]['created_time']  ?> - Last Update  : <?php echo $page[0]['updated_time'] ?></p>
      </div>

    <br>
    <br>
    <!-- Start ISI -->
    <p>Isi Berita
      <?php echo $page[0]['isi_berita'] ?>
    </p>
    <!-- End ISI -->

    </div>
    <div class="col-md-3">
      <h2 class="big-title">Berita Terbaru</h2>
      <?php
        $query = $this->db->query("SELECT * FROM berita WHERE status = '1' ORDER BY id_berita DESC LIMIT 3")->result_array();
      ?>
      <div class="information">
        <div class="contact-datails">
          <?php foreach ($query as $q) { ?>
            <p><i class="fa fa-newspaper-o"></i> <b><?php echo strtoupper($q['judul_berita']) ?></b></p>
            <p><?php echo substr($q['isi_berita'] , 0 , 50) ?></p><a href="<?php echo base_url('berita/berita_detail/').$q['kode_berita'] ?>">Baca Selengkapnya</a><br><br>
          <?php } ?>
        </div>
      </div>

    </div>
  </div>
</div>

<br>
