<!-- Berita Terbaru Section -->
<div class="project section" style="padding-top: 0 !important;">
        <div class="container">
          <!-- Start Heading -->
          <div class="heading">
            <div class="section-title">Berita <span>Terbaru <?php echo title_website() ?></span></div>
          </div>
          <!-- End Heading -->

          <!-- Some Text -->
          <p class="description text-center">
            Berikut adalah berita terbaru dari <?php echo title_website() ?>
          </p>
          <div class="row">
            <!-- Start Recent Projects Carousel -->
            <div id="projects-carousel-3" class="touch-carousel">
              <?php
              foreach ($berita as $b) {
              ?>
              <div class="col-md-12">
                <div class="projects-box item">
                  <div class="projects-thumb">
                    <a href="#">
                      <img src="assets/img/news/img1.jpg" alt="">
                    </a>
                  </div>
                  <div class="projects-content">
                    <h4><?php echo $b['judul_berita'] ?></h4>
                    <div class="recent-meta">
                      <span class="date"><i class="fa fa-file-text-o"></i><?php echo $b['created_time'] ?></span>
                    </div>
                    <p class="projects-desc"><?php echo preg_replace("/<img[^>]+\>/i", " ", substr($b['isi_berita'] , 0 , 300)); ?></p>
                    <a href="<?php echo base_url('berita/').$b['kode_berita'] ?>" class="btn btn-effect">Baca Selengkapnya<i class="icon-arrow-right"></i></a>
                  </div>
                </div>
              </div>
              <?php
              } //batas foreach
              ?>
            </div>
            <!-- End Recent Projects Carousel -->
          </div>
        </div>
        <!-- .container -->
      </div>
      <!-- End Project Section  -->
