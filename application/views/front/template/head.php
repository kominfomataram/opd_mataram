<head>
  <!-- Basic -->
  <title>
    <?php

    if ($judul == null) {
      echo title_website();
    }
    else {
      echo title_website().' | '.strtoupper($judul);
    }


    ?>
  </title>

  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('/media/favicon/favicon.ico') ?>" />

  <!-- Define Charset -->
  <meta charset="utf-8">
  <!-- Responsive Metatag -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- Page Description and Author -->
  <meta name="description" content="ConBiz - Responsive HTML5 Template">
  <meta name="author" content="Grayrids">

  <!-- Bootstrap CSS  -->
  <link rel="stylesheet" href="<?php echo base_url('assets/skpd/') ?>css/bootstrap.min.css" type="text/css" media="screen">
  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="<?php echo base_url('assets/skpd/') ?>fonts/font-awesome.min.css" type="text/css" media="screen">
  <!-- Icon -->
  <link rel="stylesheet" href="<?php echo base_url('assets/skpd/') ?>fonts/simple-line-icons.css" type="text/css" media="screen">
  <!-- ConBiz Iocn -->
  <link rel="stylesheet" href="<?php echo base_url('assets/skpd/') ?>fonts/flaticon.css" type="text/css" media="screen">
  <!-- rs style -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/skpd/') ?>css/settings.css" media="screen">
  <!-- ConBiz CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/skpd/') ?>css/main.css" media="screen">
  <!-- Responsive CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/skpd/') ?>css/responsive.css" media="screen">
  <!-- Css3 Transitions Styles  -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/skpd/') ?>css/animate.css" media="screen">
  <!-- Slicknav  -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/skpd/') ?>css/slicknav.css" media="screen">

  <!-- Selected Preset -->
  <?php $warna = $this->db->query('SELECT warna FROM static')->row()->warna; ?>
  <?php $warna_baru = str_replace("#","",$warna) ?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/skpd/') ?>css/colors/custom.php?warna=<?php echo $warna_baru ?>" media="screen" />

  <!--[if IE 8]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
