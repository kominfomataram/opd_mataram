<!-- Berita Terbaru Section -->
<div class="project section" style="padding-bottom: 0 !important;">
        <div class="container">
          <!-- Start Heading -->
          <div class="heading">
            <div class="section-title">Link Terkait<span> <?php echo title_website() ?></span></div>
          </div>
          <!-- End Heading -->

          <!-- Some Text -->
          <p class="description text-center">
            Berikut adalah link yang terkait dengan <?php echo title_website() ?>
          </p>
          <?php $link =  $this->db->query("SELECT * FROM link_terkait WHERE softdelete_status = '0' ")->result_array() ?>

          <div class="row">
            <!-- Start Recent Projects Carousel -->
            <div id="projects-carousel" class="touch-carousel">
              <?php
              foreach ($link as $l) {
              ?>
              <div class="col-md-12">
                <div class="projects-box item">
                  <div class="projects-thumb">
                    <a href="#">
                      <img src="" alt="">
                    </a>
                    <a href="<?php echo $l['link'] ?>"><img style="height: 100px" src="<?php echo base_url('').$l['gbr'] ?>" alt="<?php echo base_url('').$l['text'] ?>"></a>
                  </div>
                  <div class="projects-content">

                  </div>
                </div>
              </div>
              <?php
              } //batas foreach
              ?>
            </div>
            <!-- End Recent Projects Carousel -->
          </div>
        </div>
        <!-- .container -->
      </div>
      <!-- End Project Section  -->
