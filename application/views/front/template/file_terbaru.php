<!-- Start Bidang Section -->
      <div class="team section">
        <div class="container">
          <!-- Start Heading -->
          <div class="heading">
            <div class="section-title">File Terbaru Pada  <span><?php echo title_website() ?></span></div>
          </div>
          <!-- End Heading -->
          <!-- Some Text -->
          <p class="description text-center">
            Berikut adalah File - File Terbaru Yang Terdapat Pada <?php echo title_website() ?>
          </p>
          <div class="row">
            <div id="client-logo" class="touch-carousel">
              <?php $query =  $this->db->query("SELECT * FROM download WHERE softdelete_status = '0' ORDER BY id_download DESC LIMIT 6")->result_array() ?>
              <?php  foreach ($query as $q) { ?>
                <?php $icon = explode("." , $q['url_file']); $format_file; ?>
                
                  <?php
                      if ($icon[1] == 'jpg') {
                        $format_file = 'assets/skpd/img/icon/jpg.png';
                      } elseif ($icon[1] == 'pdf') {
                        $format_file = 'assets/skpd/img/icon/pdf.png';
                      } elseif ($icon[1] == 'ppt') {
                        $format_file = 'assets/skpd/img/icon/ppt.png';
                      } elseif ($icon[1] == 'doc') {
                        $format_file = 'assets/skpd/img/icon/doc.png';
                      } elseif ($icon[1] == 'xls') {
                        $format_file = 'assets/skpd/img/icon/xls.png';
                      } elseif ($icon[1] == 'zip') {
                        $format_file = 'assets/skpd/img/icon/zip.png';
                      }
                      else
                      {
                        $format_file = 'assets/skpd/img/icon/jpg.png';
                      }
                  ?>

              <div class="client-logo item" >
                <div class="client-item">
                  <a href="<?php echo base_url('media/file/'.$q['url_file']) ?>">
                  <img src="<?php echo base_url($format_file) ?>" alt="" />
                </a><br>
                  <h3><?php echo strtoupper($q['judul']) ?></h3>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>


        </div>
      </div>
      <!-- Start Bidang Section -->
