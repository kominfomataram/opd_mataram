<!-- Start  Logo & Navigation  -->
<div class="navbar navbar-default navbar-top" role="navigation" data-spy="affix" data-offset-top="50">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <!-- Stat Toggle Nav Link For Mobiles -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!-- End Toggle Nav Link For Mobiles -->
      <div>
        <br>
        <a class="navbar-brand" href="<?php echo base_url() ?>">
          <img src="<?php echo base_url('assets/skpd/img/logo/logo.png'); ?>" style="height:70px; width:auto;">
        </a>
        <?php if (static_data('logo_instansi') == "Data Tidak Ada"): ?>

        <?php else: ?>
          <a class="navbar-brand" href="<?php echo base_url() ?>">
            <img src="<?php echo base_url('media/static/'.static_data('logo_instansi')); ?>" style="height:70px; width:auto;">
          </a>
        <?php endif; ?>
      </div>
    </div>
    <!--  Brand and toggle menu for mobile ends  -->

    <div class="navbar-collapse collapse">
      <!-- Stat Search -->
      <div class="search-side">
        <a class="show-search">
        <i class="icon-magnifier"></i>
        </a>
      </div>
      <!-- Form for navbar search area -->
      <form class="full-search">
        <div class="container">
          <div class="row">
            <form action="<?php echo base_url('pencarian/') ?>" method="post">
              <input class="form-control" type="text" name="pencarian" placeholder="Pencarian">
            </form>
            <a class="close-search">
            <span class="fa fa-times fa-2x">
            </span>
            </a>
          </div>
        </div>
      </form>
      <!-- Search form ends -->

      <ul class="nav navbar-nav navbar-right">
                <li class="drop">
                  <a class="active" href="<?php echo base_url() ?>" style="padding-top: 8px; padding-bottom: 8px;">
                  BERANDA
                  </a>
                </li>
                <li>
                  <a href="<?php echo base_url('berita/berita_list') ?>" style="padding-top: 8px; padding-bottom: 8px;">
                  BERITA
                  </a>
                </li>
                <li class="drop">
                  <a href="<?php echo base_url('gallery') ?>" style="padding-top: 8px; padding-bottom: 8px;">
                  GALLERY
                  </a>
                </li>
                <li class="drop">
                  <a href="<?php echo base_url('download') ?>" style="padding-top: 8px; padding-bottom: 8px;">DOWNLOAD</a>
                </li>
                <li class="drop">
                  <a href="#" style="padding-top: 8px; padding-bottom: 8px;">BIDANG</a>
                  <?php $query =  $this->db->query("SELECT * FROM page WHERE kategori = '1'")->result_array() ?>
                  <ul class="dropdown">
                    <?php  foreach ($query as $q) { ?>
                      <li><a href="<?php echo base_url($q['kode_page']) ?>"><?php echo strtoupper($q['judul_page']) ?></a></li>
                    <?php } ?>
                  </ul>
                </li>

                <?php $query =  $this->db->query("SELECT * FROM page WHERE kategori = '2'")->result_array() ?>
                <?php  foreach ($query as $q) { ?>
                  <li class="drop">
                  <?php if ($q['kategori'] != null && $q['id_page'] != '1' && $q['id_page'] != '2' && $q['id_page'] != '3' && $q['id_page'] != '4'): ?>
                      <a href="<?php echo base_url($q['kode_page']) ?>" style="padding-top: 8px; padding-bottom: 8px;"><?php echo strtoupper($q['judul_page']) ?></a>
                      
                      <?php $query_sub =  $this->db->query("SELECT * FROM page WHERE kategori =".$q['id_page'])->result_array() ?>
                      <ul class="dropdown">
                      <?php foreach ($query_sub as $qs): ?>
                        
                        <li><a href="<?php echo base_url($qs['kode_page']) ?>"><?php echo strtoupper($qs['judul_page']) ?></a></li>

                        <!-- <?php $query_sub2 =  $this->db->query("SELECT * FROM page WHERE kategori =".$q['id_page'])->result_array() ?>
                        <?php foreach ($query_sub2 as $qs2): ?>
                          <ul class="dropdown">
                          <li><a href="<?php echo base_url($qs2['kode_page']) ?>"><?php echo strtoupper($qs2['judul_page']) ?></a></li>
                          </ul>
                        <?php endforeach; ?> -->
                        
                        
                      <?php endforeach; ?>
                      </ul>
                  </li>
                  <?php else: ?>
                      <li><a href="<?php echo base_url($q['kode_page']) ?>" style="padding-top: 8px; padding-bottom: 8px;"><?php echo strtoupper($q['judul_page']) ?></a></li>
                  <?php endif; ?>

                <?php } ?>
              </ul>

    </div>
  </div>

  <?php $this->load->view('front/template/navigation_mobile'); ?>

</div>
<!-- End Header Logo & Navigation -->
