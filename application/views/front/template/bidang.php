<!-- Start Bidang Section -->
      <div class="team section">
        <div class="container">
          <!-- Start Heading -->
          <div class="heading">
            <div class="section-title">Bidang PADA  <span><?php echo title_website() ?></span></div>
          </div>
          <!-- End Heading -->
          <!-- Some Text -->
          <p class="description text-center">
            Berikut adalah Nama - Nama Bidang Yang Terdapat Pada <?php echo title_website() ?>
          </p>
          <div class="row">
            <?php $query =  $this->db->query("SELECT * FROM page WHERE kategori = '1'")->result_array() ?>
            <ul class="dropdown">
              <?php  foreach ($query as $q) { ?>
                <div class="col-md-3 col-sm-6">
                  <div class="team-member">
                    <div class="info">
                      <h4><a href="<?php echo base_url($q['kode_page']) ?>" style="color:white"><?php echo strtoupper($q['judul_page']) ?></a></h4>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </ul>
          </div>
        </div>
      </div>
      <!-- Start Bidang Section -->
