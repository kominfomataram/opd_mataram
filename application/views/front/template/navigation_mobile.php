<!-- Mobile Menu Start -->
<ul class="wpb-mobile-menu">
  <li >
    <a class="active" href="<?php echo base_url() ?>" style="padding-top: 8px; padding-bottom: 8px;">
    BERANDA
    </a>
  </li>
  <li>
    <a href="<?php echo base_url('berita/berita_list') ?>" style="padding-top: 8px; padding-bottom: 8px;">
    BERITA
    </a>
  </li>
  <li >
    <a href="<?php echo base_url('gallery') ?>" style="padding-top: 8px; padding-bottom: 8px;">
    GALLERY
    </a>
  </li>
  <li >
    <a href="<?php echo base_url('download') ?>" style="padding-top: 8px; padding-bottom: 8px;">DOWNLOAD</a>
  </li>
  <li >
    <a href="#" style="padding-top: 8px; padding-bottom: 8px;">BIDANG</a>
    <?php $query =  $this->db->query("SELECT * FROM page WHERE kategori = '1'")->result_array() ?>
    <ul class="dropdown">
      <?php  foreach ($query as $q) { ?>
        <li><a href="<?php echo base_url($q['kode_page']) ?>"><?php echo strtoupper($q['judul_page']) ?></a></li>
      <?php } ?>
    </ul>
  </li>

  <?php $query =  $this->db->query("SELECT * FROM page WHERE kategori = '2'")->result_array() ?>
  <?php  foreach ($query as $q) { ?>
    <li >
    <?php if ($q['kategori'] != null && $q['id_page'] != '1' && $q['id_page'] != '2' && $q['id_page'] != '3' && $q['id_page'] != '4'): ?>
        <a href="<?php echo base_url($q['kode_page']) ?>" style="padding-top: 8px; padding-bottom: 8px;"><?php echo strtoupper($q['judul_page']) ?></a>
        <?php $query_sub =  $this->db->query("SELECT * FROM page WHERE kategori =".$q['id_page'])->result_array() ?>
        <?php foreach ($query_sub as $qs): ?>
          <ul class="dropdown">
          <li><a href="<?php echo base_url($qs['kode_page']) ?>"><?php echo strtoupper($qs['judul_page']) ?></a></li>

          <!-- <?php $query_sub2 =  $this->db->query("SELECT * FROM page WHERE kategori =".$q['id_page'])->result_array() ?>
          <?php foreach ($query_sub2 as $qs2): ?>
            <ul class="dropdown">
            <li><a href="<?php echo base_url($qs2['kode_page']) ?>"><?php echo strtoupper($qs2['judul_page']) ?></a></li>
            </ul>
          <?php endforeach; ?> -->

          </ul>
        <?php endforeach; ?>
    </li>
    <?php else: ?>
        <li><a href="<?php echo base_url($q['kode_page']) ?>" style="padding-top: 8px; padding-bottom: 8px;"><?php echo strtoupper($q['judul_page']) ?></a></li>
    <?php endif; ?>

  <?php } ?>
</ul>
<!-- Mobile Menu End -->
