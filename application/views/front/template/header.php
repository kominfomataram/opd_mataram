<!-- Start Header Section -->
<header id="header-wrap" class="site-header clearfix">
  <!-- Start Top Bar -->
  <div class="top-bar hidden-xs">
    <div class="container">
      <div class="row">
        <div class="col-md-7 col-sm-9">
          <!-- Start Contact Info -->
          <ul class="contact-details">
            <li>
              <a href="#">
              <i class="icon-envelope">
              </i>
               <?php echo static_data('email') ?>
              </a>
            </li>
            <li>
              <a href="#">
              <i class="icon-call-out">
              </i>
              <?php echo static_data('telpon') ?>
              </a>
            </li>
            <li>
              <a href="#">
              <i class="icon-clock">
              </i>
              <span class="timing"><?php echo static_data('jam_kerja') ?></span>
              </a>
            </li>
          </ul>
          <!-- End Contact Info -->
        </div>
        <div class="col-md-5 col-sm-3">
          <!-- Start Social Links -->
          <ul class="social-list">
            <li>
              <a href="<?php echo static_data('facebook_dinas') ?>" class="social-link facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>
            </li>
            <li>
              <a href="<?php echo static_data('twitter_dinas') ?>" class="social-link twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>
            </li>
          </ul>
          <!-- End Social Links -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Top Bar -->

  <?php $this->load->view('front/template/navigation');  ?>


  <div class="clearfix"></div>
</header>
<!-- End Header Section -->
