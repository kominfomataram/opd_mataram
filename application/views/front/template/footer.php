<!-- Start Footer Section -->
<footer>
  <div class="container">
    <div class="row footer-widgets">
      <!-- Start Contact Widget -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="footer-widget contact-widget">
          <h4>
            <?php echo title_website() ?>
          </h4>
          <p>
            <?php echo static_data('deskripsi_dinas') ?>
          </p>
        </div>
      </div>
      <!-- End Contact Widget -->

      <!-- Start latest Work Posts Widget -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div >
          <h4 style="color:white">
            DI DUKUNG OLEH
          </h4>
          <br>
          <div class="">
            <a href="assets/img/work/img-1.jpg">
            <img alt="" src="<?php echo base_url('assets/skpd/img/logo/logo.png')  ?>" style="height:100px; width:auto;">
            </a>
            <a href="assets/img/work/img-1.jpg">
            <img alt="" src="<?php echo base_url('assets/skpd/img/logo/logo_kominfo_kota.png')  ?>" style="height:100px; width:auto;">
            </a>
          </div>
        </div>
      </div>
      <!-- End latest Work  Widget -->

      <!-- Start Address Widget -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="footer-widget">
          <h4>
            BIODATA
          </h4>
          <ul class="address">
            <li>
              <a href="#"><i class="fa fa-map-marker"></i> <?php echo static_data('alamat') ?></a>
            </li>
            <li>
              <a href="#"><i class="fa fa-phone"></i> <?php echo static_data('telpon') ?></a>
            </li>
            <li>
              <a href="#"><i class="fa fa-envelope"></i> <?php echo static_data('email') ?></a>
            </li>
          </ul>
        </div>
      </div>
      <!-- End Address Widget -->

      <!-- Start Text Widget -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="footer-widget hours-widget">
          <h4>
            Jam Kerja
          </h4>
          <div class="contact-info">
            <p>Berikut adalah Jam Kerja <?php echo title_website() ?></p>
            <?php echo static_data('jam_kerja') ?>
          </div>
        </div>
      </div>
      <!-- End Text Widget -->

    </div>
    <!-- .row -->
  </div>
</footer>
<!-- End Footer Section -->

<!-- Start Copyright -->
<div class="copyright-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <p>
          Copyright &copy; 2018 <?php echo title_website() ?> - Di Dukung Oleh
          <a rel="nofollow" href="http://www.mataramkota.go.id/opd-23-dinas-komunikasi-dan-informatika">
          Dinas Komunikasi dan Informatika Kota Mataram
          </a>
        </p>
      </div>
    </div>
    <!-- .row -->
  </div>
</div>
<!-- End Copyright -->
