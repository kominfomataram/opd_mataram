<div class="col-md-3">
  <center><img src="<?php echo base_url('media/static/').static_data('logo_kementrian') ?>" alt="" style="height:100px; width:auto;"></center>
  <br><br>
  <h2 class="big-title"><?php echo static_data('nama_kementerian') ?></h2>
  <div class="information">
    <div class="contact-datails">
      <p><i class="fa fa-globe"></i>
      <a href="<?php echo static_data('link_kementerian') ?>" target="_blank"><?php echo static_data('link_kementerian') ?></a></p>
      <p><i class="fa fa-home"></i> <?php echo static_data('alamat_kementerian') ?></p>
    </div>
  </div>

  <center><img src="<?php echo base_url('media/static/').static_data('logo_provinsi') ?>" alt="" style="height:100px; width:auto;"></center>
  <br><br>
  <h2 class="big-title"><?php echo static_data('nama_provinsi') ?></h2>
  <div class="information">
    <div class="contact-datails">
      <p><i class="fa fa-globe"></i>
      <a href="<?php echo static_data('link_provinsi') ?>" target="_blank"><?php echo static_data('link_provinsi') ?></a></p>
      <p><i class="fa fa-home"></i> <?php echo static_data('alamat_provinsi') ?></p>
    </div>
  </div>
</div>
