<section id="portfolio" class="section">
  <!-- Portfolio Recent Projects -->
  <!-- Start Heading -->
  <div class="heading">
    <div class="section-title">Gallery Foto <span>Terbaru <?php echo title_website() ?></span></div>
  </div>
  <!-- End Heading -->

  <!-- Some Text -->
  <p class="description text-center">
    Berikut adalah gallery terbaru dari <?php echo title_website() ?>
  </p>

  <div id="portfolio-list">
    <?php $query = $this->db->query('SELECT * FROM foto ORDER BY id_foto DESC LIMIT 6')->result_array(); ?>
    <?php foreach ($query as $q): ?>
      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 mix">
        <div class="portfolio-item">
          <img src="<?php echo base_url('media/gallery/'.$q['nama_album'].'/'.$q['nama']) ?>" alt="" style="height:250px; width:700px;" />
          <div class="overlay">
            <div class="icon">
              <a href="<?php echo base_url('media/gallery/'.$q['nama_album'].'/'.$q['nama']) ?>"  class="lightbox"><i class="icon-picture right"></i></a>
            </div>
          </div>
          <div class="content">
            <h3><?php echo $q['nama'] ?></h3>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
  <!-- End Portfolio  -->
</section>
