<!doctype html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en">
<![endif]-->
<!--[if (gte IE 9)|!(IE)]>
<html lang="en" class="no-js">
<![endif]-->
<html lang="en">

  <?php $this->load->view('front/template/head'); ?>

  <body>
    <!-- Full Body Container -->
    <div id="container">

      <?php $this->load->view('front/template/header'); ?>

      <?php

          if       ($tipe_page == 'beranda') {
            $this->load->view('front/front_beranda');
          } elseif ($tipe_page == 'default') {
            $this->load->view('front/front_default');
          } elseif ($tipe_page == 'sidebar') {
            $this->load->view('front/front_with_sidebar');
          } elseif ($tipe_page == 'berita') {
            $this->load->view('front/berita/front_berita');
          } elseif ($tipe_page == 'berita_detail') {
            $this->load->view('front/berita/front_berita_detail');
          } elseif ($tipe_page == 'gallery') {
            $this->load->view('front/gallery/front_gallery');
          } elseif ($tipe_page == 'gallery_detail') {
            $this->load->view('front/gallery/front_gallery_detail');
          } elseif ($tipe_page == 'download') {
            $this->load->view('front/download/front_download');
          } elseif ($tipe_page == 'error') {
            $this->load->view('front/front_404');
          } else {
            $this->load->view('front/front_404');
          }


      ?>

      <?php $this->load->view('front/template/footer'); ?>

    </div>
    <!-- End Full Body Container -->

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
      <i class="fa fa-angle-up"></i>
    </a>

    <!-- Start Loader -->
    <div id="loader">
      <div class="square-spin">
        <div></div>
      </div>
    </div>


    <?php $this->load->view('front/template/js'); ?>

  </body>
</html>
