<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PAGINATION</title>
  </head>
  <body>
    <br>
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <?php
              foreach ($berita as $b)
              {
                echo '<h2 class="big-title">'.$b['judul_berita'].'</h2>';
                echo '<div class="pull-left">
                        <p>Di posting oleh : '.$b['author'].' - Tanggal : '.$b['created_time'].'</p>
                      </div><br><br>';
                echo '<p>'.substr($b['isi_berita'] , 0 , 150).'</p>'.'<a href="'.base_url('berita/berita_detail/').$b['kode_berita'].'"><button type="button" class="btn btn-success" name="button">Baca Selengkapnya</button></a>';
                echo '<hr>';
              }
          ?>
          <h1><?php echo $halaman ?></h1>
        </div>
      </div>
    </div>
    <br>
  </body>
</html>
