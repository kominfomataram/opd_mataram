<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|time
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|dfs
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Page/page_index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

#$route['test'] = 'FrontWeb/index';

## PAGE MASTER
$route['(:any)'] = 'Page/page_dinamis/$1' ;

## LOGIN
$route['admin/admin_login'] = 'admin/Login/';

## ALL ABOUT JSON
$route['json/album/all'] = 'admin/Gallery/json_data_album';
$route['json/album/all_by_id/(:num)'] = 'admin/Gallery/json_data_album_by_id/$1';

## ALL ABOUT FRONT

    ## FRONT BERITA
    $route['berita/berita_detail/(:any)'] = 'Page/page_berita_detail/$1';
    $route['berita/(:any)'] = 'Page/page_front_berita/$1';
    $route['berita/berita_list/(:any)'] = 'Page/page_front_berita/$1';

    ## FRONT GALLERY
    $route['gallery'] = 'Page/page_gallery_index';
    $route['gallery/(:any)'] = 'Page/page_gallery_detail/$1';


    ## FRONT PENCARIAN
    $route['pencarian/(:any)'] = 'Page/page_search/$1';

## ALL ABOUT ADMIN

    ## ADMIN RESET PASSWORD
    $route['admin/reset_password'] = 'Register/reset_password';
    $route['admin/reset_password_action'] = 'Register/reset_password_action';
    $route['admin/reset_password_update'] = 'Register/reset_password_update';
    $route['reset_password_akun/(:any)/(:any)/(:any)'] = 'Register/reset_password_page/$1/$2/$3';

    ## ADMIN ADMIN
    $route['admin/update_admin/(:any)'] = 'admin/Admin/update_admin/$1';
    $route['admin/reset_default_password/(:any)'] = 'admin/Admin/reset_password_default/$1';
    $route['admin/admin_data'] = 'admin/Web_master/list_admin';

    $route['admin/update_admin_action'] = 'admin/Admin/update_admin_action';
    $route['admin/create_admin_action'] = 'admin/Admin/create_admin_action';

    ## ADMIN BERITA
    $route['admin/berita/tambah_berita']  = 'admin/Berita/berita_tambah';
    $route['admin/berita/ubah_berita/(:any)']  = 'admin/Berita/berita_ubah/$1';
    $route['admin/berita/kategori_berita'] = 'admin/Berita/berita_kategori';
    $route['admin/berita/trash_berita'] = 'admin/Berita/berita_trash';
      ## ADMIN BERITA PROCESS
      $route['admin/berita/create_berita'] = 'admin/Berita/berita_create';
      $route['admin/berita/semua_data_berita']    = 'admin/Berita/berita_semua_data';
      $route['admin/berita/update_berita/(:any)'] = 'admin/Berita/berita_update/$1';
      $route['admin/berita/delete_berita/(:any)/(:any)'] = 'admin/Berita/berita_delete/$1/$2';
      $route['admin/berita/deletesoft_berita/(:any)'] = 'admin/Berita/berita_softdelete/$1';
      $route['admin/berita/suspen_berita/(:any)'] = 'admin/Berita/berita_suspen/$1';
      $route['admin/berita/publish_berita/(:any)'] = 'admin/Berita/berita_publish/$1';

      $route['admin/berita/create_kategori_berita'] = 'admin/Berita/berita_create_kategori';
      $route['admin/berita/delete_kategori_berita/(:any)'] = 'admin/Berita/berita_delete_kategori/$1';
      $route['admin/berita/get_kategori/(:any)'] = 'admin/Berita/berita_get_kategori/$1';
      $route['admin/berita/update_kategori'] = 'admin/Berita/berita_update_kategori';


    ## ADMIN DOWNLOAD
    $route['admin/download'] = 'admin/Download/index';
    $route['admin/download/tambah_download'] = 'admin/Download/download_tambah_file';
    $route['admin/download/detail_download/(:any)'] = 'admin/Download/download_ubah_file/$1';
    $route['admin/download/trash_download'] = 'admin/Download/download_trash';

      ## ADMIN DOWNLOAD PROCESS
      $route['admin/download/upload_file'] = 'admin/Download/download_create_file';
      $route['admin/download/update_file'] = 'admin/Download/download_update_file';
      $route['admin/download/restore_file/(:any)'] = 'admin/Download/download_restore_file/$1';
      $route['admin/download/delete_file/(:any)/(:any)'] = 'admin/Download/download_delete/$1/$2';
      # $route['admin/download/(:any)'] = 'admin/Download/download_tambah_file/$1';

    ## ADMIN LINK TERKAIT
    $route['admin/link_terkait'] = 'admin/Link_terkait/index';
    $route['admin/link_terkait/tambah_link_terkait'] = 'admin/Link_terkait/link_terkait_tambah';
    $route['admin/link_terkait/update_link_terkait'] = 'admin/Link_terkait/link_terkait_update';
    $route['admin/link_terkait/trash_link_terkait'] = 'admin/Link_terkait/link_terkait_trash';

      ## ADMIN LINK TERKAIT PROCESS
      #

      $route['admin/link_terkait/ubah_link_terkait/(:any)'] = 'admin/Link_terkait/link_terkait_ubah/$1';
      $route['admin/link_terkait/delete_link_terkait/(:any)/(:any)'] = 'admin/Link_terkait/link_terkait_delete/$1/$2';
      $route['admin/link_terkait/restore_link_terkait/(:any)'] = 'admin/Link_terkait/link_restore/$1';

    ## ADMIN GALLERY
    $route['admin/gallery'] = 'admin/Gallery/index';
    $route['admin/gallery/tambah_gallery'] = 'admin/Gallery/gallery_tambah';
    $route['admin/gallery/tambah_foto/(:num)'] = 'admin/Gallery/gallery_tambah_foto/$1';
      ## ADMIN GALLERY PROCESS
      $route['admin/gallery/upload_gallery/(:any)']  = 'admin/Gallery/gallery_upload/$1';
      $route['admin/gallery/remove_gallery/(:any)']  = 'admin/Gallery/gallery_remove/$1';
      $route['admin/gallery/create_gallery'] = 'admin/Gallery/gallery_folder_create';
      $route['admin/gallery/delete_gallery/(:num)/(:any)'] = 'admin/Gallery/gallery_folder_delete/$1/$2';
      $route['admin/gallery/update_gallery'] = 'admin/Gallery/gallery_folder_update';
      $route['admin/gallery/delete_foto/(:any)/(:any)'] = 'admin/Gallery/gallery_foto_delete/$1/$2';

    ## ADMIN HALAMAN
    $route['admin/halaman/tambah_halaman']  = 'admin/Halaman/halaman_tambah';
    $route['admin/halaman/data_halaman']    = 'admin/Halaman/halaman_data';
    $route['admin/halaman/detail_halaman/(:any)'] = 'admin/Halaman/halaman_detail/$1';
      ## ADMIN HALAMAN PROCESS
      $route['admin/halaman/create_halaman'] = 'admin/Halaman/halaman_create';
      $route['admin/halaman/update_halaman'] = 'admin/Halaman/halaman_update';
      $route['admin/halaman/delete_halaman/(:any)'] = 'admin/Halaman/halaman_delete/$1';

    ## ADMIN WEB MASTER
    $route['admin/web_master/halaman_depan'] = 'admin/Web_master/halaman_depan';
       ## ADMIN WEB MASTER PROCESS
       $route['admin/web_master/update_web_master'] = 'admin/Web_master/halaman_update';

    ## ADMIN PENGATURAN
    $route['admin/pengaturan/(:any)'] = 'admin/Pengaturan/index/$1';
      ## ADMIN PENGATURAN PROCESS
      $route['admin/pengaturan/admin/update_admin'] = 'admin/Pengaturan/pengaturan_update';
      $route['admin/update_admin_action_pengaturan'] = 'admin/Admin/update_admin_action_pengaturan';

    ## ADMIN MENU GENERATOR
    $route['admin/menu_generator'] = 'admin/Web_master/menu_generator';

    ## ADMIN DASHBOARD
    $route['admin/dashboard'] = 'admin/Dashboard/index';

    ## ADMIN LOG
    $route['admin/log'] = 'admin/Admin/log';

    ## ADMIN PESAN
    $route['admin/pesan'] = 'admin/Admin/pesan';
## END OF ALL ABOUT ADMIN
