<?php

/**
 *
 */
class JajeTujaqLib
{
  public function __construct($params = array())
  {
    $this->CI =& get_instance();
    $this->CI->load->helper('url');
    $this->CI->config->item('base_url');
    $this->CI->load->database();
  }

  public function tanggal_konversi($tanggal = null)
  {
    # code...
    return date('Y-m-d' , strtotime($tanggal));
  }

  public function tanggal_konversi_normal($tanggal = null)
  {
    # code...
    return date('d-m-Y' , strtotime($tanggal));
  }

  #Jika Where Lebih Dari Satu Gunakan Parameter Menggunakan Array
  public function hitung_jumlah($nama_tabel = null, $where = null)
  {
    # code...
    if (isset($where)) {
      # code...
      $query = $this->CI->db->query('SELECT COUNT(*) FROM'.$nama_tabel.'WHERE'.$where)->result_array();
      return $query;
    }
    else {
      # code...
      $query = $this->CI->db->query('SELECT COUNT(*) as jumlah_'.$nama_tabel.' FROM '.$nama_tabel)->result_array();
      return $query;
    }
  } #batas hitung jumlah


}


?>
