<?php

  function title_website($lokasi_website = null)
  {
    # code...
    $ci =& get_instance();
    $ci->load->database();
    $data =  $ci->db->query('SELECT nama_dinas as title FROM static')->row()->title;
    if ($data == null)
    {
      # code...
      return 'Data Tidak Ada';
    }
    else
    {
      # code...
      if ($lokasi_website == null) {
        return $data;
      }
      else {
        # code...
        return $data.' | '.$lokasi_website;
      }
    }
  } //batas function title_website

  function static_data($static_data = null)
  {
    // --------------------------------------------------------------
    // Yang bisa dipanggil lewat fungsi ini adalah sebagai berikut  :
    // --------------------------------------------------------------

    #email
    #kontak
    #home_gbr_1
    #home_gbr_2
    #home_gbr_3
    #link_kementerian
    #link_provinsi
    #link_kabupaten
    #jam_kerja
    #warna_website
    #favicon
    #nama_dinas
    #nama_kementerian
    #nama_provinsi

    $ci =& get_instance();
    $ci->load->database();
    $data =  $ci->db->query('SELECT '.$static_data.' as static FROM static')->row()->static;
    if ($data == null && $data == '')
    {
      # code...
      $data = 'Data Tidak Ada';
      return $data;
    }
    else
    {
      return $data;
    }


  }

  function assets($nama_folder = null , $target = null)
  {
    # code...
    $ci =& get_instance();
    $ci->load->helper('url');
    return base_url('assets/'.$nama_folder.'/'.$target);
  }

  function assets_gambar($nama_folder = null , $target = null , $panjang = null , $lebar = null , $class = null)
  {
    # code...
    $ci =& get_instance();
    $ci->load->helper('url');
    $gambar = '<img src="'.base_url('assets/'.$nama_folder.'/').$target.' alt="" height="'.$panjang.'" width="'.$lebar.' class="'.$class.'" ">';
  }

 function bersihkan_kata($string) {
   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
   return preg_replace('/-+/', '-', $string);
 }

 function hitung_jumlah($nama_tabel = null, $where = null)
 {
   $ci =& get_instance();
   $ci->load->helper('url');
   $ci->config->item('base_url');
   $ci->load->database();
   if (isset($where)) {
     # code...
     $query = $ci->db->query('SELECT COUNT(*) FROM'.$nama_tabel.'WHERE'.$where)->row();
     return $query;
   }
   else {
     # code...
     $query = $ci->db->query('SELECT COUNT(*) as jumlah_'.$nama_tabel.' FROM '.$nama_tabel)->row();
     return $query;
   }
 } #batas hitung jumlah

?>
