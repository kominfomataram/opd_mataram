<?php

# Simple Log System untuk melihat aktivitas pada userdata
# Format Database
# id -> AI | NN  -> INT
# username -> Relasi pada username , bisa username admin atau id_admin -> BASED CONDITION
# aktivitas -> VARCHAR | 650
# date_timestamp -> waktu dan tanggal aktivitas
# ipaddress -> ip digunakan pada saat login


function log_logged($keterangan = null)
{
  # parameter $keterangan pada saat dipanggi menggunakan keterangan sebagai berikut :
  # 'LOGIN'
  # 'LOGOUT'

  $ci =& get_instance();
  $ci->load->database();
  $ci->load->library('session');
  $tanggal = date('Y-m-d h:i:sa');
  $username = $ci->session->userdata('username');

  $aktivitas = 'Melakukan '.strtoupper($keterangan).' Pada System';
  $data = array('username' => $username ,
                'aktivitas' => $aktivitas ,
                'date_timestamp' => $tanggal ,
                'ip_address' => $_SERVER['REMOTE_ADDR'] );
  $ci->db->insert('log',$data);
}

function log_record_data($data = null , $keterangan = null)
{
  # parameter $keterangan pada saat dipanggil menggunakan keterangan sebagai berikut :
  # 'INSERT'
  # 'DELETE'
  # 'UPDATE'
  # 'UPLOAD FOTO'
  # Dan sebagainya

  $ci =& get_instance();
  $ci->load->database();
  $ci->load->library('session');
  $tanggal = date('Y-m-d h:i:sa');
  $username = $ci->session->userdata('username');

  $aktivitas = 'Melakukan '.strtoupper($keterangan).' Data dengan Rincian Data Sebagai Berikut || '.json_encode($data);
  $data1 = array('username' => $username ,
                'aktivitas' => $aktivitas ,
                'date_timestamp' => $tanggal ,
                'ip_address' => $_SERVER['REMOTE_ADDR'] );
  $ci->db->insert('log',$data1);
}

?>
