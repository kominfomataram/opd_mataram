<?php

/**
 *
 */
class JSON extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
  }

  ## GROUP BY OBJECT UNTUK DIBUAT JSON

  ## BERITA JSON
  public function berita()
  {

  }

  public function berita_statistik_by_month()
  {
    $query = $this->db->query('SELECT berita.created_time AS bulan , COUNT( * ) as data
                               FROM berita
                               GROUP BY MONTH(berita.created_time)
                               ORDER BY COUNT( * )')
                    ->result_array();
    echo json_encode($query);
  }

  public function berita_statistik_by_author()
  {
    $query = $this->db->query('SELECT berita.author AS author , COUNT( * ) as data
                               FROM berita
                               GROUP BY berita.author
                               ORDER BY COUNT( * )')
                    ->result_array();
    echo json_encode($query);
  }
  ## END BERITA JSON

}


?>
