<?php


class Page extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('PageModel');
        $this->load->model('BeritaModel');
        $this->load->model('GalleryModel');
        $this->load->model('DownloadModel');
    }

    public function page_index()
    {
        $data['tipe_page'] = 'beranda';
        $data['judul'] = 'Beranda';
        $data['page'] = null;
        $data['berita'] = $this->BeritaModel->get_all_berita_front();
        $data['download'] = $this->DownloadModel->read_download_all();
        $this->load->view('front/index', $data);
    }

    public function page_dinamis($kode_page = null , $id = null)
    {
        $kode = $this->PageModel->read_page_by_kodepage($kode_page);
        $data['page'] = $this->PageModel->read_page_isi($kode_page);
        if ($kode == null) {

            $data['judul'] = 'ERROR';
            $data['tipe_page'] = 'error';
            $this->load->view('front/index', $data);

        } elseif ($data['page'][0]['kode_page'] == 'gallery') {
            $data['tipe_page'] = 'gallery';
            $data['gallery'] = $this->GalleryModel->get_all_album();
            $data['judul'] = $data['page'][0]['judul_page'];
            $this->load->view('front/index', $data);
        } elseif ($data['page'][0]['kode_page'] == 'download') {
            $data['tipe_page'] = 'download';
            $data['download'] = $this->DownloadModel->read_download_all();
            $data['judul'] = $data['page'][0]['judul_page'];
            $this->load->view('front/index', $data);
        } else {
            $kode = $this->PageModel->read_page_by_kodepage($kode_page);
            $data['tipe_page'] = $kode[0]['tipe_page'];
            $data['page'] = $this->PageModel->read_page_isi($kode_page);
            $data['judul'] = $data['page'][0]['judul_page'];
            $this->load->view('front/index', $data);
        }
    }

    public function page_berita_detail($kode_berita)
    {
        $data['page'] = $this->BeritaModel->get_all_berita_front_by_kode($kode_berita);
        $data['judul'] = $data['page'][0]['judul_berita'];
        $data['tipe_page'] = 'berita_detail';
        $this->load->view('front/index', $data);
    }

    public function page_gallery_detail($judul_album)
    {
        $data['gambar'] = $this->GalleryModel->get_foto_by_judul_album($judul_album , 0);
        $data['judul'] = 'Gallery Detail';
        $data['tipe_page'] = 'gallery_detail';
        $this->load->view('front/index', $data);
    }

    public function page_search($keyword = null)
    {
        $keyword = $_POST['pencarian'];
        $data['berita'] = $this->BeritaModel->search_berita($keyword);
        $data['foto'] = $this->GalleryModel->search_foto($keyword);
        $data['file'] = $this->DownloadModel->search_file($keyword);
        $data['tipe_page'] = 'pencarian';
        $this->load->view('front/pencarian/front_pencarian', $data);
        echo 'MANDALA = '.$keyword;
    }

    public function page_front_berita($id = null)
    {
        $data['page'] = $this->PageModel->read_page_isi('berita');
        $data['tipe_page'] = 'berita';
        $jml = $this->db->query('SELECT * FROM berita WHERE status = 1')->num_rows();
        $this->load->library('pagination');
        $config['base_url'] = base_url('berita/berita_list/');
        $config['total_rows'] = $jml;
        $config['per_page'] = '3';
        $config['uri_segment'] = 3;
        $config['first_link']       = 'Pertama';
        $config['last_link']        = 'Akhir';
        $config['next_link']        = 'Selanjutnya';
        $config['prev_link']        = 'Sebelumnya';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $this->pagination->initialize($config);
        $data['berita'] = $this->BeritaModel->get_all_berita_front_temp($config['per_page'],$id)->result_array();
        $data['halaman'] = $this->pagination->create_links();
        $data['judul'] = $data['page'][0]['judul_page'];
        $this->load->view('front/index', $data);
    }
}
