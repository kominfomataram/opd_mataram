<?php

/**
 * ONLY FOR TESTING !
 */
class Test extends CI_Controller
{
  function __construct()
  {
    # code...
    parent::__construct();
    $this->load->library('JajeTujaqLib');
    $this->load->library('upload');
    $this->load->library('MY_Upload');
    $this->load->model('PageModel');
    $this->load->model('BeritaModel');
    $this->load->model('GalleryModel');
    $this->load->model('DownloadModel');
  }

  public function index()
  {
    $this->load->view('test/test');
  }

  public function test_upload()
  {
    echo 'TEST';
		if($this->upload->do_multi_upload("uploadedimages")) {
      echo 'COBA';
			print_r($this->upload->get_multi_upload_data());
		}
  }

  public function test_color()
  {
    # code...
    $this->load->view('test/warna');
  }

  public function color_test()
  {
    # code...
    $warna = $_POST['warna'];
    echo 'Warna anda adalah = '.$warna;
  }

  public function pagination($id = null)
  {
    $data['page'] = $this->PageModel->read_page_isi('berita');
    $data['tipe_page'] = 'berita';

    $jml = $this->db->query('SELECT * FROM berita WHERE status = 1')->num_rows();
    $this->load->library('pagination');
    //pengaturan pagination
    $config['base_url'] = base_url('Test/pagination/');
    $config['total_rows'] = $jml;
    $config['per_page'] = '3';
    $config['uri_segment'] = 3;
    $config['first_page'] = 'Awal';
    $config['last_page'] = 'Akhir';
    $config['next_page'] = '&laquo;';
    $config['prev_page'] = '&raquo;';

    $this->pagination->initialize($config);

    $data['berita'] = $this->BeritaModel->get_all_berita_front_temp($config['per_page'],$id)->result_array();


    //buat pagination
    $data['halaman'] = $this->pagination->create_links();

    //tamplikan data
    //$data['berita'] = $this->BeritaModel->get_all_berita_front();
    $data['judul'] = $data['page'][0]['judul_page'];
    //$data['berita'] = $this->BeritaModel->get_all_berita_front_temp($config['per_page'], 1);

    $this->load->view('test/pagination', $data);
  }

}


?>
