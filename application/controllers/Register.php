<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Register extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('RegisterModel');
	}
    /*
    *Showing  Login page here
    */
    public function index(){
    	$this->load->view('admin/v_register');
    }

    public function add_admin()
    {
    	$this->form_validation->set_rules('username', 'Nama Pengguna', 'trim|required|is_unique[login.username]',array('required'=>'* %s tidak boleh kosong','is_unique'=>'* %s sudah digunakan'));
    	$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[login.email]',array('required'=>'* %s tidak boleh kosong','min_length'=>'* Minimal 9 karakter','is_unique'=>'* %s sudah digunakan'));
    	$this->form_validation->set_rules('password', 'Kata Sandi', 'trim|required|min_length[8]',array('required'=>'* %s tidak boleh kosong', 'min_length'=>'* Minimal 8 karakter'));

    	$this->form_validation->set_rules('repassword', 'Konfirmasi Kata Sandi', 'trim|required|matches[password]|min_length[8]',array('required'=>'* %s tidak boleh kosong', 'matches'=>'* Konfirmasi kata sandi salah!', 'min_length'=>'* Minimal 8 karakter'));

    	if ($this->form_validation->run() != FALSE) {
    		$username = $this->input->post('username');
    		$email = $this->input->post('email');
    		$password = md5($this->input->post('password'));
    		$cek = $this->M_register->cek_db($username,$email);

			if(isset($cek)){
				$this->session->set_flashdata('pesan', 'Username atau email telah terdaftar!');
				redirect('Register');
			}else{
				$data  = array(
					'username' => $username,
					'password' => $password,
					'level'=> 3,
					'email'=> $email,
					'data_akun' => 0,
					'status' => 1,
				);
				$this->M_register->add_user($data);
				$this->session->set_flashdata('pesan', 'Anda telah berhasil mendaftar, silahkan masuk!');
				redirect('C_login');
			}

    	}else{
    		$this->load->view('admin/v_register');
    	}

    }


		public function reset_password()
		{
			$this->load->view('admin/template/meta');
			$this->load->view('admin/reset_password');
		}

		public function reset_password_action()
		{
			$this->load->helper(array('form','url'));
			$this->load->library('form_validation');
			$akun = $_POST['akun'];
			$this->form_validation->set_rules('akun', 'Email', 'required|valid_email');
			if ($this->form_validation->run() == FALSE) {
				$query = $this->db->query("SELECT email , nama , username FROM admin WHERE username = '".$akun."'")->result_array();
				$this->kirim_email($query[0]['email'],$query[0]['nama'],$query[0]['username']);
			}
			else {
				$query = $this->db->query("SELECT nama , email , username FROM admin WHERE email = '".$akun."'")->result_array();
				$this->kirim_email($query[0]['email'],$query[0]['nama'],$query[0]['username']);
			}
		}

		public function kirim_email($email = null , $nama = null , $username = null )
	  {
	    $ci = get_instance();
	    $ci->load->library('email');
			$config['protocol'] = "smtp";
			$config['smtp_host'] = "ssl://smtp.gmail.com";
			$config['smtp_port'] = "465";
			$config['smtp_user'] = "sysadm.opdcms.mataramkota@gmail.com";
			$config['smtp_pass'] = "mataram12345";
			$config['charset'] = "utf-8";
			$config['mailtype'] = "html";
	    $config['newline'] = "\r\n";

	    $ci->email->initialize($config);

	    $ci->email->from('sysadm.opdcms.mataramkota@gmail.com', 'Reset Password Untuk '.static_data('nama_dinas'));
	    $list = array($email , 'sysadm.opdcms.mataramkota@gmail.com');
	    $ci->email->to($list);
	    $ci->email->subject('Penggantian Password Baru dengan Nama : '.$nama.' dan Email : '.$email.' - '.static_data('nama_dinas'));
	    $ci->email->message('Dear Admin atas nama - <b>'.$nama.'</b> <br> Anda dapat melakukan reset password melalui link berikut <br>'.base_url('/reset_password_akun/'.md5($username).'/'.$username.'/'.sha1($username.'/'.$email)).' <br> Salam - Sistem OPD CMS Mataram Kota <br> Terima Kasih');
	    if ($this->email->send()) {
				$data['pesan'] = 'Email Berhasil Terkirim <br> <h4>SILAHKAN CEK EMAIL ANDA UNTUK MELAKUKAN RESET PASSWORD</h4>';
	      $this->load->view('admin/reset_password_berhasil',$data);
	    } else {
				$data['pesan'] = 'Email Gagal Terkirim';
	      $this->load->view('admin/reset_password_berhasil',$data);
	    }
		}

		public function reset_password_page($string_1 = null , $username = null , $string_2 = null)
		{
				$data['username'] = $username;
				$query = $this->db->query("SELECT nama , email , username FROM admin WHERE username = '".$username."'")->result_array();
				$data['email'] = $query[0]['email'];
				$this->load->view('admin/reset_password_action',$data);
		}

		public function reset_password_update()
		{
			$password_1 = $_POST['password_1'];
			$password_2 = $_POST['password_2'];
			$username = $_POST['username'];
			$email = $_POST['email'];
			if ($password_1 != $password_2) {
				$this->session->set_flashdata('pesan', 'Masukan Password Pertama dan Kedua Anda Tidak Sama');
				redirect('/reset_password_akun/'.md5($username).'/'.$username.'/'.sha1($username.'/'.$email));
			}
			else {
				$this->session->set_flashdata('pesan_berhasil', 'Pengubahan Password Berhasil , Anda Dapat Menuju Halaman Login Sekarang');
				$data = array('password' => md5($password_1));
				$this->db->where('username',$username);
				$this->db->update('admin' , $data);
				redirect('/reset_password_akun/'.md5($username).'/'.$username.'/'.sha1($username.'/'.$email));
			}
		}

}
