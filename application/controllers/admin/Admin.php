<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$status_log = $this->session->userdata('is_logged_in');
		if ($status_log != '1' && $status_log != '2' && $status_log != '3') {
			$this->session->set_flashdata('msg','Please Login to Continue');
	    redirect('admin/Login');
    }
    $this->load->model('AdminModel');
	$this->load->library('JajeTujaqLib');
	}

	public function tambah_admin()
    {
    	$this->form_validation->set_rules('username', 'Nama Pengguna', 'trim|required|is_unique[admin.username]',array('required'=>'* %s tidak boleh kosong','is_unique'=>'* %s sudah digunakan'));
    	$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[admin.email]',array('required'=>'* %s tidak boleh kosong','min_length'=>'* Minimal 9 karakter','is_unique'=>'* %s sudah digunakan'));
    	$this->form_validation->set_rules('password', 'Kata Sandi', 'trim|required|min_length[8]',array('required'=>'* %s tidak boleh kosong', 'min_length'=>'* Minimal 8 karakter'));

    	$this->form_validation->set_rules('repassword', 'Konfirmasi Kata Sandi', 'trim|required|matches[password]|min_length[8]',array('required'=>'* %s tidak boleh kosong', 'matches'=>'* Konfirmasi kata sandi salah!', 'min_length'=>'* Minimal 8 karakter'));

    	if ($this->form_validation->run() != FALSE) {
    		$username = $this->input->post('username');
    		$email = $this->input->post('email');
    		$level = $this->input->post('level');
    		$password = md5($this->input->post('password'));
    		$cek = $this->AdminModel->cek_db($username,$email);

			if(isset($cek)){
				$this->session->set_flashdata('pesan', 'Username atau email telah terdaftar!');
				redirect('Register');
			}else{
				$data  = array(
					'username' => $username,
					'password' => $password,
					'id_level_admin'=> $level,
					'email'=> $email,
					'isi_biodata' => 0,
					'status' => 1,
				);
				$this->AdminModel->add_admin($data);
				$this->session->set_flashdata('berhasil', 'Akun telah berhasil ditambahkan!');
				redirect('admin/Web_master/tambah_admin');
			}

    	}else{
    		$this->load->view('admin/template/meta');
			$this->load->view('admin/template/header');
			$this->load->view('admin/template/sidebar');
			$this->load->view('admin/wm_tambah_admin');
			$this->load->view('admin/template/footer');
    	}
    }

    public function isi_data_akun()
    {
	    $this->form_validation->set_rules('nama', 'Nama Lengkap', 'required',array('required'=>'* %s tidak boleh kosong'));
	    $this->form_validation->set_rules('no_tlp', 'no_tlp', 'required',array('required'=>'* %s tidak boleh kosong'));
	    $this->form_validation->set_rules('jenkel', 'Jenis kelamin', 'required',array('required'=>'* %s tidak boleh kosong'));
	    $this->form_validation->set_rules('alamat', 'Alamat', 'required',array('required'=>'* %s tidak boleh kosong'));

	    if ($this->form_validation->run() != FALSE) {
	        $username  = $this->session->userdata('username');
	        $foto      = $this->input->post('foto');
	        $nama      = $this->input->post('nama');
	        $no_tlp    = $this->input->post('no_tlp');
	        $jenkel    = $this->input->post('jenkel');
	        $alamat    = $this->input->post('alamat');

	        $this->load->library('upload');
	        $nmfile = "profil_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
	        $config['upload_path'] = './media/img/profil/'; //path folder
	        $config['allowed_types'] = 'jpg|jpeg'; //type yang dapat diakses bisa anda sesuaikan
	        $config['max_size'] = '300'; //maksimum besar file 300kb
	        $config['max_width']  = '850'; //lebar maksimum 850 px
	        $config['max_height']  = '1134'; //tinggi maksimu 1134 px
	        $config['file_name'] = $nmfile; //nama yang terupload nantinya

	        $this->upload->initialize($config);

	        if($_FILES['foto']['name'])
	        {
	            if ($this->upload->do_upload('foto'))
	            {
	                $gbr = $this->upload->data();
	                $data = array(
	                    'nama'    		  => $nama,
	                    'alamat'  		  => $alamat,
	                    'jenis_kelamin'   => $jenkel,
	                    'telpon'          => $no_tlp,
	                    'isi_biodata' 	  => 1,
	                    'username' 		  => $username,
	                    'gambar'         => "media/img/profil/".$gbr['file_name']
	                );
	                $this->AdminModel->update_data_akun($data,$username);
	                $this->session->set_userdata($data);
									//print_r($this->session->userdata());
									redirect('admin/dashboard'); //jika berhasil
	            }else{
	                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
	                $this->session->set_flashdata("pesan", $this->upload->display_errors()."<b>Gagal upload gambar! Pastikan format dan ukuran gambar sesuai syarat.</b>");
	                $this->load->view('admin/isi_data_akun');
	            }
	        }else{
	            //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
	            $this->session->set_flashdata("pesan", "<b>Gagal upload gambar! Pastikan format dan ukuran gambar sesuai syarat.</b>");
	            $this->load->view('admin/isi_data_akun');
	        }
	    }else{
	        $this->load->view('admin/isi_data_akun.php');
	    }
	}

	public function update_admin($id_admin = null)
	{
		$data['js'] = 'wm_list_admin';
		$data['admin'] = $this->AdminModel->read_admin_by_username($id_admin);
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/admin/admin_update',$data);
		$this->load->view('admin/template/footer',$data);
	}

	public function create_admin_action()
	{
		$email = $this->input->post('email');
		$username = $this->input->post('username');		
		$name = $this->input->post('nama');
		$telpon = $this->input->post('telpon');
		$password = md5($this->input->post('password'));
		$confirm_password = md5($this->input->post('confirm_password'));
		$level = $this->input->post('level');
		$alamat = $this->input->post('alamat');
		$jk = $this->input->post('jk');

		if ($password == $confirm_password) {
			$data = array('email' => $email , 'username' => $username , 'nama' => $name , 'telpon' => $telpon , 'id_level_admin' => $level , 'password' => $password , 'jenis_kelamin' => $jk , 'alamat' => $alamat);		
			$this->AdminModel->add_admin($data);						
			$this->session->set_flashdata('berhasil', 'Admin Berhasil Di Tambahkan');
			redirect('admin/admin_data');
		} else {
			$this->session->set_flashdata('pesan', 'Password Tidak Cocok');
			redirect('admin/update_admin/'.$this->session->userdata('username'));
		}
	}

	public function update_admin_action(Type $var = null)
	{
		$email = $this->input->post('email');
		$username = $this->input->post('username');
		$username_lama = $this->input->post('username_lama');
		$name = $this->input->post('nama');
		$telpon = $this->input->post('telpon');
		$password = md5($this->input->post('password'));
		$confirm_password = md5($this->input->post('confirm_password'));
		$level = $this->input->post('level');
		$alamat = $this->input->post('alamat');
		$jk = $this->input->post('jk');

		if ($password == $confirm_password) {
			$data = array('email' => $email , 'username' => $username , 'nama' => $name , 'telpon' => $telpon , 'id_level_admin' => $level , 'password' => $password , 'jenis_kelamin' => $jk , 'alamat' => $alamat);		
			$this->AdminModel->update_admin_by_self($username_lama , $data);
			$this->session->set_userdata(array('username' => $username));
			$this->session->set_flashdata('berhasil_reset_password', 'Admin Berhasil Di Update');
			redirect('admin/admin_data');
		} else {
			$this->session->set_flashdata('pesan', 'Password Tidak Cocok');
			redirect('admin/update_admin/'.$this->session->userdata('username'));
		}
	}

	public function update_admin_action_pengaturan(Type $var = null)
	{
		$email = $this->input->post('email');
		$username = $this->input->post('username');
		$username_lama = $this->input->post('username_lama');
		$name = $this->input->post('nama');
		$telpon = $this->input->post('telpon');
		$password = md5($this->input->post('password'));
		$confirm_password = md5($this->input->post('confirm_password'));
		$level = $this->input->post('level');
		$alamat = $this->input->post('alamat');
		$jk = $this->input->post('jk');

		if ($password == $confirm_password) {
			$data = array('email' => $email , 'username' => $username , 'nama' => $name , 'telpon' => $telpon , 'id_level_admin' => $level , 'password' => $password , 'jenis_kelamin' => $jk , 'alamat' => $alamat);		
			$this->AdminModel->update_admin_by_self($username_lama , $data);
			$this->session->set_userdata(array('username' => $username));
			$this->session->set_flashdata('berhasil', 'Admin Berhasil Di Update');
			redirect('admin/pengaturan/'.$this->session->userdata('username'));
		} else {
			$this->session->set_flashdata('pesan', 'Password Tidak Cocok');
			redirect('admin/pengaturan/'.$this->session->userdata('username'));
		}
	}

	public function ganti_status_admin($username , $status)
	{
		if ($status == '1') {
			$this->db->where('username' , $username);
			$this->db->update('admin' , array('status' => '0'));			
		} elseif($status == '0') {
			$this->db->where('username' , $username);
			$this->db->update('admin' ,array('status' => '1'));			
		}
		 redirect('admin/admin_data');
	}

	public function log()
	{
		$data['js'] = 'wm_list_admin';
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/log/log_data');
		$this->load->view('admin/template/footer',$data);
	}

	public function reset_password()
	{
		$this->load->view('admin/template/meta');
		$this->load->view('admin/reset_password');
	}

	public function reset_password_action()
	{
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		$akun = $_POST['akun'];
		$this->form_validation->set_rules('akun', 'Email', 'required|valid_email');
		if ($this->form_validation->run() == FALSE) {
			$query = $this->db->query("SELECT email , nama FROM admin WHERE username = '".$akun."'")->result_array();
			$this->kirim_email($query[0]['email'],$query[0]['nama']);
		}
		else {
			$query = $this->db->query("SELECT nama , email FROM admin WHERE email = '".$akun."'")->result_array();
			$this->kirim_email($query[0]['email'],$query[0]['nama']);
		}
	}

	public function kirim_email($email = null , $nama = null)
  	{
    $ci = get_instance();
    $ci->load->library('email');
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "ssl://smtp.gmail.com";
		$config['smtp_port'] = "465";
		$config['smtp_user'] = "arsip.diskominfo@gmail.com";
		$config['smtp_pass'] = "mandalamandala";
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
    $config['newline'] = "\r\n";

    $ci->email->initialize($config);

    $ci->email->from('sysadm.opdcms.mataramkota@gmail.com', 'Reset Password Untuk '.static_data('nama_dinas'));
    $list = array($email);
    $ci->email->to($list);
    $ci->email->subject('Penggantian Password Baru dengan Nama : '.$nama.' dan Email : '.$email);
    $ci->email->message('Dear Admin atas nama - <b>'.$nama.'</b> <br> Anda dapat melakukan reset password melalui link berikut <br>'.base_url('').' <br> Salam - Sistem OPD CMS Mataram Kota <br> Terima Kasih');
    if ($this->email->send()) {
			$data['pesan'] = 'Email Berhasil Terkirim';
      $this->load->view('admin/reset_password_berhasil',$data);
    } else {
			$data['pesan'] = 'Email Gagal Terkirim';
      $this->load->view('admin/reset_password_berhasil',$data);
    }
	}

	public function reset_password_default($username)
	{
		$data = array('password' => md5('admin'));
		$this->db->where('username',$username);
		$this->db->update('admin' , $data);
		$this->session->set_flashdata('berhasil_reset_password', 'Password Berhasil Di Reset Dengan Default');
		redirect('admin/admin_data');
	}

	public function pesan()
	{
		$data['js'] = 'pesan';
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/pesan/pesan_data');
		$this->load->view('admin/template/footer',$data);
	}

}

/* End of file admin.php */
/* Location: ./application/views/admin/admin.php */
