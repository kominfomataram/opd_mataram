<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Web_master extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $status_log = $this->session->userdata('is_logged_in');
    		if ($status_log != '1' && $status_log != '2' && $status_log != '3') {
    			$this->session->set_flashdata('msg','Please Login to Continue');
    	    redirect('admin/Login');
        }
        $this->load->model('WebMasterModel');
        $this->load->model('AdminModel');
        $this->load->model('PageModel');
    }

    public function index()
    {
    }

    public function tambah_admin($value='')
    {
        $this->load->view('admin/template/meta');
        $this->load->view('admin/template/header');
        $this->load->view('admin/template/sidebar');
        $this->load->view('admin/admin/admin_tambah');
        $this->load->view('admin/template/footer');
    }

    public function list_admin()
    {
        $data['admin'] = $this->AdminModel->read_admin_all();
        $footer['js']='wm_list_admin';
        $header['meta']='wm_list_admin';
        $this->load->view('admin/template/meta');
        $this->load->view('admin/template/header');
        $this->load->view('admin/template/sidebar');
        $this->load->view('admin/admin/admin_data', $data);
        $this->load->view('admin/template/footer', $footer);
    }

    public function halaman_depan()
    {
        # code...
        $data['mark'] = 'halaman_depan';
        $this->load->view('admin/template/meta');
        $this->load->view('admin/template/header');
        $this->load->view('admin/template/sidebar');
        $this->load->view('admin/halaman_depan/wm_halaman_depan');
        $this->load->view('admin/template/footer', $data);
    }

    # route info = admin/web_master/update_web_master
    public function halaman_update()
    {
        # code...Matar

        $this->load->library('PHP_ICO');

        $nama = $this->input->post('title');
        $email = $this->input->post('email');
        $telpon = $this->input->post('telpon');
        $jam = $this->input->post('jam');
        $nk = $this->input->post('nama_kementerian');
        $lk = $this->input->post('link_kementerian');
        $np = $this->input->post('nama_provinsi');
        $lp = $this->input->post('link_provinsi');
        $alamat = $_POST['alamat'];
        $warna = $_POST['warna'];
        $deskripsi = $_POST['deskripsi'];
        $facebook = $_POST['facebook'];
        $twitter = $_POST['twitter'];

        $this->load->library('upload');
        $a = 'upload/';
        $nmfile = "file_".$nama.'_'.time(); //nama file saya beri nama langsung dan diikuti fungsi
        $config['image_library'] = 'gd2';
        $config['upload_path'] = './media/static/';
        $config['allowed_types']        = 'gif|jpg|png|pdf|doc|sql|svg';
        $config['max_size']             = 2048;
        $config['max_width']            = 1920;
        $config['max_height']           = 1800;
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);

        $last_instansi = $this->db->query('SELECT logo_instansi FROM static')->row()->logo_instansi;
        $last_provinsi = $this->db->query('SELECT logo_provinsi FROM static')->row()->logo_provinsi;
        $last_kementerian = $this->db->query('SELECT logo_kementrian FROM static')->row()->logo_kementrian;
        $last_slide1 = $this->db->query('SELECT home_gbr_1 FROM static')->row()->home_gbr_1;
        $last_slide2 = $this->db->query('SELECT home_gbr_2 FROM static')->row()->home_gbr_2;
        $last_slide3 = $this->db->query('SELECT home_gbr_3 FROM static')->row()->home_gbr_3;

        $file_name_instansi = $last_instansi;
        $file_name_kementrian = $last_kementerian;
        $file_name_provinsi = $last_provinsi;
        $file_slide1 = $last_slide1;
        $file_slide2 = $last_slide2;
        $file_slide3 = $last_slide3;

        for ($i=0; $i < 6 ; $i++) {
            if (!empty($_FILES['logo'.$i]['name'])) {
                if (!$this->upload->do_upload('logo'.$i)) {
                        $this->upload->display_errors();
                } else {
                    if ($i==0) {
                        $upload_data = $this->upload->data();
                        $file_name_instansi = $upload_data['file_name'];

                        $files = glob(FCPATH.'/media/favicon/*');
                        foreach($files as $file){
                        if(is_file($file))
                        unlink($file);
                        }

                        $destination = FCPATH.'/media/favicon/favicon.ico';
                        $ico_lib = new PHP_ICO();
                        $ico_lib->add_image(  FCPATH.'/media/static/'.$file_name_instansi , array( array( 16, 16 ), array( 24, 24 ), array( 32, 32 ) ) );
                        $ico_lib->save_ico( $destination );

                    }elseif ($i==1) {
                        $upload_data = $this->upload->data();
                        $file_name_kementrian = $upload_data['file_name'];
                    }elseif ($i==2) {
                        $upload_data = $this->upload->data();
                        $file_name_provinsi = $upload_data['file_name'];
                    }elseif ($i==3) {
                        $upload_data = $this->upload->data();
                        $file_slide1 = $upload_data['file_name'];
                    }elseif ($i==4) {
                        $upload_data = $this->upload->data();
                        $file_slide2 = $upload_data['file_name'];
                    }elseif ($i==5) {
                        $upload_data = $this->upload->data();
                        $file_slide3 = $upload_data['file_name'];
                    }
                }
            } else {
                echo "gagal";
            }
        }



        $data = array(
            'nama_dinas' => $nama ,
            'email' => $email ,
            'telpon' => $telpon ,
            'jam_kerja' => $jam ,
            'alamat' => $alamat ,
            'link_kementerian' => $lk ,
            'link_provinsi' => $lp ,
            'logo_instansi' => $file_name_instansi,
            'logo_kementrian' => $file_name_kementrian,
            'logo_provinsi' => $file_name_provinsi,
            'home_gbr_1' => $file_slide1 ,
            'home_gbr_2' => $file_slide2 ,
            'home_gbr_3' => $file_slide3 ,
            'warna' => $warna ,
            'deskripsi_dinas' => $deskripsi ,
            'facebook_dinas' => $facebook ,
            'twitter_dinas' => $twitter
        );
        //print_r($data);
        $this->WebMasterModel->update_halaman_depan($data);
        log_record_data($data, 'MENGUPDATE HALAMAN DEPAN');
        redirect('admin/Web_master/halaman_depan');
    }

    # Web Master untuk membuat menu , menu tersebut berfungsi akan ditampilkan di depan layar
    public function menu_generator()
    {
        # code...
        $data['js'] = 'menu_generator';
        $data['page'] = $this->PageModel->read_page_all();
        $this->load->view('admin/template/meta');
        $this->load->view('admin/template/header');
        $this->load->view('admin/template/sidebar');
        $this->load->view('admin/menu_generator/menu_generator', $data);
        $this->load->view('admin/template/footer', $data);
    }

    public function menu_generator_update()
    {
        $menu = $_POST['test'];
        $data = array('menu' => $menu);

        $check = $this->db->get('menu_temp')->result_array();
        if ($check[0]['menu'] == null) {
            $this->WebMasterModel->create_menu_generator($data);
        } else {
            $this->WebMasterModel->update_menu_generator($data);
        }
    }

    public function menu_json()
    {
        # code...
        $data = $this->WebMasterModel->read_menu_generator();
        echo json_encode($data[0]);
    }
}

/* End of file web_master.php */
/* Location: ./application/views/admin/web_master.php */
