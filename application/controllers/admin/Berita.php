<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$status_log = $this->session->userdata('is_logged_in');
		if ($status_log != '1' && $status_log != '2' && $status_log != '3') {
			$this->session->set_flashdata('msg','Please Login to Continue');
	    redirect('admin/Login');
    }
	  $this->load->model('BeritaModel');
	}

	public function index()
	{
	}

	public function berita_tambah()
	{
		$data['js'] = 'berita/berita_tambah';
		$data['kategori'] = $this->BeritaModel->get_all_kategori();
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/berita/berita_tambah',$data);
		$this->load->view('admin/template/footer',$data);
	}

	public function berita_ubah($id)
	{
		$data['js'] = 'berita/berita_ubah';
		$data['kategori'] = $this->BeritaModel->get_all_kategori();
		$data['data_berita'] = $this->BeritaModel->get_one_berita($id);
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/berita/berita_ubah',$data);
		$this->load->view('admin/template/footer',$data);
	}

	public function berita_semua_data()
	{
		$data['js'] = 'berita/berita_semua_data';
		$data['data_berita'] = $this->BeritaModel->get_all_berita($softdelete = '0');
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/berita/berita_data',$data);
		$this->load->view('admin/template/footer',$data);
	}

	public function berita_create()
  {
	    $judul = $_POST['judul'];
	    $kode = bersihkan_kata(str_replace(" ", "-" , strtolower($judul)));
	    $kategori = $_POST['kategori'];
	    $isi = $_POST['isi'];
	    $kata_kunci = $_POST['kata_kunci'];
	    $admin = $this->session->userdata('username');
	    $data = array(
        	'judul_berita' => $judul ,
        	'kode_berita' => $kode ,
        	'isi_berita' => $isi ,
          'created_time' => date("Y-m-d h:i:sa"),
        	'id_kategori_berita' => $kategori ,
        	'author' => $admin,
        	'status'=>1,
					'softdelete_status' => 0
       	);
        $this->BeritaModel->create_berita($data);
				log_record_data($data , 'MEMBUAT BERITA');
        redirect('admin/berita/berita_semua_data');
    }

    public function berita_update($id)
    {
	    $judul = $_POST['judul'];
	    $kode = bersihkan_kata(str_replace(" ", "-" , strtolower($judul)));
	    $kategori = $_POST['kategori'];
	    $isi = $_POST['isi'];
	    $kata_kunci = $_POST['kata_kunci'];
	    $admin = $this->session->userdata('username');
	    $data = array(
	      	'judul_berita' => $judul ,
        	'kode_berita' => $kode ,
        	'isi_berita' => $isi ,
	        'updated_time' => date("Y-m-d h:i:sa"),
        	'id_kategori_berita' => $kategori ,
        	'author' => $admin,
        	'keyword' => $kata_kunci,
	    );
	    $this->BeritaModel->update_berita($id,$data);
			log_record_data($data , 'MERUBAH BERITA');
      redirect('admin/berita/berita_semua_data');

    }

		public function berita_trash()
		{
			$data['js'] = 'berita/berita_semua_data';
			$data['data_berita'] = $this->BeritaModel->get_all_berita($softdelete = '1');
			$this->load->view('admin/template/meta');
			$this->load->view('admin/template/header');
			$this->load->view('admin/template/sidebar');
			$this->load->view('admin/berita/berita_trash',$data);
			$this->load->view('admin/template/footer',$data);
		}

		public function berita_softdelete($id)
    {
	    $where = array('id_berita' =>$id);
	    $this->BeritaModel->deletesoft_berita($where);
			log_record_data('' , 'MENGHAPUS BERITA');
	    echo json_encode(array("status" => TRUE));
    }


    public function berita_delete($id , $type )
    {
			if ($type == 1) {
				# code...
		    $where = array('id_berita' =>$id , );
		    $this->BeritaModel->delete_berita($where);
				log_record_data('' , 'MENGHAPUS BERITA');
		    echo json_encode(array("status" => TRUE));
			}
			elseif ($type == 0) {
		    $this->BeritaModel->deletesoft_berita($id);
				log_record_data('' , 'SOFT DELETE BERITA');
		    echo json_encode(array("status" => TRUE));
			}
			elseif ($type == 2) {
		    $this->BeritaModel->return_berita($id);
				log_record_data('' , 'RETURN BERITA');
		    echo json_encode(array("status" => TRUE));
			}
    }

    public function berita_suspen($id)
    {
	    $data = array(
	      	'status' => 0,
	    );
	    $this->BeritaModel->suspen_berita($id,$data);
	    log_record_data('' , 'SUSPEN BERITA');
	    echo json_encode(array("status" => TRUE));
    }

    public function berita_publish($id)
    {
	    $data = array(
	      	'status' => 1,
	    );
	    $this->BeritaModel->publish_berita($id,$data);
//		log_record_data('' , 'PUBLISH BERITA');
	    echo json_encode(array("status" => TRUE));
    }

    //Kategori Berita
    public function berita_kategori()
	{
		$data['js'] = 'berita/berita_kategori';
		$data['kategori_berita'] = $this->BeritaModel->get_all_kategori();
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/berita/berita_kategori',$data);
		$this->load->view('admin/template/footer',$data);
	}

    public function berita_create_kategori()
    {
	    $nama_kategori = $_POST['nama_kategori'];
	    $deskripsi_kategori = $_POST['deskripsi_kategori_berita'];
	    $data = array(
        	'nama_kategori' => $nama_kategori ,
        	'deskripsi_kategori' => $deskripsi_kategori ,
       	);
      $this->BeritaModel->create_kategori_berita($data);
			log_record_data($data , 'MEMBUAT KATEGORI');
	    echo json_encode(array("status" => TRUE));
    }

    public function berita_get_kategori($id)
    {
    	$data = $this->BeritaModel->get_kategori_berita($id);
    	echo json_encode($data);
    }

    public function berita_update_kategori()
    {
	    $id = $_POST['id_kategori_berita'];
	    $nama_kategori = $_POST['nama_kategori'];
	    $deskripsi_kategori = $_POST['deskripsi_kategori'];
	    $data = array(
        	'nama_kategori' => $nama_kategori ,
        	'deskripsi_kategori' => $deskripsi_kategori ,
       	);
        $this->BeritaModel->update_kategori_berita($id,$data);
	    echo json_encode(array("status" => TRUE));

    }

    public function berita_delete_kategori($id)
    {
	    $where = array('id_kategori_berita' =>$id , );
	    $this->BeritaModel->delete_kategori_berita($where);
	    echo json_encode(array("status" => TRUE));
    }
}

/* End of file berita.php */
/* Location: ./application/views/admin/berita.php */
