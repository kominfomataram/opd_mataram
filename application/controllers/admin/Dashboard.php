<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$status_log = $this->session->userdata('is_logged_in');
		if ($status_log != '1' && $status_log != '2' && $status_log != '3') {
			$this->session->set_flashdata('msg','Please Login to Continue');
	    redirect('admin/Login');
    }
		$this->load->library('JajeTujaqLib');
	}

	public function index()
	{
		if($this->session->userdata('isi_biodata')=='0'){
			$this->load->view('admin/isi_data_akun');
      	}else{
      		$data['js']='dashboard';
      		$data['meta']='dashboard';
			$this->load->view('admin/template/meta',$data);
			$this->load->view('admin/template/header');
			$this->load->view('admin/template/sidebar');
			$this->load->view('admin/dashboard/dashboard');
			$this->load->view('admin/template/footer',$data);
		}

	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/admin/Dashboard.php */
