<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$status_log = $this->session->userdata('is_logged_in');
		if ($status_log != '1' && $status_log != '2' && $status_log != '3') {
			$this->session->set_flashdata('msg','Please Login to Continue');
	    redirect('admin/Login');
    }
		$this->load->model('AdminModel');
	}

	public function index($username = null)
	{
		$data['userdata'] = $this->AdminModel->read_admin_by_username($username);
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/pengaturan/pengaturan',$data);
		$this->load->view('admin/template/footer');
	}

	public function pengaturan_update()
	{
		$username_lama = $this->session->userdata('username');
		$username = $_POST['username'];
		$name = $_POST['nama'];
		$telpon = $_POST['telpon'];
		$alamat = $_POST['alamat'];
		$email = $_POST['email'];
		$password = $_POST['password'];

		$this->load->library('upload');
		$nmfile = "fileadminavatar_".date('Y-m-d');
		$config['upload_path'] = FCPATH.'assets/back/admin_avatar/';
		$config['allowed_types']        = 'doc|docx|pdf|ppt|xls|jpg|jpeg|png|ico|bmp';
		$config['max_size']             = 50000;
		$config['file_name'] = $nmfile;

		$this->upload->initialize($config);
		$upload_photo = $this->upload->do_upload('photo');
		$data_photo = $this->upload->data();

    if ($upload_photo == null) {
			$data = array('username' => $username , 'nama' => $name , 'telpon' => $telpon ,
										'alamat' => $alamat , 'email' => $email , 'password' => md5($password));
			$this->AdminModel->update_data_akun($data , $username_lama);
			log_record_data($data , 'MENGUPDATE PENGATURAN');
			redirect('admin/pengaturan/'.$username);
    } else {
			$data = array('username' => $username , 'nama' => $name , 'telpon' => $telpon ,
										'alamat' => $alamat , 'email' => $email , 'password' => md5($password) ,'gambar'=> $nmfile.$data_photo['file_ext'] );
			$this->AdminModel->update_data_akun($data , $username_lama);
			log_record_data($data , 'MENGUPDATE PENGATURAN');
			redirect('admin/pengaturan/'.$username);
		}


	}


}

/* End of file pengaturan.php */
/* Location: ./application/views/admin/pengaturan.php */
