<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$status_log = $this->session->userdata('is_logged_in');
		if ($status_log != '1' && $status_log != '2' && $status_log != '3') {
			$this->session->set_flashdata('msg','Please Login to Continue');
	    redirect('admin/Login');
    }
		$this->load->model('DownloadModel');
	}

	public function index()
	{
		$data['js'] = 'download/download_semua_data';
		$data['download'] = $this->DownloadModel->get_all_download();
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/download/download_data',$data);
		$this->load->view('admin/template/footer',$data);
	}

	public function download_tambah_file($id_download = null)
	{
		$data['js'] = 'download/download_tambah';
		$data['download'] = $this->DownloadModel->read_download_all();
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/download/download_tambah',$data);
		$this->load->view('admin/template/footer',$data);
	}

	public function download_ubah_file($id_download = null)
	{
		$data['js'] = 'download/download_tambah';
		$data['download'] = $this->DownloadModel->read_download_by_id($id_download);
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/download/download_update',$data);
		$this->load->view('admin/template/footer',$data);
	}

	public function download_create_file()
	{
		$nama = $_POST['nama_file'];
		$this->load->library('upload');
		$nmfile = "file_".$nama.'_'.time();
		$config['upload_path'] = './media/file/';
		$config['allowed_types']        = 'doc|docx|pdf|ppt|xls|jpg|png|ico|bmp';
		$config['max_size']             = 50000;
		$config['file_name'] = $nmfile;

		$this->upload->initialize($config);
		$this->upload->do_upload('file');
		$upload_data = $this->upload->data();

		$data = array(
			'judul' => $nama,
			'url_file' => $upload_data['file_name'],
			'admin_username' => $this->session->userdata('username'),
			'softdelete_status' => 0 );
		$this->DownloadModel->create_file($data);
		$this->session->set_flashdata('msg1','File telah berhasil ditambahkan!');
		redirect('admin/download');
	}

	public function download_update_file()
	{
		$nama = $_POST['nama_file'];
		$id = $_POST['id'];
		$this->load->library('upload');
		$nmfile = "file_".$nama.'_'.time();
		$config['upload_path'] = './media/file/';
		$config['allowed_types']        = 'doc|docx|pdf|ppt|xls|jpg|png|ico|bmp';
		$config['max_size']             = 50000;
		$config['file_name'] = $nmfile;

		$this->upload->initialize($config);

		if (!($this->upload->do_upload('file'))) {
			$upload_data = $this->upload->data();
			$url_query = $this->db->query('SELECT url_file FROM download WHERE id_download ='.$id)->row()->url_file;
			$data = array('judul' => $nama , 'url_file' => $url_query  , 'admin_username' => $this->session->userdata('username') );
		} else {
			$upload_data = $this->upload->data();
			$data = array('judul' => $nama , 'url_file' => $upload_data['file_name']  , 'admin_username' => $this->session->userdata('username') );
		}

		$this->DownloadModel->update_file($id , $data);
		$this->session->set_flashdata('msg1','File telah berhasil diubah!');
		redirect('admin/download');
	}

	public function download_restore_file($id)
	{
		$data = array('softdelete_status' => 0);
		$this->DownloadModel->update_file($id , $data);
		$this->session->set_flashdata('msg1','File telah berhasil dikembalikan!');
		log_record_data('' , 'MENGEMBALIKAN FILE DOWNLOAD DENGAN ID = '.$id);
	    echo json_encode(array("status" => TRUE));
	}

	public function download_trash()
	{
		$data['js'] = 'download/download_semua_data';
		$data['download'] = $this->DownloadModel->get_all_download_trash();
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/download/download_trash',$data);
		$this->load->view('admin/template/footer',$data);
	}

    public function download_delete($id , $type )
    {
		if ($type == 1) {
			# code...
		    $file=$this->DownloadModel->get_file_url($id);
		    if (isset($file->url_file)){
		    	unlink('media/file/'.$file->url_file);
		    }
		    $this->DownloadModel->delete_file($id);
		    $this->session->set_flashdata('msg1','File telah berhasil dihapus secara permanen!');
				log_record_data('' , 'MENGHAPUS FILE DOWNLOAD');
		    echo json_encode(array("status" => TRUE));
		}
		elseif ($type == 0) {
		    $this->DownloadModel->deletesoft_download($id);
		    $this->session->set_flashdata('msg1','File telah berhasil dihapus!');
				log_record_data('' , 'SOFT DELETE FILE DOWNLOAD');
		    echo json_encode(array("status" => TRUE));
		}
    }



}

/* End of file download.php */
/* Location: ./application/views/admin/download.php */
