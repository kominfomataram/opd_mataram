<?php


class Halaman extends CI_Controller
{
    # Bismillah
    # do it clear and clean

    function __construct()
    {
      parent::__construct();
      $status_log = $this->session->userdata('is_logged_in');
  		if ($status_log != '1' && $status_log != '2' && $status_log != '3') {
  			$this->session->set_flashdata('msg','Please Login to Continue');
  	    redirect('admin/Login');
      }
      $this->load->model('PageModel');
    }

    public function halaman_tambah()
    {
        # code...
        $data['js'] = 'admin/halaman_detail';
        $this->load->view('admin/template/meta');
		    $this->load->view('admin/template/header');
		    $this->load->view('admin/template/sidebar');
		    $this->load->view('admin/halaman/halaman_tambah');
		    $this->load->view('admin/template/footer',$data);
    }

    public function halaman_data()
    {
        # code...
        $data['halaman'] = $this->PageModel->read_page_all();
        $this->load->view('admin/template/meta');
		    $this->load->view('admin/template/header');
		    $this->load->view('admin/template/sidebar');
		    $this->load->view('admin/halaman/halaman_data' , $data);
		    $this->load->view('admin/template/footer');
    }

    public function halaman_detail($kode = null)
    {
      # code...
      $data['js'] = 'admin/halaman_detail';
      $data['halaman'] = $this->PageModel->read_page_isi($kode);
      $this->load->view('admin/template/meta');
      $this->load->view('admin/template/header');
      $this->load->view('admin/template/sidebar');
      $this->load->view('admin/halaman/halaman_detail' , $data);
      $this->load->view('admin/template/footer',$data);
    }

    public function halaman_create()
    {
        $judul = $_POST['judul'];
        $kode = bersihkan_kata(str_replace(" ", "-" , strtolower($judul)));
        $isi = $_POST['editor1'];
        $tipe = $_POST['tipe'];
        $kategori = $_POST['kategori'];
        $admin = $this->session->userdata('username');
        $data = array('admin_username' => $admin ,
                      'judul_page' => $judul ,
                      'isi_page' => $isi ,
                      'kode_page' => $kode ,
                      'tipe_page' => $tipe ,
                      'created_time' => date("Y-m-d h:i:sa") ,
                      'kategori' => $kategori);
        $this->PageModel->create_page($data);
        log_record_data($data , 'MEMBUAT HALAMAN');
        redirect('admin/halaman/data_halaman');
    }

    public function halaman_update()
    {
      $judul = $_POST['judul'];
      $id = $_POST['id_page'];
      $kode = bersihkan_kata(str_replace(" ", "-" , strtolower($judul)));
      $isi = $_POST['editor1'];
      $tipe = $_POST['tipe'];
      $kategori = $_POST['kategori'];
      $admin = $this->session->userdata('username');
      $data = array('admin_username' => $admin , 'judul_page' => $judul ,
                    'isi_page' => $isi , 'kode_page' => $kode , 'tipe_page' => $tipe ,
                    'created_time' => date("Y-m-d h:i:sa") , 'kategori' => $kategori );
      log_record_data($data , 'MENGUPDATE HALAMAN');
      $this->PageModel->update_page($id , $data);
      redirect('admin/halaman/data_halaman');
    }

    public function halaman_delete($kode = null)
    {
      # code...
      $this->PageModel->delete_page($kode);
      log_record_data($kode, 'MENGHAPUS HALAMAN');
      redirect('admin/halaman/data_halaman');
    }
}
?>
