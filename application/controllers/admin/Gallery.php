<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$status_log = $this->session->userdata('is_logged_in');
		if ($status_log != '1' && $status_log != '2' && $status_log != '3') {
			$this->session->set_flashdata('msg','Please Login to Continue');
	    redirect('admin/Login');
    }
		$this->load->model('GalleryModel');
	}

	public function index()
	{
		$data['js']= 'gallery';
		$data['meta']='gallery';
		$data['gallery'] = $this->GalleryModel->get_all_album();
		$this->load->view('admin/template/meta',$data);
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/gallery/gallery_data',$data);
		$this->load->view('admin/template/footer',$data);
	}

	public function gallery_folder_create()
	{
		$nama = $this->input->post('nama');
		$tanggal = date('Y-m-d h:i:sa');
		$username = $this->session->userdata('username');
		$nama_baru = bersihkan_kata(str_replace(" ", "-" , strtolower($nama)));
		$data = array('judul' => $nama , 'tanggal_create' => $tanggal , 'admin_username' => $username );

		$lokasi_folder = FCPATH."media/gallery/".$nama_baru;
		if (mkdir( $lokasi_folder , 0777)){
			$this->session->set_flashdata('msg1','Album berhasil dibuat.');
			$this->GalleryModel->create_album($data);
		} // buat folder
		log_record_data($data , 'MEMBUAT ALBUM');
		redirect('admin/gallery/');
	}

	public function gallery_folder_delete($id = null , $nama)
	{
		$nama_baru = bersihkan_kata(str_replace(" ", "-" , strtolower($nama)));
		$lokasi_folder = FCPATH."media/gallery/".$nama_baru;
		$this->load->helper("file"); // load the helper
		if(delete_files($lokasi_folder, true)){ // delete all files/folders
			$this->session->set_flashdata('msg1','Album berhasil dihapus secara permanen!');
		}
		rmdir($lokasi_folder); // buat folder
		$this->GalleryModel->delete_album($id);
		log_record_data($lokasi_folder , 'MENGHAPUS ALBUM');

		redirect('admin/gallery/');
	}

	public function gallery_folder_update()
	{
		$judul1 = $_POST['judul_lama'];
		$judul2 = $_POST['judul_baru'];
		$judul_baru = bersihkan_kata(str_replace(" ", "-" , strtolower($judul2)));
		$lokasi_folder1 = FCPATH."media/gallery/".$judul1;
		$lokasi_folder2 = FCPATH."media/gallery/".$judul2;
		if (rename($lokasi_folder1,$lokasi_folder2)){
			$data = array('judul'=>$judul2);
			$this->GalleryModel->update_album($id,$data);
			$this->session->set_flashdata('msg1','Album berhasil diubah!');
		} // buat folder
		log_record_data($lokasi_folder , 'MENGUBAH NAMA ALBUM');
		redirect('admin/gallery/');
	}

	public function gallery_tambah_foto($kode_folder = null)
	{
		$data['js']= 'gallery/gallery_tambah_foto';
		$data['meta']='gallery';
		$data['nama_folder'] = $this->GalleryModel->get_folder_by_id_album($kode_folder);
		$data['foto'] = $this->GalleryModel->get_foto_by_judul_album($data['nama_folder']->judul , 0);
		$this->load->view('admin/template/meta',$data);
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/gallery/gallery_tambah_foto',$data);
		$this->load->view('admin/template/footer',$data);
	}

	public function gallery_upload($folder=null)
	{
        $config['upload_path']   = FCPATH.'/media/gallery/'.$folder.'/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|ico';
        $this->load->library('upload',$config);
				$tanggal = date('Y-m-d h:i:sa');
				$username = $this->session->userdata('username');
        if($this->upload->do_upload('userfile')){
        	$token=$this->input->post('token_foto');
        	$nama=$this->upload->data('file_name');
					$data = array('nama'=>$nama,'tanggal_posting'=>$tanggal_create,'nama_album'=>$folder,'admin_username'=>$username,'token'=>$token , 'softdelete_status' => '0');
					log_record_data($data , 'MENAMBAH FOTO');
					$this->db->insert('foto', $data);
        }

	}

	public function gallery_remove($folder=null)
	{
		$token=$this->input->post('token');
		$foto=$this->db->get_where('foto',array('token'=>$token));
		if($foto->num_rows()>0){
			$hasil=$foto->row();
			$nama_foto=$hasil->nama;
			log_record_data($nama_foto , 'MENGHAPUS FOTO');
			if(file_exists($file=FCPATH.'/media/gallery/'.$folder.'/'.$nama_foto)){
				unlink($file);
			}
			$this->db->delete('foto',array('token'=>$token));
		}
		echo "{}";
	}

	public function gallery_foto_delete($folder=null,$nama_foto=null)
	{
		log_record_data($nama_foto , 'MENGHAPUS FOTO');
		if(file_exists($file=FCPATH.'/media/gallery/'.$folder.'/'.$nama_foto))
		{
			unlink($file);
		}
		$this->db->delete('foto',array('nama'=>$nama_foto));
	    echo json_encode(array("status" => TRUE));

	}

	public function json_data_album()
	{
		echo json_encode($this->GalleryModel->get_all_album());
	}

	public function json_data_album_by_id($id)
	{
		echo json_encode($this->GalleryModel->get_album_by_id($id));
	}
}

/* End of file gallery.php */
/* Location: ./application/views/admin/gallery.php */
