<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Link_terkait extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$status_log = $this->session->userdata('is_logged_in');
		if ($status_log != '1' && $status_log != '2' && $status_log != '3') {
			$this->session->set_flashdata('msg','Please Login to Continue');
	    	redirect('admin/Login');
    	}
   		$this->load->model('LinkModel');
		$this->load->library('JajeTujaqLib');
	}

	public function index()
	{
		$data['js'] = 'link_terkait/link_data';
		$data['link'] = $this->LinkModel->get_all_link(0);
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/link/link_terkait',$data);
		$this->load->view('admin/template/footer',$data);
	}

	public function link_terkait_tambah()
	{
		$data['js'] = '';
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/link/link_terkait_tambah');
		$this->load->view('admin/template/footer',$data);
	}

	public function link_terkait_ubah($id = null)
	{
		$data['js'] = '';
		$data['link'] = $this->LinkModel->read_link_by_id($id);
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/link/link_terkait_update',$data);
		$this->load->view('admin/template/footer',$data);
	}

	public function link_terkait_create()
	{
		$link = $this->input->post('link');
		$text = $this->input->post('text');
	    $gambar   = $this->input->post('gbr');

		$this->load->library('upload');
		$nmfile = "file_banner_".time();
		$config['upload_path']   = './media/banner/';
		$config['allowed_types'] = 'jpg|jpeg|png|';
		$config['max_size']      = '500';
		$config['file_name'] 	 = $nmfile;

		$this->upload->initialize($config);

        if($_FILES['gbr']['name'])
        {
            if ($this->upload->do_upload('gbr'))
            {
                $gbr = $this->upload->data();
                $data = array(
            		'link' => $link ,
					'gbr'  => "media/banner/".$gbr['file_name'],
					'text' => $text,
					'softdelete_status' => 0
                );
				$this->LinkModel->create_link($data);
            	$this->session->set_flashdata("berhasil", "<b>Data berhasil ditambahkan.</b>");

				redirect('admin/link_terkait');
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", $this->upload->display_errors()."<b>Gagal upload gambar! Pastikan format dan ukuran gambar sesuai syarat.</b>");
				redirect('admin/link_terkait/tambah_link_terkait');
            }
        }else{
            //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
            $this->session->set_flashdata("pesan", "<b>Gagal upload gambar! Pastikan format dan ukuran gambar sesuai syarat.</b>");
			redirect('admin/link_terkait/tambah_link_terkait');
        }
		
	}

	public function link_terkait_update()
	{
		$id = $this->input->post('id');
		$link = $this->input->post('link');
		$text = $this->input->post('text');
	    $gambar   = $this->input->post('gbr');

		$this->load->library('upload');
		$nmfile = "file_banner_".time();
		$config['upload_path']   = './media/banner/';
		$config['allowed_types'] = 'jpg|jpeg|png|';
		$config['max_size']      = '500';
		$config['file_name'] 	 = $nmfile;

		$this->upload->initialize($config);

            if ($this->upload->do_upload('gbr'))
            {
                $gbr = $this->upload->data();
                $data = array(
            		'link' => $link ,
					'gbr'  => "media/banner/".$gbr['file_name'],
					'text' => $text
                );
                $this->LinkModel->update_link($id , $data);
            	$this->session->set_flashdata("berhasil", "<b>Data berhasil ditambahkan.</b>");
				redirect('admin/link_terkait');
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
		 		$data = array(
            		'link' => $link ,
					'text' => $text
                );
                $this->LinkModel->update_link($id , $data);
                $this->session->set_flashdata("berhasil", $this->upload->display_errors()."<b>Data berhasil ditambahkan tanpa foto.</b>");
				redirect('admin/link_terkait');

            }
	}

	// public function link_terkait_delete($id = null)
	// {
	// 	$this->LinkModel->delete_link($id);
	// 	redirect('admin/link_terkait');
		
	// }

	public function link_restore($id)
	{
		$data = array('softdelete_status' => 0);
		$this->LinkModel->update_link($id , $data);
		$this->session->set_flashdata('berhasil','Link terkait telah berhasil dikembalikan!');
		log_record_data('' , 'MENGEMBALIKAN LINK TERKAIT DENGAN ID = '.$id);
	    echo json_encode(array("status" => TRUE));
	}

	
	public function link_terkait_trash()
	{
		$data['js'] = 'link_terkait/link_data';
		$data['link'] = $this->LinkModel->get_all_link(1);
		$this->load->view('admin/template/meta');
		$this->load->view('admin/template/header');
		$this->load->view('admin/template/sidebar');
		$this->load->view('admin/link/link_terkait_trash',$data);
		$this->load->view('admin/template/footer',$data);
	}

    public function link_terkait_delete($id , $type )
    {
		if ($type == 1) {
			# code...
		    $file=$this->LinkModel->get_url($id);
		    if (isset($file->gbr)){
		    	unlink($file->gbr);
		    }
		    $this->LinkModel->delete_link($id);
		    $this->session->set_flashdata('berhasil','File telah berhasil dihapus secara permanen!');
				log_record_data('' , 'MENGHAPUS FILE DOWNLOAD');
		    echo json_encode(array("status" => TRUE));
		}
		elseif ($type == 0) {
		    $this->LinkModel->deletesoft_link($id);
		    $this->session->set_flashdata('berhasil','File telah berhasil dihapus!');
				log_record_data('' , 'SOFT DELETE FILE DOWNLOAD');
		    echo json_encode(array("status" => TRUE));
		}
    }
}

/* End of file Link_terkait.php */
/* Location: ./application/controllers/admin/Link_terkait.php */