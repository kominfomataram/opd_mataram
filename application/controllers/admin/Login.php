<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
         parent::__construct();
        $this->load->model('AdminModel');
    }
    /*
    *Showing  Login page here
    */
    public function index(){
        $this->load->view('admin/template/meta');
        $this->load->view('admin/login');
    }

    /**
    * check the username and the password with the database
    * @return void
    */

    public function validasi(){
    	$username = $this->input->post('username');
    	$password = md5($this->input->post('password'));
    	$is_valid = $this->AdminModel->validate($username, $password);

    	if($is_valid){/*If valid username and password set */
    		//$get_data = $this->AdminModel->get_data($username, $password);
            $this->db->select('*');
            $this->db->from('admin');
            $this->db->join('level_admin', 'level_admin.id_level_admin = admin.id_level_admin');
            $this->db->where('password', $password);
            $this->db->where('username', $username);
            $get_data = $this->db->get()->result();

    		foreach($get_data as $val)
    		{
    			$username       = $val->username;
                $nama           = $val->nama;
                $telpon         = $val->telpon;
                $email          = $val->email;
                $alamat         = $val->alamat;
                $jenis_kelamin  = $val->jenis_kelamin;
    			      $password       = $val->password;
                $gambar         = $val->gambar;
                $status         = $val->status;
                $id_level_admin = $val->id_level_admin;
                $nama_level_admin = $val->nama_level;
                $isi_biodata     = $val->isi_biodata;


    			if(($id_level_admin=='1') && $status == '1'){
    				$data = array(
            			      'username'=> $username,
                        'nama'=> $nama,
                        'telpon'=> $telpon,
                        'email'=> $email,
                        'alamat'=> $alamat,
                        'jenis_kelamin'=> $jenis_kelamin,
                        'password'=> $password,
                        'gambar'=> $gambar,
                        'status'=> $status,
                        'id_level_admin'=> $id_level_admin,
                        'nama_level_admin'=> $nama_level_admin,
                        'isi_biodata'=> $isi_biodata,
                        'is_logged_in'=> '1',
                        'file_manager'=>true,
    				);
    				$this->session->set_userdata($data);
                    // session_start();
                    // $_SESSION['KCFINDER']                 = array();
                    // $_SESSION['KCFINDER']['disabled']     = false;
                     /*Here  setting the Admin datas in session */
						log_logged('LOGIN');
  					redirect('admin/Dashboard');
    			}elseif(($id_level_admin == '2') && ($status == '1')){
    				$data = array(
    				          	'username'=> $username,
                        'nama'=> $nama,
                        'telpon'=> $telpon,
                        'email'=> $email,
                        'alamat'=> $alamat,
                        'jenis_kelamin'=> $jenis_kelamin,
                        'password'=> $password,
                        'gambar'=> $gambar,
                        'status'=> $status,
                        'id_level_admin'=> $id_level_admin,
                        'nama_level_admin'=> $nama_level_admin,
                        'isi_biodata'=> $isi_biodata,
                        'is_logged_in'=> '2',
                        'file_manager'=>true,
    				);
    				$this->session->set_userdata($data); /*Here  setting the Admin datas in session */
                    // session_start();
                    // $_SESSION['KCFINDER']                 = array();
                    // $_SESSION['KCFINDER']['disabled']     = false;
					  log_logged('LOGIN');
						//print_r($data);
    				redirect('admin/Dashboard');
    			}
					elseif(($id_level_admin=='3') && ($status == '1')){
                    $data = array(
                        'username'=> $username,
                        'nama'=> $nama,
                        'telpon'=> $telpon,
                        'email'=> $email,
                        'alamat'=> $alamat,
                        'jenis_kelamin'=> $jenis_kelamin,
                        'password'=> $password,
                        'gambar'=> $gambar,
                        'status'=> $status,
                        'id_level_admin'=> $id_level_admin,
                        'nama_level_admin'=> $nama_level_admin,
                        'isi_biodata'=> $isi_biodata,
                        'is_logged_in'=> '3',
                        'file_manager'=>true,
                    );
                    $this->session->set_userdata($data); /*Here  setting the Admin datas in session */
                    // session_start();
                    // $_SESSION['KCFINDER']                 = array();
                    // $_SESSION['KCFINDER']['disabled']     = false;
										log_logged('LOGIN');
										//print_r($data);
									  redirect('admin/Dashboard');
          }else
					{
                    $this->session->set_flashdata('msg1', 'Akun anda di suspend!');
                    redirect('admin/Login');
          }
    		}
        }else {// incorrect username or password
        	$this->session->set_flashdata('msg1', 'Username atau Password Salah!');
        	redirect('admin/Login');
        }
    }

    /**
        * Unset the session, and logout the user.
        * @return void
    */
    public function logout(){
      $array_items = array(
        'username',
        'nama',
        'telpon',
        'email',
        'alamat',
        'jenis_kelamin',
        'password',
        'gambar',
        'status',
        'isi_biodata',
        'id_level_admin',
        'nama_level_admin',
        'is_logged_in',
      );
	  log_logged('LOGOUT');
      $this->session->unset_userdata($array_items);
      $this->session->sess_destroy();
      redirect('admin/Login');
    }

}

/* End of file login.php */
/* Location: ./application/controllers/admin/login.php */
