<?php 
/**
* 
*/
class RegisterModel extends CI_Model
{
	public function cek_db($username,$email)
	{
		$this->db->where('username', $username);
		$this->db->or_where('email', $email);
		$query = $this->db->get('login');
		return $query->row();
	}

	public function add_user($data)
	{
		$this->db->insert('admin',$data);
	}

	public function add_data_akun($data)
	{
		$this->db->insert('admin',$data);
	}

	public function update_status_data_akun($username)
	{
		$this->db->where('username', $username);
     	$this->db->update('admin', array('data_akun' => 1));
	}

}
?>