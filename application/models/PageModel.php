<?php

class PageModel extends CI_Model
{

    public function read_page_by_kodepage($kode = null)
    {
        return $this->db->select('kode_page , tipe_page')
                        ->from('page')
                        ->where('kode_page',$kode)->get()->result_array();
    }

    public function read_page_isi($kode = null)
    {
        return $this->db->select('*')
                        ->from('page')
                        ->where('kode_page',$kode)->get()->result_array();
    }

    public function read_page_by_something($select = null , $parameter = null)
    {
        return $this->db->select($select)
                        ->from('page')
                        ->where($parameter)->get()->result_array();
    }

    public function read_page_all()
    {
        return $this->db->get('page')->result_array();
    }

    public function create_page($data)
    {
        $this->db->insert('page',$data);
    }

    public function delete_page($id_page)
    {
        $this->db->where('kode_page' , $id_page);
        $this->db->delete('page');
    }

    public function update_page($kode,$data)
    {
        $this->db->where('id_page',$kode);
        $this->db->update('page',$data);
    }

}


?>
