<?php

/**
 *
 */
class WebMasterModel extends CI_Model
{

  public function update_halaman_depan($data)
  {
    # code...
    $this->db->update('static',$data);
  }

  public function update_menu_generator($data)
  {
    # code...
    $this->db->update('menu_temp',$data);
  }

  public function create_menu_generator($data)
  {
    # code...
    $this->db->insert('menu_temp',$data);
  }

  public function read_menu_generator()
  {
    return $this->db->get('menu_temp')->result_array();
  }




}


?>
