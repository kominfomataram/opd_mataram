<?php

class AdminModel extends CI_Model {

    /**
    * Validate the login's data with the database
    * @param string $username
    * @param string $password
    * @return void
    */

    /*Login Admin*/
   	public function validate($username, $password){
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->join('level_admin', 'level_admin.id_level_admin = admin.id_level_admin');
		$this->db->where('password', $password);
		$this->db->where('username', $username);
		return $this->db->get()->result();
	}

	/*Data Session */
	public function get_data($username, $password)
	{
		$this->db->where('password', $password);
		$this->db->where('username', $username);
		return $this->db->get('admin')->result();
	}
	// Ambil Data Gambar
	public function get_url_gbr($username)
	{
		$query = $this->db->get_where('admin', array('login_username' => $username));
		return $query->result();
	}

	public function update_admin_by_self($username , $data)
	{
		$this->db->where('username' , $username);
		$this->db->update('admin' , $data);
	}

	public function cek_db($username,$email)
	{
		$this->db->where('username', $username);
		$this->db->or_where('email', $email);
		$query = $this->db->get('admin');
		return $query->row();
	}

	public function add_admin($data)
	{
		$this->db->insert('admin',$data);
	}

	public function update_data_akun($data,$username)
	{
		$this->db->where('username', $username);
    $this->db->update('admin', $data);
	}

  public function read_admin_by_username($username = null)
  {
    return $this->db->where('username' , $username)->get('admin')->result_array();
  }

  public function read_admin_all()
  {
    # code...
    return $this->db->get('admin')->result_array();
  }
}
