<?php

/**
 *
 */
class LinkModel extends CI_Model
{

  public function read_link_all()
  {
    $this->db->where('berita.softdelete_status = 0');
    return $this->db->get('link_terkait')->result_array();
  }

  public function get_all_berita($softdelete)
  {
    $this->db->select('*');
    $this->db->from('berita');
    $this->db->join('kategori_berita', 'kategori_berita.id_kategori_berita = berita.id_kategori_berita');
    $this->db->where('berita.softdelete_status = '.$softdelete);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function get_all_link($softdelete)
  {
           $this->db->select('*')
                    ->from('link_terkait')
                    ->where('softdelete_status', $softdelete);
    return $this->db->get()->result_array();
  }

  public function read_link_by_id($id)
  {
    return $this->db->where('id' , $id)
                    ->get('link_terkait')->result_array();
  }

  public function create_link($data)
  {
    $this->db->insert('link_terkait',$data);
  }

  public function update_link($id , $data)
  {
    $this->db->where('id' , $id);
    $this->db->update('link_terkait' , $data);
  }

  public function update_link_restore($id , $data)
  {
    $this->db->where('id' , $id);
    $this->db->update('link_terkait' , $data);
  }


  public function delete_link($id)
  {
    $this->db->where('id' , $id);
    $this->db->where('softdelete_status',1);
    $this->db->delete('link_terkait');
  }

  public function get_url($id)
  {
    $this->db->select('gbr');
    $this->db->from('link_terkait');
    $this->db->where('id' , $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function deletesoft_link($id)
  {
    $data = array('softdelete_status' => 1);
    $this->db->where('id',$id);
    $this->db->update('link_terkait', $data);
  }


}


?>
