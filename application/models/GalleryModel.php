<?php

/**
 *
 */
class GalleryModel extends CI_Model
{

  public function get_all_album()
  {    
    return $this->db->get('album')
                ->result_array();
  }

  public function get_album_by_id($id)
  {
    return $this->db->where('id_album' , $id)->get('album')
                ->result_array();
  }

  public function get_foto_by_judul_album($id , $softdelete)
  {
           $this->db->select('*')
                    ->from('foto')
                    ->where('nama_album',$id)
                    ->where('softdelete_status', $softdelete);
    return $this->db->get()->result_array();
  }

  public function get_all_foto_trash($softdelete)
  {
           $this->db->select('*')
                    ->from('foto')
                    ->where('softdelete_status', $softdelete);
    return $this->db->get()->result_array();
  }

  public function get_folder_by_id_album($id)
  {
           $this->db->select('judul')
                    ->from('album')
                    ->where('id_album',$id);
    return $this->db->get()->row();
  }

  public function create_album($data)
  {
    $this->db->insert('album',$data);
  }

  public function delete_album($id)
  {
    $this->db->where('id_album' , $id)->delete('album');
  }

  public function update_album($id , $data)
  {
    $this->db->update('album', $data);
		$this->db->where('id_album', $id);
  }

  # PENCARIAN
	public function search_foto($keyword)
	{
		return $this->db->select('*')
						        ->from('nama')
						        ->like('judul' ,'%'.$keyword.'%')->result_array();
	}

}


?>
