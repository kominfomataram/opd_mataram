<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BeritaModel extends CI_Model {

	public function get_all_kategori()
	{
		return $this->db->get('kategori_berita')->result_array();
	}

	public function get_all_berita($softdelete)
	{
		$this->db->select('*');
		$this->db->from('berita');
		$this->db->join('kategori_berita', 'kategori_berita.id_kategori_berita = berita.id_kategori_berita');
		$this->db->where('berita.softdelete_status = '.$softdelete);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_one_berita($id)
	{
		$this->db->select('*');
		$this->db->from('berita');
		$this->db->join('kategori_berita', 'kategori_berita.id_kategori_berita = berita.id_kategori_berita');
		$this->db->where('berita.id_berita', $id);
		$query = $this->db->get();
		return $query->row();
	}

	public function create_berita($data)
	{
		$this->db->insert('berita', $data);
	}

	public function update_berita($id,$data)
	{
		$this->db->where('id_berita', $id);
		$this->db->update('berita', $data);
	}

	public function delete_berita($id)
	{
		$this->db->delete('berita',$id);
		$this->session->set_flashdata('msg1','Berita berhasil dihapus!');
	}

	public function deletesoft_berita($id)
	{
		$data = array('softdelete_status' => 1);
		$this->db->where('id_berita',$id);
		$this->db->update('berita', $data);
		$this->session->set_flashdata('msg1','Berita berhasil di soft delete!');
	}

	public function return_berita($id)
	{
		$data = array('softdelete_status' => 0);
		$this->db->where('id_berita',$id);
		$this->db->update('berita', $data);
		$this->session->set_flashdata('msg1','Berita berhasil di return!');
	}

	public function suspen_berita($id,$data)
	{
		$this->db->where('id_berita', $id);
		$this->db->update('berita', $data);
		$this->session->set_flashdata('msg1','Berita berhasil suspen!');
	}

	public function publish_berita($id,$data)
	{
		$this->db->where('id_berita', $id);
		$this->db->update('berita', $data);
		$this->session->set_flashdata('msg1','Berita berhasil publish!');
	}

	//kategori berita
	public function delete_kategori_berita($id)
	{
		$this->db->delete('kategori_berita',$id);
		$this->session->set_flashdata('msg1','Kategori berita berhasil dihapus!');
	}

	public function get_kategori_berita($id)
	{
		$query = $this->db->get_where('kategori_berita', array('id_kategori_berita' => $id));
		return $query->row();
	}

	public function update_kategori_berita($id,$data)
	{
		$this->db->where('id_kategori_berita', $id);
		$this->db->update('kategori_berita', $data);
		$this->session->set_flashdata('msg1','Kategori berita berhasil diubah!');
	}


	public function create_kategori_berita($data)
	{
		$this->db->insert('kategori_berita', $data);
		$this->session->set_flashdata('msg1','Kategori berita berhasil ditambahkan!');

	}

	## SOME QUERY BERITA UNTUK DEPAN
	public function get_all_berita_front_by_kode($kode_berita)
	{
	  $this->db->select('*')
						 ->from('berita')
						 ->join('kategori_berita' , 'berita.id_kategori_berita = kategori_berita.id_kategori_berita' , 'left')
						 ->join('admin' , 'admin.username = berita.author' , 'left')
						 ->where('berita.kode_berita',$kode_berita);
		return $this->db->get()->result_array();
	}

	## SOME QUERY BERITA
	public function search_berita_front($keyword)
	{
		return $this->db->select('*')
						 ->from('berita')
						 ->like('isi_berita' ,'%'.$keyword.'%')->result_array();
	}

	# UNTUK DEPAN
	public function get_all_berita_front()
	{
	  $this->db->select('*')
						 ->from('berita')
						 ->join('kategori_berita' , 'berita.id_kategori_berita = kategori_berita.id_kategori_berita' , 'left')
						 ->join('admin' , 'admin.username = berita.author' , 'left')
						 ->where('berita.status = 1')
						 ->where('berita.softdelete_status = 0');
		return $this->db->get()->result_array();
	}

	public function get_all_berita_front_temp($num , $offset)
	{
	  $query = $this->db->select('*')
						 ->from('berita')
						 ->join('kategori_berita' , 'berita.id_kategori_berita = kategori_berita.id_kategori_berita' , 'left')
						 ->join('admin' , 'admin.username = berita.author' , 'left')
						 ->where('berita.status = 1')->where('berita.softdelete_status = 0')->order_by('id_berita DESC')->limit($num , $offset);
		return $query->get();
	}

	public function get_all_berita_front_jumlah()
	{
	  $this->db->select('*')
						 ->from('berita')
						 ->join('kategori_berita' , 'berita.id_kategori_berita = kategori_berita.id_kategori_berita' , 'left')
						 ->join('admin' , 'admin.username = berita.author' , 'left')
						 ->where('berita.status = 1');
		return $this->db->get()->num_rows();
	}

	public function get_all_berita_front2()
	{
	  return $this->db->select('*')
						 ->from('berita')
						 ->join('kategori_berita' , 'berita.id_kategori_berita = kategori_berita.id_kategori_berita' , 'left')
						 ->join('admin' , 'admin.username = berita.author' , 'left')->get();

	}


	# PENCARIAN
	public function search_berita($keyword)
	{
		return $this->db->select('*')
						        ->from('berita')
						        ->like('isi_berita' ,'%'.$keyword.'%')->result_array();
	}


}

/* End of file BeritaModel.php */
/* Location: ./application/models/BeritaModel.php */
