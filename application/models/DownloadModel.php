<?php

/**
 *
 */
class DownloadModel extends CI_Model
{

  public function read_download_all()
  {
    return $this->db->get('download')->result_array();
  }

  public function get_all_download()
  {
    $this->db->select('*');
    $this->db->from('download');
    $this->db->where('softdelete_status = 0');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function get_all_download_trash()
  {
    $this->db->select('*');
    $this->db->from('download');
    $this->db->where('softdelete_status = 1');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function read_download_by_id($id)
  {
    return $this->db->where('id_download' , $id)
                    ->get('download')->result_array();
  }

  # PENCARIAN
  public function search_file($keyword)
  {
    return $this->db->select('*')
                    ->from('berita')
                    ->like('isi_berita' ,'%'.$keyword.'%')->result_array();
  }

  public function create_file($data)
  {
    $this->db->insert('download',$data);
  }

  public function update_file($id , $data)
  {
    $this->db->where('id_download' , $id);
    $this->db->update('download' , $data);
  }

  public function delete_file($id)
  {
    $this->db->where('id_download',$id);
    $this->db->where('softdelete_status',1);
    $this->db->delete('download');
  }

  public function get_file_url($id)
  {
    $this->db->select('url_file');
    $this->db->from('download');
    $this->db->where('id_download' , $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function deletesoft_download($id)
  {
    $data = array('softdelete_status' => 1);
    $this->db->where('id_download',$id);
    $this->db->update('download', $data);
  }

}


?>
