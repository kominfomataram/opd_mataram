-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2018 at 07:10 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cmsopdbaru`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(30) NOT NULL,
  `nama` varchar(40) DEFAULT NULL,
  `telpon` varchar(15) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `alamat` varchar(45) DEFAULT NULL,
  `jenis_kelamin` tinyint(4) DEFAULT NULL,
  `password` varchar(65) DEFAULT NULL,
  `gambar` varchar(150) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isi_biodata` tinyint(4) DEFAULT NULL,
  `id_level_admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `nama`, `telpon`, `email`, `alamat`, `jenis_kelamin`, `password`, `gambar`, `status`, `isi_biodata`, `id_level_admin`) VALUES
('admin', 'Admin', NULL, NULL, NULL, 1, '21232f297a57a5a743894a0e4a801fc3', NULL, 1, 1, 2),
('author', 'Author', NULL, NULL, NULL, 1, '21232f297a57a5a743894a0e4a801fc3', NULL, 1, 1, 3),
('superadmin', 'Superadmin', NULL, NULL, NULL, 1, '21232f297a57a5a743894a0e4a801fc3', NULL, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id_album` int(11) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `tanggal_create` datetime DEFAULT NULL,
  `admin_username` varchar(30) NOT NULL,
  `softdelete_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `judul_berita` varchar(45) DEFAULT NULL,
  `kode_berita` varchar(45) DEFAULT NULL,
  `isi_berita` text,
  `created_time` date DEFAULT NULL,
  `updated_time` date DEFAULT NULL,
  `id_kategori_berita` int(11) NOT NULL,
  `author` varchar(30) NOT NULL,
  `keyword` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `softdelete_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `download`
--

CREATE TABLE `download` (
  `id_download` int(11) NOT NULL,
  `judul` varchar(45) DEFAULT NULL,
  `url_file` varchar(300) DEFAULT NULL,
  `admin_username` varchar(30) NOT NULL,
  `softdelete_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `foto`
--

CREATE TABLE `foto` (
  `id_foto` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `tanggal_posting` varchar(45) DEFAULT NULL,
  `nama_album` varchar(50) NOT NULL COMMENT 'Termasuk dalam album yang mana foto tersebut',
  `admin_username` varchar(30) NOT NULL,
  `token` varchar(100) NOT NULL,
  `softdelete_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_berita`
--

CREATE TABLE `kategori_berita` (
  `id_kategori_berita` int(11) NOT NULL,
  `nama_kategori` varchar(45) DEFAULT NULL,
  `deskripsi_kategori` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `level_admin`
--

CREATE TABLE `level_admin` (
  `id_level_admin` int(11) NOT NULL,
  `nama_level` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level_admin`
--

INSERT INTO `level_admin` (`id_level_admin`, `nama_level`) VALUES
(1, 'Super Admin'),
(2, 'Admin'),
(3, 'Author');

-- --------------------------------------------------------

--
-- Table structure for table `link_terkait`
--

CREATE TABLE `link_terkait` (
  `softdelete_status` tinyint(1) NOT NULL,
  `id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `gbr` varchar(100) NOT NULL,
  `text` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `link_terkait`
--

INSERT INTO `link_terkait` (`softdelete_status`, `id`, `link`, `gbr`, `text`) VALUES
(0, 1, 'http://lpse.mataramkota.go.id/', 'media/banner/file_banner_1520905051.jpg', 'LPSE'),
(0, 2, 'http://sirup.lkpp.go.id/', 'media/banner/file_banner_1520905616.jpg', 'SIRUP'),
(0, 3, 'http://sip-ppid.mataramkota.go.id/', 'media/banner/file_banner_1520905892.jpg', 'SIP PPID'),
(0, 4, 'http://www.mataramkota.go.id/file/ROAD%20MAP%20RB%20KOMAT.pdf', 'media/banner/file_banner_1520905993.jpg', 'ROAD MAP RB KOTA MATARAM'),
(0, 5, 'http://www.lapor.go.id/', 'media/banner/file_banner_1520906090.jpg', 'LAPOR'),
(0, 6, 'http://play.google.com/store/search?q=mataram%20SmartCity', 'media/banner/file_banner_1520906156.png', 'SMART CITY'),
(0, 7, 'http://www.mataramkota.go.id/file/SSH%202018.pdf', 'media/banner/file_banner_1520906216.jpg', 'SSH'),
(0, 8, 'http://simyandu.mataramkota.go.id', 'media/banner/file_banner_1520917688.jpg', 'SIMYANDU');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `aktivitas` longtext NOT NULL,
  `date_timestamp` datetime NOT NULL,
  `ip_address` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `username`, `aktivitas`, `date_timestamp`, `ip_address`) VALUES
(1, 'superadmin', 'Melakukan LOGIN Pada System', '2018-09-16 03:39:30', '::1'),
(2, 'superadmin', 'Melakukan LOGIN Pada System', '2018-09-16 06:53:56', '::1'),
(3, 'superadmin', 'Melakukan LOGOUT Pada System', '2018-09-16 07:00:21', '::1'),
(4, 'superadmin', 'Melakukan LOGIN Pada System', '2018-09-16 07:00:50', '::1'),
(5, 'superadmin', 'Melakukan LOGIN Pada System', '2018-09-16 07:06:47', '::1'),
(6, 'superadmin', 'Melakukan LOGOUT Pada System', '2018-09-16 07:09:18', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(45) DEFAULT NULL,
  `icon` varchar(45) DEFAULT NULL,
  `slug` varchar(45) DEFAULT NULL,
  `number` varchar(45) DEFAULT NULL,
  `id_parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu_temp`
--

CREATE TABLE `menu_temp` (
  `menu` varchar(500) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id_page` int(11) NOT NULL,
  `judul_page` varchar(45) DEFAULT NULL,
  `isi_page` tinytext,
  `created_time` date DEFAULT NULL,
  `updated_time` date DEFAULT NULL,
  `tipe_page` varchar(10) DEFAULT NULL,
  `admin_username` varchar(30) NOT NULL,
  `kode_page` varchar(45) DEFAULT NULL,
  `kategori` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id_page`, `judul_page`, `isi_page`, `created_time`, `updated_time`, `tipe_page`, `admin_username`, `kode_page`, `kategori`) VALUES
(18, 'Beranda', NULL, NULL, NULL, NULL, 'admin', NULL, NULL),
(19, 'Berita', NULL, NULL, NULL, NULL, 'admin', 'berita', NULL),
(20, 'Gallery', NULL, NULL, NULL, NULL, 'admin', 'gallery', NULL),
(21, 'Download', NULL, NULL, NULL, NULL, 'admin', 'download', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `static`
--

CREATE TABLE `static` (
  `id_static` int(11) NOT NULL,
  `nama_dinas` varchar(100) DEFAULT NULL COMMENT 'Nama site setiap dinas',
  `home_gbr_1` varchar(150) DEFAULT NULL COMMENT 'URL Untuk Gambar Home Depan\n',
  `home_gbr_2` varchar(150) DEFAULT NULL COMMENT 'URL Untuk Gambar Home Depan\n',
  `home_gbr_3` varchar(150) DEFAULT NULL COMMENT 'URL Untuk Gambar Home Depan\n',
  `nama_kementerian` varchar(80) DEFAULT NULL,
  `alamat_kementerian` varchar(150) DEFAULT NULL,
  `link_kementerian` varchar(45) DEFAULT NULL,
  `nama_provinsi` varchar(80) DEFAULT NULL,
  `alamat_provinsi` varchar(150) DEFAULT NULL,
  `link_provinsi` varchar(45) DEFAULT NULL,
  `link_kabupaten` varchar(45) DEFAULT NULL,
  `logo_kementrian` varchar(150) DEFAULT NULL,
  `logo_provinsi` varchar(150) DEFAULT NULL,
  `logo_instansi` varchar(150) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `telpon` varchar(45) DEFAULT NULL,
  `jam_kerja` varchar(70) DEFAULT NULL,
  `warna` varchar(10) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `deskripsi_dinas` varchar(200) DEFAULT NULL,
  `facebook_dinas` varchar(100) DEFAULT NULL,
  `twitter_dinas` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `static`
--

INSERT INTO `static` (`id_static`, `nama_dinas`, `home_gbr_1`, `home_gbr_2`, `home_gbr_3`, `nama_kementerian`, `alamat_kementerian`, `link_kementerian`, `nama_provinsi`, `alamat_provinsi`, `link_provinsi`, `link_kabupaten`, `logo_kementrian`, `logo_provinsi`, `logo_instansi`, `email`, `telpon`, `jam_kerja`, `warna`, `alamat`, `deskripsi_dinas`, `facebook_dinas`, `twitter_dinas`) VALUES
(1, 'Data Tidak Ada', 'file_Data_Tidak_Ada_1530502111.png', NULL, NULL, NULL, NULL, 'Data Tidak Ada', NULL, NULL, 'Data Tidak Ada', NULL, NULL, NULL, NULL, 'coba@gmail.com', 'Data Tidak Ada', 'Data Tidak Ada', '#000000', 'Data Tidak Ada', 'Data Tidak Ada', 'Data Tidak Ada', 'Data Tidak Ada');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_admin_level_admin_idx` (`id_level_admin`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id_album`),
  ADD UNIQUE KEY `judul` (`judul`),
  ADD KEY `fk_album_admin1_idx` (`admin_username`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`id_download`),
  ADD KEY `fk_download_admin1_idx` (`admin_username`);

--
-- Indexes for table `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`id_foto`),
  ADD KEY `fk_foto_admin1_idx` (`admin_username`),
  ADD KEY `nama_album` (`nama_album`);

--
-- Indexes for table `kategori_berita`
--
ALTER TABLE `kategori_berita`
  ADD PRIMARY KEY (`id_kategori_berita`);

--
-- Indexes for table `level_admin`
--
ALTER TABLE `level_admin`
  ADD PRIMARY KEY (`id_level_admin`);

--
-- Indexes for table `link_terkait`
--
ALTER TABLE `link_terkait`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `fk_menu_menu1_idx` (`id_parent`);

--
-- Indexes for table `menu_temp`
--
ALTER TABLE `menu_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id_page`),
  ADD KEY `fk_page_admin1_idx` (`admin_username`);

--
-- Indexes for table `static`
--
ALTER TABLE `static`
  ADD PRIMARY KEY (`id_static`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id_album` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `download`
--
ALTER TABLE `download`
  MODIFY `id_download` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `foto`
--
ALTER TABLE `foto`
  MODIFY `id_foto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori_berita`
--
ALTER TABLE `kategori_berita`
  MODIFY `id_kategori_berita` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `level_admin`
--
ALTER TABLE `level_admin`
  MODIFY `id_level_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `link_terkait`
--
ALTER TABLE `link_terkait`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu_temp`
--
ALTER TABLE `menu_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id_page` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `static`
--
ALTER TABLE `static`
  MODIFY `id_static` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `fk_admin_level_admin` FOREIGN KEY (`id_level_admin`) REFERENCES `level_admin` (`id_level_admin`);

--
-- Constraints for table `album`
--
ALTER TABLE `album`
  ADD CONSTRAINT `fk_album_admin1` FOREIGN KEY (`admin_username`) REFERENCES `admin` (`username`);

--
-- Constraints for table `download`
--
ALTER TABLE `download`
  ADD CONSTRAINT `fk_download_admin1` FOREIGN KEY (`admin_username`) REFERENCES `admin` (`username`);

--
-- Constraints for table `foto`
--
ALTER TABLE `foto`
  ADD CONSTRAINT `fk_foto_admin1` FOREIGN KEY (`admin_username`) REFERENCES `admin` (`username`),
  ADD CONSTRAINT `foto_ibfk_1` FOREIGN KEY (`nama_album`) REFERENCES `album` (`judul`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `log_ibfk_1` FOREIGN KEY (`username`) REFERENCES `admin` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `fk_menu_menu1` FOREIGN KEY (`id_parent`) REFERENCES `menu` (`id_menu`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `fk_page_admin1` FOREIGN KEY (`admin_username`) REFERENCES `admin` (`username`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
