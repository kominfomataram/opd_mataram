-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema inspek_dbs
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema inspek_dbs
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `inspek_dbs` DEFAULT CHARACTER SET latin1 ;
USE `inspek_dbs` ;

-- -----------------------------------------------------
-- Table `inspek_dbs`.`level_admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inspek_dbs`.`level_admin` (
  `id_level_admin` INT(11) NOT NULL AUTO_INCREMENT,
  `nama_level` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_level_admin`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `inspek_dbs`.`admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inspek_dbs`.`admin` (
  `username` VARCHAR(30) NOT NULL,
  `nama` VARCHAR(40) NULL DEFAULT NULL,
  `telpon` VARCHAR(15) NULL DEFAULT NULL,
  `email` VARCHAR(80) NULL,
  `alamat` VARCHAR(45) NULL DEFAULT NULL,
  `jenis_kelamin` TINYINT(4) NULL DEFAULT NULL,
  `password` VARCHAR(65) NULL DEFAULT NULL,
  `gambar` VARCHAR(150) NULL DEFAULT NULL,
  `status` TINYINT(4) NULL DEFAULT NULL,
  `isi_biodata` TINYINT(4) NULL DEFAULT NULL,
  `id_level_admin` INT(11) NOT NULL,
  PRIMARY KEY (`username`),
  INDEX `fk_admin_level_admin_idx` (`id_level_admin` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  CONSTRAINT `fk_admin_level_admin`
    FOREIGN KEY (`id_level_admin`)
    REFERENCES `inspek_dbs`.`level_admin` (`id_level_admin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `inspek_dbs`.`album`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inspek_dbs`.`album` (
  `id_album` INT(11) NOT NULL AUTO_INCREMENT,
  `judul` VARCHAR(50) NULL DEFAULT NULL,
  `tanggal_create` DATETIME NULL DEFAULT NULL,
  `admin_username` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id_album`),
  UNIQUE INDEX `judul` (`judul` ASC),
  INDEX `fk_album_admin1_idx` (`admin_username` ASC),
  CONSTRAINT `fk_album_admin1`
    FOREIGN KEY (`admin_username`)
    REFERENCES `inspek_dbs`.`admin` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `inspek_dbs`.`berita`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inspek_dbs`.`berita` (
  `id_berita` INT(11) NOT NULL AUTO_INCREMENT,
  `judul_berita` VARCHAR(45) NULL DEFAULT NULL,
  `kode_berita` VARCHAR(45) NULL DEFAULT NULL,
  `isi_berita` TINYTEXT NULL DEFAULT NULL,
  `created_time` DATE NULL DEFAULT NULL,
  `updated_time` DATE NULL DEFAULT NULL,
  `id_kategori_berita` INT(11) NOT NULL,
  `author` VARCHAR(30) NOT NULL,
  `keyword` TEXT NULL DEFAULT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_berita`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `inspek_dbs`.`download`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inspek_dbs`.`download` (
  `id_download` INT(11) NOT NULL AUTO_INCREMENT,
  `judul` VARCHAR(45) NULL DEFAULT NULL,
  `url_file` VARCHAR(300) NULL DEFAULT NULL,
  `admin_username` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id_download`),
  INDEX `fk_download_admin1_idx` (`admin_username` ASC),
  CONSTRAINT `fk_download_admin1`
    FOREIGN KEY (`admin_username`)
    REFERENCES `inspek_dbs`.`admin` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `inspek_dbs`.`foto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inspek_dbs`.`foto` (
  `id_foto` INT(11) NOT NULL AUTO_INCREMENT,
  `nama` VARCHAR(45) NULL DEFAULT NULL,
  `tanggal_posting` VARCHAR(45) NULL DEFAULT NULL,
  `nama_album` VARCHAR(50) NOT NULL COMMENT 'Termasuk dalam album yang mana foto tersebut',
  `admin_username` VARCHAR(30) NOT NULL,
  `token` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_foto`),
  INDEX `fk_foto_admin1_idx` (`admin_username` ASC),
  INDEX `nama_album` (`nama_album` ASC),
  CONSTRAINT `fk_foto_admin1`
    FOREIGN KEY (`admin_username`)
    REFERENCES `inspek_dbs`.`admin` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `foto_ibfk_1`
    FOREIGN KEY (`nama_album`)
    REFERENCES `inspek_dbs`.`album` (`judul`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `inspek_dbs`.`kategori_berita`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inspek_dbs`.`kategori_berita` (
  `id_kategori_berita` INT(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` VARCHAR(45) NULL DEFAULT NULL,
  `deskripsi_kategori` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id_kategori_berita`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `inspek_dbs`.`log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inspek_dbs`.`log` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(30) NOT NULL,
  `aktivitas` LONGTEXT NOT NULL,
  `date_timestamp` DATETIME NOT NULL,
  `ip_address` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `username` (`username` ASC),
  CONSTRAINT `log_ibfk_1`
    FOREIGN KEY (`username`)
    REFERENCES `inspek_dbs`.`admin` (`username`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 143
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `inspek_dbs`.`menu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inspek_dbs`.`menu` (
  `id_menu` INT(11) NOT NULL AUTO_INCREMENT,
  `nama_menu` VARCHAR(45) NULL DEFAULT NULL,
  `icon` VARCHAR(45) NULL DEFAULT NULL,
  `slug` VARCHAR(45) NULL DEFAULT NULL,
  `number` VARCHAR(45) NULL DEFAULT NULL,
  `id_parent` INT(11) NOT NULL,
  PRIMARY KEY (`id_menu`),
  INDEX `fk_menu_menu1_idx` (`id_parent` ASC),
  CONSTRAINT `fk_menu_menu1`
    FOREIGN KEY (`id_parent`)
    REFERENCES `inspek_dbs`.`menu` (`id_menu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `inspek_dbs`.`menu_temp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inspek_dbs`.`menu_temp` (
  `menu` VARCHAR(500) NOT NULL,
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `inspek_dbs`.`page`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inspek_dbs`.`page` (
  `id_page` INT(11) NOT NULL AUTO_INCREMENT,
  `judul_page` VARCHAR(45) NULL DEFAULT NULL,
  `isi_page` TINYTEXT NULL DEFAULT NULL,
  `created_time` DATE NULL DEFAULT NULL,
  `updated_time` DATE NULL DEFAULT NULL,
  `tipe_page` VARCHAR(10) NULL DEFAULT NULL,
  `admin_username` VARCHAR(30) NOT NULL,
  `kode_page` VARCHAR(45) NULL DEFAULT NULL,
  `kategori` INT(5) NULL DEFAULT NULL,
  PRIMARY KEY (`id_page`),
  INDEX `fk_page_admin1_idx` (`admin_username` ASC),
  CONSTRAINT `fk_page_admin1`
    FOREIGN KEY (`admin_username`)
    REFERENCES `inspek_dbs`.`admin` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 18
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `inspek_dbs`.`static`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inspek_dbs`.`static` (
  `id_static` INT(11) NOT NULL AUTO_INCREMENT,
  `nama_dinas` VARCHAR(40) NULL DEFAULT NULL COMMENT 'Nama site setiap dinas',
  `home_gbr_1` VARCHAR(150) NULL DEFAULT NULL COMMENT 'URL Untuk Gambar Home Depan\n',
  `home_gbr_2` VARCHAR(150) NULL DEFAULT NULL COMMENT 'URL Untuk Gambar Home Depan\n',
  `home_gbr_3` VARCHAR(150) NULL DEFAULT NULL COMMENT 'URL Untuk Gambar Home Depan\n',
  `nama_kementerian` VARCHAR(55) NULL,
  `alamat_kementerian` VARCHAR(150) NULL DEFAULT NULL,
  `link_kementerian` VARCHAR(45) NULL DEFAULT NULL,
  `nama_provinsi` VARCHAR(55) NULL,
  `alamat_provinsi` VARCHAR(70) NULL DEFAULT NULL,
  `link_provinsi` VARCHAR(45) NULL DEFAULT NULL,
  `link_kabupaten` VARCHAR(45) NULL DEFAULT NULL,
  `logo_kementrian` VARCHAR(150) NULL DEFAULT NULL,
  `logo_provinsi` VARCHAR(150) NULL DEFAULT NULL,
  `logo_instansi` VARCHAR(150) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `telpon` VARCHAR(45) NULL DEFAULT NULL,
  `jam_kerja` VARCHAR(45) NULL DEFAULT NULL,
  `warna` VARCHAR(10) NULL DEFAULT NULL,
  `alamat` VARCHAR(60) NULL DEFAULT NULL,
  `deskripsi_dinas` VARCHAR(200) NULL DEFAULT NULL,
  `facebook_dinas` VARCHAR(100) NULL,
  `twiiter_dinas` VARCHAR(100) NULL,
  PRIMARY KEY (`id_static`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `inspek_dbs`.`level_admin`
-- -----------------------------------------------------
START TRANSACTION;
USE `inspek_dbs`;
INSERT INTO `inspek_dbs`.`level_admin` (`id_level_admin`, `nama_level`) VALUES (1, 'Super Admin');
INSERT INTO `inspek_dbs`.`level_admin` (`id_level_admin`, `nama_level`) VALUES (2, 'Admin');
INSERT INTO `inspek_dbs`.`level_admin` (`id_level_admin`, `nama_level`) VALUES (3, 'Author');

COMMIT;


-- -----------------------------------------------------
-- Data for table `inspek_dbs`.`admin`
-- -----------------------------------------------------
START TRANSACTION;
USE `inspek_dbs`;
INSERT INTO `inspek_dbs`.`admin` (`username`, `nama`, `telpon`, `email`, `alamat`, `jenis_kelamin`, `password`, `gambar`, `status`, `isi_biodata`, `id_level_admin`) VALUES ('admin', 'Admin', NULL, NULL, NULL, NULL, '21232f297a57a5a743894a0e4a801fc3', NULL, NULL, 1, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `inspek_dbs`.`page`
-- -----------------------------------------------------
START TRANSACTION;
USE `inspek_dbs`;
INSERT INTO `inspek_dbs`.`page` (`id_page`, `judul_page`, `isi_page`, `created_time`, `updated_time`, `tipe_page`, `admin_username`, `kode_page`, `kategori`) VALUES (NULL, 'Beranda', NULL, NULL, NULL, NULL, 'admin', NULL, NULL);
INSERT INTO `inspek_dbs`.`page` (`id_page`, `judul_page`, `isi_page`, `created_time`, `updated_time`, `tipe_page`, `admin_username`, `kode_page`, `kategori`) VALUES (NULL, 'Berita', NULL, NULL, NULL, NULL, 'admin', 'berita', NULL);
INSERT INTO `inspek_dbs`.`page` (`id_page`, `judul_page`, `isi_page`, `created_time`, `updated_time`, `tipe_page`, `admin_username`, `kode_page`, `kategori`) VALUES (NULL, 'Gallery', NULL, NULL, NULL, NULL, 'admin', 'gallery', NULL);
INSERT INTO `inspek_dbs`.`page` (`id_page`, `judul_page`, `isi_page`, `created_time`, `updated_time`, `tipe_page`, `admin_username`, `kode_page`, `kategori`) VALUES (NULL, 'Download', NULL, NULL, NULL, NULL, 'admin', 'download', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `inspek_dbs`.`static`
-- -----------------------------------------------------
START TRANSACTION;
USE `inspek_dbs`;
INSERT INTO `inspek_dbs`.`static` (`id_static`, `nama_dinas`, `home_gbr_1`, `home_gbr_2`, `home_gbr_3`, `nama_kementerian`, `alamat_kementerian`, `link_kementerian`, `nama_provinsi`, `alamat_provinsi`, `link_provinsi`, `link_kabupaten`, `logo_kementrian`, `logo_provinsi`, `logo_instansi`, `email`, `telpon`, `jam_kerja`, `warna`, `alamat`, `deskripsi_dinas`, `facebook_dinas`, `twiiter_dinas`) VALUES (1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

COMMIT;
